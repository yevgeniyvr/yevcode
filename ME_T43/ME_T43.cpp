//sudo chmod 666 /dev/ttyACM0

#define serial_com
#define D_acq
#define save_img

////------TLD libraries
#include "TLDUtil.h"
#include <string>
#include "TLD.h"


//------CMT libraries
#include "CMT.h"
#include "gui.h"

//------Struck
#include "Tracker1.h"
#include "Config.h"
#include "vot.hpp"

//-----GOTURN
#include "network/regressor.h"
#include "loader/loader_alov.h"
#include "loader/loader_vot.h"
#include "tracker/tracker.h"
//#include "tracker/tracker_manager.h"
//#include "tracker_manager.h"
#include "helper/helper.h"
//#include "train/tracker_trainer.h"



//------OPENCV libraries
#include <iostream>
#include <opencv/cv.h>
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <math.h>

#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <vector>


//-----Multi thread libraries
#include <thread>
#include <mutex>
#include "Tracker.h"
#include "Tracker1.h"
#include <sstream>
#include <stack>
#include <ctime>

std::stack<clock_t> tictoc_stack;

//------namespace declarations
//using cmt::CMT;
using namespace tld;
using namespace std;
using namespace cv;
TLD *cftld;
cmt::CMT cmtobj;




//-----Variable Declaration
bool flag_detector_enable=false;
bool init_cond=true;
bool start=true;
int pan_slide = 1500;
int tilt_slide = 1500;
int max_antiwindup=2600;
int min_antiwindup=200;
int drag = 0, select_flag = 0;

Point point1, point2;
cv::Rect rect;
cv::Rect rect_CMT;

double tilt = 1500.0;
double pan=1500.0;
double Set_point_depth_TLD=0.0;


//-----TLD display variables and calculation variables
int cftld_x=0;
int cftld_y=0;
int cftld_w=0;
int cftld_h=0;
double M_D_TLD=0.0;
double confidence_TLD=0.0;
double err_x_TLD=0.0;
double err_y_TLD=0.0;
double err_w_TLD=0.0;
double err_h_TLD=0.0;
Point cftld_br;
Point cftld_tl;
//---------------------------


double TLD_min=0;
double CMT_min=0;
double STRUCK_min=0;
double GOTURN_min=0;

double confidence_CMT=0;
double CMT_init_points=0;
float CMT_x_ratio=0.0;
float CMT_y_ratio=0.0;
std::vector<Point2f> target_frame;

double confidence_STRUCK=0.0;

//sudo chmod 666 /dev/ttyACM0

#define serial_com
#define D_acq
#define save_img
#define VOT_RECTANGLE

////------TLD libraries
#include "TLDUtil.h"
#include <string>
#include "TLD.h"


//------CMT libraries
#include "CMT.h"
#include "gui.h"

//------Struck
#include "Tracker1.h"
#include "Config.h"
//#include "vot.hpp"

//-----GOTURN
//#include "network/regressor.h" //Need
//#include "loader/loader_alov.h" //Need
//#include "loader/loader_vot.h" // Need
//#include "tracker/tracker.h" //Need
//#include "tracker/tracker_manager.h"
//#include "tracker_manager.h"
//#include "helper/helper.h"//Need
//#include "train/tracker_trainer.h"



//------OPENCV libraries
#include <iostream>
#include <opencv/cv.h>
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <math.h>
#include <stdio.h>

#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <vector>


//-----Multi thread libraries
#include <thread>
#include <mutex>
#include "Tracker.h"
#include "Tracker1.h"
#include <sstream>
#include <stack>
#include <ctime>
#include <X11/Xlib.h>
#include <cstdlib>


// VOT STUFF
#include "/home/yevgeniy/Documents/vot-toolkit/tracker/examples/native/vot.h"



std::stack<clock_t> tictoc_stack;

//------namespace declarations
//using cmt::CMT;
using namespace tld;
using namespace std;
using namespace cv;
TLD *cftld;
cmt::CMT cmtobj;




//-----Variable Declaration
bool flag_detector_enable=false;
bool init_cond=true;
bool start=true;
int pan_slide = 1500;
int tilt_slide = 1500;
int max_antiwindup=2600;
int min_antiwindup=200;
int drag = 0, select_flag = 0;

Point point1, point2;
cv::Rect rect;
cv::Rect rect_CMT;

double tilt = 1500.0;
double pan=1500.0;
double Set_point_depth_TLD=0.0;


//-----TLD display variables and calculation variables
int cftld_x=0;
int cftld_y=0;
int cftld_w=0;
int cftld_h=0;
double M_D_TLD=0.0;
double confidence_TLD=0.0;
double err_x_TLD=0.0;
double err_y_TLD=0.0;
double err_w_TLD=0.0;
double err_h_TLD=0.0;
Point cftld_br;
Point cftld_tl;
//---------------------------


double TLD_min=0;
double CMT_min=0;
double STRUCK_min=0;
double GOTURN_min=0;

double confidence_CMT=0;
double CMT_init_points=0;
float CMT_x_ratio=0.0;
float CMT_y_ratio=0.0;
std::vector<Point2f> target_frame;

double confidence_STRUCK=0.0;


int struck_x=0;
int struck_y=0;
int struck_x_tl=0;
int struck_y_tl=0;
int struck_x_br=0;
int struck_y_br=0;
int struck_w=0;
int struck_h=0;

//NORMALLY IMAGES ARE 640,480 -> Im trying 360,240

//----Images and Matrices
Mat img(640,480,CV_8UC1);
Mat CMT_image(240, 360, CV_8UC1);
Mat grey;
Mat roiImg;
Mat mytemplate;
Mat img_cftld;
Mat estimated_TLD;
Mat estimated_CMT;
Mat estimated_STRUCK;
Mat_<float> PID_control_matrix(2,1);
//Mat estimated;
Mat estimated;
//Mat grey_CMT1;

//----Mutex for threads
std::mutex mu1;
std::mutex mu2;
std::mutex mu3;
std::mutex mu4;


std::mutex mu_cftld;
std::mutex mu_struck;
std::mutex mu_cmt;
std::mutex mu_goturn;


//------BB colors
#define GREEN  cv::Scalar (0,255,0)
#define RED    cv::Scalar (0,0,255)
#define BLUE   cv::Scalar (255,0,0)
#define YELLOW cv::Scalar(0,125,255)
#define WHITE cv::Scalar(255,255,255)
//Added by Yev
#define PURPLE cv::Scalar(128,0,128)

//------BB colors for differen detectors
cv::Scalar color;
cv::Scalar color_TLD;
cv::Scalar color_CMT;
cv::Scalar color_STRUCK;
//Added by Yev
cv::Scalar color_GOTURN;

//------Declaration of variable for saving .txt files
ofstream data_t;
ofstream data_t_cov;
static int c=0;

//----Serial Port declaration (Arduino)
#ifdef serial_com
char serialPortFilename[] = "/dev/ttyACM0";
#endif


//--------GOTURN DECLARATIONS
const bool show_intermediate_output = false;
const string model_file  = "/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/nets/tracker.prototxt";
const string trained_file = "/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/nets/models/pretrained_model/tracker.caffemodel";
const string videos_folder = "/home/yevgeniy/Desktop/Datasets/alov300++_rectangleAnnotation_full";
const bool do_train = false;
int gpu_id = 0;
//Regressor regressor(model_file, trained_file, gpu_id, do_train); //Need
//Tracker tracker(show_intermediate_output); //Need
//BoundingBox bbox_gt;//Need
Mat imageGOTURN(640,480,CV_8UC3);
//----GOTURN display variables and calculation variables
int GOTURN_x=0;
int GOTURN_y=0;
int GOTURN_w=0;
int GOTURN_h=0;
double M_D_GOTURN=0.0;
double confidence_GOTURN=0.0;
double err_x_GOTURN=0.0;
double err_y_GOTURN=0.0;
double err_w_GOTURN=0.0;
double err_h_GOTURN=0.0;
Point GOTURN_br;
Point GOTURN_tl;
Mat estimated_GOTURN;
Point matchGOTURN;
//BoundingBox result; //Need
//BoundingBox bbox_estimate; //Need
//BoundingBox bbox_gt;
//---------------------------

struck_con::Tracker *struck_1;
string configPath = "config.txt";
Config conf(configPath);
struck_con::Tracker main_struck(conf);

int code;

static Mat_<float> Initialbox(4,1);


double distanceCalculate(double x1, double x2, double y1, double y2,double h1, double h2, double w1, double w2)
{
	//cout <<"I am in distanceCalculate "<<endl;
    double x = x1 - x2;
    double y = y1 - y2;
    double h = h1 - h2;
    double w = w1 - w2;
    double dist;


    dist = pow(x,2)+pow(y,2)+pow(h,2)+pow(w,2);
    dist = sqrt(dist);

    return dist;
}



void rectangle(Mat& rMat, const FloatRect& rRect, const Scalar& rColour)
{
	IntRect r(rRect);
	rectangle(rMat, Point(r.XMin(), r.YMin()), Point(r.XMax(), r.YMax()), rColour);
}


void display_detectors()
{



//	 //-------TLD Display
//	 rectangle(img, cftld_tl, cftld_br, color_TLD, 3);
//	 circle(img, Point(cftld_x,cftld_y), 5, color_TLD, 3);
//	 //-------CMT Display
//	 circle(img, Point(cmt_x,cmt_y), 5, color_CMT, 3);
//	 rectangle(img, Point(cmt_x_tl,cmt_y_tl), Point(cmt_x_br,cmt_y_br), color_CMT, 3);
	   if (!((img.size().width > 0) & (img.size().height > 0)))
		   return;


	 	color=color_TLD;
		if(estimated_TLD.empty()==false)
		{
			rectangle( img, Point(estimated_TLD.at<float>(0)- estimated_TLD.at<float>(3)/2 ,estimated_TLD.at<float>(1)- estimated_TLD.at<float>(2)/2), Point(estimated_TLD.at<float>(3)/2 + estimated_TLD.at<float>(0),  estimated_TLD.at<float>(2)/2 + estimated_TLD.at<float>(1)),color, 3 );
			circle(img, Point(estimated_TLD.at<float>(0),estimated_TLD.at<float>(1)), 5, color, 3);
		}
		color=color_CMT;
		if(estimated_CMT.empty()==false)
		{
			rectangle( img, Point(estimated_CMT.at<float>(0)- estimated_CMT.at<float>(3)/2 ,estimated_CMT.at<float>(1)- estimated_CMT.at<float>(2)/2), Point(estimated_CMT.at<float>(3)/2 + estimated_CMT.at<float>(0),  estimated_CMT.at<float>(2)/2 + estimated_CMT.at<float>(1)), color, 3 );
			circle(img, Point(estimated_CMT.at<float>(0),estimated_CMT.at<float>(1)), 5, color, 3);


			 //-------CMT Display Points
			for(size_t i = 0; i < cmtobj.points_active.size(); i++)
			{
				circle(img, Point(cmtobj.points_active[i].x,cmtobj.points_active[i].y),  1, color_CMT);
				target_frame.push_back(cmtobj.points_active[i]);
			}


		}
		color=color_STRUCK;
		if(estimated_STRUCK.empty()==false)
		{
			rectangle( img, Point(estimated_STRUCK.at<float>(0)- estimated_STRUCK.at<float>(3)/2 ,estimated_STRUCK.at<float>(1)- estimated_STRUCK.at<float>(2)/2), Point(estimated_STRUCK.at<float>(3)/2 + estimated_STRUCK.at<float>(0),  estimated_STRUCK.at<float>(2)/2 + estimated_STRUCK.at<float>(1)), color, 3 );
			circle(img, Point(estimated_STRUCK.at<float>(0),estimated_STRUCK.at<float>(1)), 5, color, 3);
		}
		/*
		color=color_GOTURN;
		if(estimated_GOTURN.empty()==false)
		{

			rectangle( img, Point(estimated_GOTURN.at<float>(0)- estimated_GOTURN.at<float>(3)/2 ,estimated_GOTURN.at<float>(1)- estimated_GOTURN.at<float>(2)/2), Point(estimated_GOTURN.at<float>(3)/2 + estimated_GOTURN.at<float>(0),  estimated_GOTURN.at<float>(2)/2 + estimated_GOTURN.at<float>(1)),color, 3 );
			circle(img, Point(estimated_GOTURN.at<float>(0),estimated_GOTURN.at<float>(1)), 5, color, 3);
		}
		*/

}

void TLD_THREAD()
{

	//TODO: ALl this variables can be declared in a different file.
	bool KF_init_TLD=false;
	static KalmanFilter KF_TLD(8, 4, 0);
	static Mat_<float> state_TLD(8, 1);
	static Mat processNoise_TLD(8, 1, CV_32F);
	static Mat_<float> measurement_TLD(4,1);
	static Mat_<float> prev_measurement_TLD(4,1); prev_measurement_TLD=Initialbox;
	Mat prediction_TLD,Ht_TLD,Ree_TLD;

	Mat local_img;


	while(1)
	{
		double e1 = cv::getTickCount();
		if(select_flag !=2)
		{
			sleep(.5);

			//			   <<KF.measurementNoiseCov.at<float>(9,9)<<endl;		//---C_CMT_w		(9,9)
			//cout<<"select 2"<<endl;

		}

		if(select_flag ==2)
		{

			mu_cftld.lock();
			img_cftld.copyTo(local_img);
			mu_cftld.unlock();


			cftld->processImage(local_img);

			if(cftld->currBB != NULL)
			{
			  color_TLD=RED;
			  cftld_x=cftld->currBB->x + (cftld->currBB->width/2);
			  cftld_y=cftld->currBB->y + (cftld->currBB->height/2);
			  cftld_tl=cftld->currBB->tl();
			  cftld_br=cftld->currBB->br();
			  cftld_w=cftld->currBB->width;
			  cftld_h=cftld->currBB->height;
			}
			else
			{
				color_TLD=BLUE;
			}
			//TODO: Make sure to release the memory or lock the variable


			if(KF_init_TLD==false)
			{
				KF_init_TLD=true;

				KF_TLD.statePre.at<float>(0) =  cftld_x;
				KF_TLD.statePre.at<float>(1) =  cftld_y;
				KF_TLD.statePre.at<float>(2) =  cftld_h;
				KF_TLD.statePre.at<float>(3) =  cftld_w;
				KF_TLD.statePre.at<float>(4) =  0.0;
				KF_TLD.statePre.at<float>(5) =  0.0;
				KF_TLD.statePre.at<float>(6) =  0.0;
				KF_TLD.statePre.at<float>(7) =  0.0;

				KF_TLD.statePost.at<float>(0) =  cftld_x;
				KF_TLD.statePost.at<float>(1) =  cftld_y;
				KF_TLD.statePost.at<float>(2) =  cftld_h;
				KF_TLD.statePost.at<float>(3) =  cftld_w;
				KF_TLD.statePost.at<float>(4) =  0.0;
				KF_TLD.statePost.at<float>(5) =  0.0;
				KF_TLD.statePost.at<float>(6) =  0.0;
				KF_TLD.statePost.at<float>(7) =  0.0;

				KF_TLD.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																0,1,0,0,0,1,0,0,
																0,0,1,0,0,0,1,0,
																0,0,0,1,0,0,0,1,
																0,0,0,0,1,0,0,0,
																0,0,0,0,0,1,0,0,
																0,0,0,0,0,0,1,0,
																0,0,0,0,0,0,0,1);

				KF_TLD.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0,
																0,0.0001,0,0,0,0,0,0,
																0,0,0.0001,0,0,0,0,0,
																0,0,0,0.0001,0,0,0,0,
																0,0,0,0,0.008,0,0,0,
																0,0,0,0,0,0.008,0,0,
																0,0,0,0,0,0,0.008,0,
																0,0,0,0,0,0,0,0.008);

				KF_TLD.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																 0,1,0,0,0,0,0,0,
																 0,0,1,0,0,0,0,0,
																 0,0,0,1,0,0,0,0);

				KF_TLD.measurementNoiseCov=  (Mat_<float>(4, 4) << 0.24,0,0,0,
																   0,0.24,0,0,
																   0,0,0.24,0,
																   0,0,0,0.24);
				}

			measurement_TLD(0) =  cftld_x;
			measurement_TLD(1) =  cftld_y;
			measurement_TLD(2) =  cftld_h;
			measurement_TLD(3) =  cftld_w;

			prediction_TLD = KF_TLD.predict();

			transpose(KF_TLD.measurementMatrix, Ht_TLD);
			Ree_TLD= (KF_TLD.measurementMatrix*KF_TLD.errorCovPre*Ht_TLD)+KF_TLD.measurementNoiseCov;

			err_x_TLD=cftld_x-prediction_TLD.at<float>(0);
			err_y_TLD=cftld_y-prediction_TLD.at<float>(1);
			err_h_TLD=cftld_h-prediction_TLD.at<float>(2);
			err_w_TLD=cftld_w-prediction_TLD.at<float>(3);

			M_D_TLD=sqrt((pow(err_x_TLD,2)/Ree_TLD.at<float>(0,0))+(pow(err_y_TLD,2)/Ree_TLD.at<float>(1,1))+(pow(err_h_TLD,2)/Ree_TLD.at<float>(2,2))+(pow(err_w_TLD,2)/Ree_TLD.at<float>(3,3)));

			if(cftld->currBB == NULL)
			{
				M_D_TLD=20;
				confidence_TLD=(100000/(1+exp(-M_D_TLD+10)));
			}
			else
			{
				confidence_TLD=(10/(1+exp(-M_D_TLD+10)));
			}

			mu2.lock();
			estimated_TLD=KF_TLD.correct(measurement_TLD);
			mu2.unlock();
			if (estimated_TLD.empty())
			{

				cout<<"Why is TLD empty " << endl;
			}
//
//			cout <<"Estimated_TLD "<<estimated_TLD <<endl;
//			cout << "estimated_TLD.at<float>(3) " << estimated_TLD.at<float>(3) << endl;


			//cout << "Inside TLD_THREAD " << endl;
//		    cout << estimated_TLD.at<float>(0) << endl;
//			cout << estimated_TLD.at<float>(1) << endl;
//			cout << "****" << endl;
			//imshow("TLD IMAGE",img_cftld);
			//cv::waitKey(2);
		}
		double e2 = cv::getTickCount();
		//double timetrack = (e2 - e1)/ (cv::getTickFrequency());
//		if (local_img.size().width > 0 & local_img.size().height > 0)
//		imshow("TLD IMAGE",local_img);
//		cv::waitKey(2);
		//cout << "TLD TIME: " << timetrack << endl;
	}

}
/*
BoundingBox GOTURN( Mat &img, Mat &mytemplate )
{

  Mat result;
  //GO-Turn
  const cv::Mat& image = img;
  //Result goes in bbox_estimate/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/src/tracker/tracker_manager.h:14:44: error: ‘Tracker’ has not been declared

  BoundingBox bbox_estimate;
  tracker.Track(image, &regressor, &bbox_estimate);
  std::cout << "GOTURN thinks: " << bbox_estimate.get_center_x() << bbox_estimate.get_center_y() << endl;

  //matchTemplate( img, mytemplate, result, CV_TM_SQDIFF_NORMED );
  //normalize( result, result, 0, 1, NORM_MINMAX, -1, Mat() );



  return bbox_estimate;
}
*/
/*
void GOTURN_THREAD()
{
	//TODO: ALl this variables can be declared in a different file.
	bool KF_init_GOTURN=false;
	static KalmanFilter KF_GOTURN(8, 4, 0);
	static Mat_<float> state_GOTURN(8, 1);
	static Mat processNoise_GOTURN(8, 1, CV_32F);
	static Mat_<float> measurement_GOTURN(4,1);
	static Mat_<float> prev_measurement_GOTURN(4,1); prev_measurement_GOTURN.setTo(Scalar(0));
	Mat prediction_GOTURN,Ht_GOTURN,Ree_GOTURN;
	color_GOTURN = PURPLE;
	while(1)
	{
	//tic();

		if(select_flag !=2)
		{

			cout<<""<<endl;

		}

		if(select_flag ==2)
		{

			//mu4.lock();
			//const cv::Mat& image = img;
			//Mat imageGOTURN(640,480,CV_8UC3);
			//imageGOTURN = img.clone();

			BoundingBox result;
			//BoundingBox bbox_estimate;
			//double e1 = cv::getTickCount();
			tracker.Track(imageGOTURN, &regressor, &bbox_estimate);
			//double e2 = cv::getTickCount();
			//double timetrack = (e2 - e1)/ (cv::getTickFrequency());
			//cout << timetrack << endl;
			Point matchGOTURN;
			result = bbox_estimate;

		    matchGOTURN.x = result.get_center_x();
			matchGOTURN.y = result.get_center_y();

			GOTURN_x=matchGOTURN.x;
			GOTURN_y=matchGOTURN.y;
			//cftld_tl=
			//cftld_br=
			GOTURN_w=result.get_width();
			GOTURN_h=result.get_height();
			//mu4.unlock();
			//time_t end = time(0);
			//double time = difftime(end, start) * 1000.0;

			//TODO: Make sure to release the memumory or lock the variable

			//mu4.lock();
			if(KF_init_GOTURN==false)
			{
				KF_init_GOTURN=true;


				KF_GOTURN.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																0,1,0,0,0,1,0,0,
																0,0,1,0,0,0,1,0,
																0,0,0,1,0,0,0,1,
																0,0,0,0,1,0,0,0,
																0,0,0,0,0,1,0,0,
																0,0,0,0,0,0,1,0,
																0,0,0,0,0,0,0,1);

				KF_GOTURN.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0, //R
																0,0.0001,0,0,0,0,0,0,
																0,0,0.0001,0,0,0,0,0,
																0,0,0,0.0001,0,0,0,0,
																0,0,0,0,0.0008,0,0,0,
																0,0,0,0,0,0.0008,0,0,
																0,0,0,0,0,0,0.0008,0,
																0,0,0,0,0,0,0,0.0008);

				KF_GOTURN.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																 0,1,0,0,0,0,0,0,
																 0,0,1,0,0,0,0,0,
																 0,0,0,1,0,0,0,0);

				KF_GOTURN.measurementNoiseCov=  (Mat_<float>(4, 4) << 0.02,0,0,0, //Q
																   0,0.04,0,0,
																   0,0,0.02,0,
																   0,0,0,0.04);
				}

			measurement_GOTURN(0) =  GOTURN_x;
			measurement_GOTURN(1) =  GOTURN_y;
			measurement_GOTURN(2) =  GOTURN_h;
			measurement_GOTURN(3) =  GOTURN_w;

			prediction_GOTURN = KF_GOTURN.predict();

			transpose(KF_GOTURN.measurementMatrix, Ht_GOTURN);

			Ree_GOTURN= (KF_GOTURN.measurementMatrix*KF_GOTURN.errorCovPre*Ht_GOTURN)+KF_GOTURN.measurementNoiseCov;

			err_x_GOTURN=GOTURN_x-prediction_GOTURN.at<float>(0);
			err_y_GOTURN=GOTURN_y-prediction_GOTURN.at<float>(1);
			err_h_GOTURN=GOTURN_h-prediction_GOTURN.at<float>(2);
			err_w_GOTURN=GOTURN_w-prediction_GOTURN.at<float>(3);

			M_D_GOTURN=sqrt((pow(err_x_GOTURN,2)/Ree_GOTURN.at<float>(0,0))+(pow(err_y_GOTURN,2)/Ree_GOTURN.at<float>(1,1))+(pow(err_h_GOTURN,2)/Ree_GOTURN.at<float>(2,2))+(pow(err_w_GOTURN,2)/Ree_GOTURN.at<float>(3,3)));

		//	if(cftld->currBB == NULL)
		//	{
		//		M_D_GOTURN=20;
		//		confidence_GOTURN=(100000/(1+exp(-M_D_GOTURN+10)));

		//	}mage, &regressor, &bbox_estim
		//	else
		//	{
				confidence_GOTURN=(10/(1+exp(-M_D_GOTURN+10)));

		//	}
			//estimated_GOTURN = measurement_GOTURN;
			estimated_GOTURN=KF_GOTURN.correct(measurement_GOTURN);
			//double e2 = cv::getTickCount();
			//double timetrack = (e2 - e1)/ (cv::getTickFrequency());
			//cout << timetrack << endl;
			//mu4.unlock();
		}
	//toc();
	}


}
*/

void CMT_THREAD()
{

	//-----CMT display variables and calculation variables
	int cmt_x=0;
	int cmt_y=0;
	int cmt_x_tl=0;
	int cmt_y_tl=0;
	int cmt_x_br=0;
	int cmt_y_br=0;
	int cmt_w=0;
	int cmt_h=0;
	double M_D_CMT=0;
	double err_x_CMT=0;
	double err_y_CMT=0;
	double err_w_CMT=0;
	double err_h_CMT=0;
	double cv_CMT=0.2;
	double CMT_confidence=0.0;


	bool KF_init_CMT=false;
	static KalmanFilter KF_CMT(8, 4, 0);
	static Mat_<float> state_CMT(8, 1);
	static Mat processNoise_CMT(8, 1, CV_32F);
	static Mat_<float> measurement_CMT(4,1);
	static Mat_<float> prev_measurement_CMT(4,1); prev_measurement_CMT=Initialbox;
	Mat prediction_CMT,Ht_CMT,Ree_CMT;


	Mat grey_CMT1;

	while(1){
	if(select_flag !=2)
	{
		sleep(.5);
		//cout<<"select2"<<endl;
	}
	//int o =0;
		if(select_flag ==2)
		{
			Mat grey_CMT1;
			//TODO: Why do we have to copy this image here?
			mu_cmt.lock();
			//double e1 = cv::getTickCount();
			//grey.copyTo(grey_CMT1);
			//double e2 = cv::getTickCount();
			//double timetrack = (e2 - e1)/ (cv::getTickFrequency());
			//cout << timetrack << endl;

			grey.copyTo(grey_CMT1);

			mu_cmt.unlock();
			cmtobj.processFrame(grey_CMT1);

			//cmtobj.processFrame(grey_CMT1);

			CMT_confidence=cmtobj.points_active.size()/CMT_init_points;
			 if(std::isnan(CMT_confidence))
			 {
				 CMT_confidence=0.01;
			 }

			 if(CMT_confidence>=0.00)
			 {
				color_CMT=GREEN;
				cmt_x_tl=min(max((int)cmtobj.bb_rot.boundingRect().tl().x,0),(int)img.cols);
				cmt_y_tl=min(max((int)cmtobj.bb_rot.boundingRect().tl().y,0),(int)img.rows);
				cmt_x_br=min(max((int)cmtobj.bb_rot.boundingRect().br().x,10),(int)img.cols);
				cmt_y_br=min(max((int)cmtobj.bb_rot.boundingRect().br().y,10),(int)img.rows);

				//cout<<"Calculating CMT stuff.. "<<endl;
				cmt_w=(cmt_x_br-cmt_x_tl);
				cmt_h=(cmt_y_br-cmt_y_tl);
				cmt_x=cmt_x_tl+(cmt_w/2);
				cmt_y=cmt_y_tl+(cmt_h/2);

			 }
			 else
			 {
				 color_CMT=BLUE;
			 }

			 if(KF_init_CMT==false)
			 {
				KF_init_CMT=true;

//				KF_CMT.statePre.at<float>(0) =  cmt_x;
//				KF_CMT.statePre.at<float>(1) =  cmt_y;
//				KF_CMT.statePre.at<float>(2) =  cmt_h;
//				KF_CMT.statePre.at<float>(3) =  cmt_w;
//				KF_CMT.statePre.at<float>(4) =  0.0;
//				KF_CMT.statePre.at<float>(5) =  0.0;
//				KF_CMT.statePre.at<float>(6) =  0.0;
//				KF_CMT.statePre.at<float>(7) =  0.0;
//
//				KF_CMT.statePost.at<float>(0) =  cmt_x;
//				KF_CMT.statePost.at<float>(1) =  cmt_y;
//				KF_CMT.statePost.at<float>(2) =  cmt_h;
//				KF_CMT.statePost.at<float>(3) =  cmt_w;
//				KF_CMT.statePost.at<float>(4) =  0.0;
//				KF_CMT.statePost.at<float>(5) =  0.0;
//				KF_CMT.statePost.at<float>(6) =  0.0;
//				KF_CMT.statePost.at<float>(7) =  0.0;
				KF_CMT.statePre.at<float>(0) =  cmt_x;
				KF_CMT.statePre.at<float>(1) =  cmt_y;
				KF_CMT.statePre.at<float>(2) =  cmt_h;
				KF_CMT.statePre.at<float>(3) =  cmt_w;

			    KF_CMT.statePost.at<float>(0) = Initialbox(0); //State x
			    KF_CMT.statePost.at<float>(1) = Initialbox(1); //State y
			    KF_CMT.statePost.at<float>(2) = Initialbox(2); //State Vx
			    KF_CMT.statePost.at<float>(3) = Initialbox(3); //State Vy

				KF_CMT.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																0,1,0,0,0,1,0,0,
																0,0,1,0,0,0,1,0,
																0,0,0,1,0,0,0,1,
																0,0,0,0,1,0,0,0,
																0,0,0,0,0,1,0,0,
																0,0,0,0,0,0,1,0,
																0,0,0,0,0,0,0,1);


				KF_CMT.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0,
																0,0.0001,0,0,0,0,0,0,
																0,0,0.0001,0,0,0,0,0,
																0,0,0,0.0001,0,0,0,0,
																0,0,0,0,0.008,0,0,0,
																0,0,0,0,0,0.008,0,0,
																0,0,0,0,0,0,0.008,0,
																0,0,0,0,0,0,0,0.008);

				KF_CMT.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																 0,1,0,0,0,0,0,0,
																 0,0,1,0,0,0,0,0,
																 0,0,0,1,0,0,0,0);

				KF_CMT.measurementNoiseCov=  (Mat_<float>(4, 4) << 1.6,0,0,0,
																   0,1.6,0,0,
																   0,0,1.6,0,
																   0,0,0,1.6);

			 }

			 measurement_CMT(0) =  cmt_x;
			 measurement_CMT(1) =  cmt_y;
			 measurement_CMT(2) =  cmt_h;
			 measurement_CMT(3) =  cmt_w;


			 prediction_CMT = KF_CMT.predict();

			 transpose(KF_CMT.measurementMatrix, Ht_CMT);
			 Ree_CMT= (KF_CMT.measurementMatrix*KF_CMT.errorCovPre*Ht_CMT)+KF_CMT.measurementNoiseCov;

			 err_x_CMT=cmt_x-prediction_CMT.at<float>(0);
			 err_y_CMT=cmt_y-prediction_CMT.at<float>(1);
			 err_h_CMT=cmt_h-prediction_CMT.at<float>(2);
			 err_w_CMT=cmt_w-prediction_CMT.at<float>(3);

			 M_D_CMT=sqrt((pow(err_x_CMT,2)/Ree_CMT.at<float>(0,0))+(pow(err_y_CMT,2)/Ree_CMT.at<float>(1,1))+(pow(err_h_CMT,2)/Ree_CMT.at<float>(2,2))+(pow(err_w_CMT,2)/Ree_CMT.at<float>(3,3)));



			 //if(CMT_confidence<cv_CMT)
			 if(CMT_confidence<0.00)
			 {
				 M_D_CMT=20;
				 confidence_CMT=(10000/(1+exp(-M_D_CMT+10)));
				 //confidence_CMT=(exp(-(-M_D_CMT-3)));
			 }
			 else
			 {

				confidence_CMT=(10/(1+exp(-M_D_CMT+10)));
			 }

			 //cout << "confidence_CMT = " << CMT_confidence << endl;


			mu1.lock();
			estimated_CMT=KF_CMT.correct(measurement_CMT);
			mu1.unlock();
			if (estimated_CMT.empty())
			{

				cout<<"Why is CMT empty " << endl;
			}
			cout << "estimated_CMT "<<estimated_CMT<<endl;
			 //imshow("CMT IMAGE",grey_CMT1);

		}
	}
}


void STRUCK_THREAD()
{

	struck_1 = &main_struck;
	//-----CMT display variables and calculation variables
	int struck_x=0;
	int struck_y=0;
	int struck_x_tl=0;
	int struck_y_tl=0;
	int struck_x_br=0;
	int struck_y_br=0;
	int struck_w=0;
	int struck_h=0;
	double M_D_STRUCK=0;
	double err_x_STRUCK=0;
	double err_y_STRUCK=0;
	double err_w_STRUCK=0;
	double err_h_STRUCK=0;

	bool KF_init_STRUCK=false;
	static KalmanFilter KF_STRUCK(8, 4, 0);
	static Mat_<float> state_STRUCK(8, 1);
	static Mat processNoise_STRUCK(8, 1, CV_32F);
	static Mat_<float> measurement_STRUCK(4,1);
	static Mat_<float> prev_measurement_STRUCK(4,1); prev_measurement_STRUCK=Initialbox;
	Mat prediction_STRUCK,Ht_STRUCK,Ree_STRUCK;
	Mat grey_ST(360,240,CV_8UC1);

	while(1){
	//tic();
		if(select_flag !=2)
		{
			sleep(.5);
			//cout<<"  "<<endl;
		}
		if(select_flag ==2)
		{
			//TODO: Why do we have to copy this image here?
			mu3.lock();
			grey.copyTo(grey_ST);
			mu3.unlock();

			struck_1->Track(grey_ST);

			FloatRect bb = struck_1->GetBB();

			color_STRUCK=WHITE;
			struck_x_tl=min(max((int)bb.XMin(),0),(int)img.cols);
			struck_y_tl=min(max((int)bb.YMin(),0),(int)img.rows);
			struck_x_br=min(max((int)bb.XMax(),10),(int)img.cols);
			struck_y_br=min(max((int)bb.YMax(),10),(int)img.rows);


			struck_w=(struck_x_br-struck_x_tl);
			struck_h=(struck_y_br-struck_y_tl);
			struck_x=struck_x_tl+(struck_w/2);
			struck_y=struck_y_tl+(struck_h/2);

			 if(KF_init_STRUCK==false)
			 {
				KF_init_STRUCK=true;

				KF_STRUCK.statePre.at<float>(0) =  struck_x;
				KF_STRUCK.statePre.at<float>(1) =  struck_y;
				KF_STRUCK.statePre.at<float>(2) =  struck_h;
				KF_STRUCK.statePre.at<float>(3) =  struck_w;
				KF_STRUCK.statePre.at<float>(4) =  0.0;
				KF_STRUCK.statePre.at<float>(5) =  0.0;
				KF_STRUCK.statePre.at<float>(6) =  0.0;
				KF_STRUCK.statePre.at<float>(7) =  0.0;

				KF_STRUCK.statePost.at<float>(0) =  struck_x;
				KF_STRUCK.statePost.at<float>(1) =  struck_y;
				KF_STRUCK.statePost.at<float>(2) =  struck_h;
				KF_STRUCK.statePost.at<float>(3) =  struck_w;
				KF_STRUCK.statePost.at<float>(4) =  0.0;
				KF_STRUCK.statePost.at<float>(5) =  0.0;
				KF_STRUCK.statePost.at<float>(6) =  0.0;
				KF_STRUCK.statePost.at<float>(7) =  0.0;
//


				KF_STRUCK.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																   0,1,0,0,0,1,0,0,
																   0,0,1,0,0,0,1,0,
																   0,0,0,1,0,0,0,1,
																   0,0,0,0,1,0,0,0,
																   0,0,0,0,0,1,0,0,
																   0,0,0,0,0,0,1,0,
																   0,0,0,0,0,0,0,1);


				KF_STRUCK.processNoiseCov =  (Mat_<float>(8, 8) << 0.001,0,0,0,0,0,0,0,
																   0,0.001,0,0,0,0,0,0,
																   0,0,0.001,0,0,0,0,0,
																   0,0,0,0.001,0,0,0,0,
																   0,0,0,0,0.08,0,0,0,
																   0,0,0,0,0,0.08,0,0,
																   0,0,0,0,0,0,0.08,0,
																   0,0,0,0,0,0,0,0.08);


				KF_STRUCK.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																	 0,1,0,0,0,0,0,0,
																	 0,0,1,0,0,0,0,0,
																	 0,0,0,1,0,0,0,0);

				KF_STRUCK.measurementNoiseCov=  (Mat_<float>(4, 4) << 1.2,0,0,0,
																	   0,1.2,0,0,
																	   0,0,1.2,0,
																	   0,0,0,1.2);

			 }

			 measurement_STRUCK(0) =  struck_x;
			 measurement_STRUCK(1) =  struck_y;
			 measurement_STRUCK(2) =  struck_h;
			 measurement_STRUCK(3) =  struck_w;

			 prediction_STRUCK = KF_STRUCK.predict();

			 transpose(KF_STRUCK.measurementMatrix, Ht_STRUCK);
			 Ree_STRUCK= (KF_STRUCK.measurementMatrix*KF_STRUCK.errorCovPre*Ht_STRUCK)+KF_STRUCK.measurementNoiseCov;

			 err_x_STRUCK=struck_x-prediction_STRUCK.at<float>(0);
			 err_y_STRUCK=struck_y-prediction_STRUCK.at<float>(1);
			 err_h_STRUCK=struck_h-prediction_STRUCK.at<float>(2);
			 err_w_STRUCK=struck_w-prediction_STRUCK.at<float>(3);

			 M_D_STRUCK=sqrt((pow(err_x_STRUCK,2)/Ree_STRUCK.at<float>(0,0))+(pow(err_y_STRUCK,2)/Ree_STRUCK.at<float>(1,1))+(pow(err_h_STRUCK,2)/Ree_STRUCK.at<float>(2,2))+(pow(err_w_STRUCK,2)/Ree_STRUCK.at<float>(3,3)));

			confidence_STRUCK=(10/(1+exp(-M_D_STRUCK+10)));

			mu3.lock();
			estimated_STRUCK=KF_STRUCK.correct(measurement_STRUCK);
			mu3.unlock();
			if (estimated_STRUCK.empty())
			{

				cout<<"Why is STRUCK empty " << endl;
			}
			//if (grey_ST.size().width > 0 & grey_ST.size().height > 0)
				//imshow("STRUCK IMAGE", grey_ST);
			//cv::waitKey(2);
		}
	//toc();
	}
}



void mouseHandler(int event, int x, int y, int flags, void *param)
{
//    if (event == CV_EVENT_LBUTTONDOWN && !drag)
//    {
//        //-----left button clicked. ROI selection begins
//    	point1 = Point(x, y);
//        drag = 1;
//    }
//
//    if (event == CV_EVENT_MOUSEMOVE && drag)
//    {
//        //----mouse dragged. ROI being selected
//        Mat img1;
//        img.copyTo(img1);
//        point2 = Point(x, y);
//        rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
//        imshow("camera", img1);
//    }
//
//    if (event == CV_EVENT_LBUTTONUP && drag)
//    {
//        point2 = Point(x, y);
//        rect = cv::Rect(point1.x, point1.y, x - point1.x, y - point1.y);
//        rect_CMT=cv::Rect(point1.x/CMT_x_ratio, point1.y/CMT_y_ratio, (x - point1.x)/CMT_x_ratio, (y - point1.y)/CMT_y_ratio);
//        drag = 0;
//        roiImg=img(rect);//This is the image in the bounding box
//
//
//        //GO-TURN Initialization
//        //BoundingBox bbox_gt;
//        bbox_gt.x1_ = point2.x;
//        bbox_gt.y1_ = point2.y;
//        bbox_gt.x2_ = point1.x;
//        bbox_gt.y2_ = point1.y;
//
//        tracker.Init(roiImg, bbox_gt, &regressor);
//        cout << "Started tracking" << endl;
//
//		roiImg.copyTo(mytemplate);
//		imshow("MOUSE roiImg", mytemplate);
//    }
//
//    if (event == CV_EVENT_LBUTTONUP)
//    {
//    	init_cond=true;
//    	select_flag = 1;
//        drag = 0;
//    }

}
/*
void track()
{

	const double Set_point_x=img.cols/2;
	double error_x = 0.0;

	const double Set_point_y=img.rows/2;
	double error_y = 0.0;

	static KalmanFilter KF(8, 14, 2);
	static Mat_<float> state(8, 1);
	static Mat processNoise(14, 1, CV_32F);
	static Mat_<float> measurement(14,1);
	static Mat_<float> prev_measurement(14,1); prev_measurement.setTo(Scalar(0));
	Mat prediction;
	int M_D_flag=0;

	if (!start)
	{

		//cout << "!start is false!" << endl;

		if (init_cond)
		{
			init_cond=false;

			Set_point_depth_TLD=sqrt(img.cols*img.rows)/sqrt(cftld_h*cftld_w);

			KF.statePre.at<float>(0) = Initialbox(0);
			KF.statePre.at<float>(1) =  Initialbox(1);
			KF.statePre.at<float>(2) =  Initialbox(2);
			KF.statePre.at<float>(3) =  Initialbox(3);
			KF.statePre.at<float>(4) =  0.0;
			KF.statePre.at<float>(5) =  0.0;
			KF.statePre.at<float>(6) =  0.0;
			KF.statePre.at<float>(7) =  0.0;

			KF.statePost.at<float>(0) =  Initialbox(0);
			KF.statePost.at<float>(1) =  Initialbox(1);
			KF.statePost.at<float>(2) =  Initialbox(2);
			KF.statePost.at<float>(3) =  Initialbox(3);
			KF.statePost.at<float>(4) =  0.0;
			KF.statePost.at<float>(5) =  0.0;
			KF.statePost.at<float>(6) =  0.0;
			KF.statePost.at<float>(7) =  0.0;


			KF.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
														 0,1,0,0,0,1,0,0,
														 0,0,1,0,0,0,1,0,
														 0,0,0,1,0,0,0,1,
														 0,0,0,0,1,0,0,0,
														 0,0,0,0,0,1,0,0,
														 0,0,0,0,0,0,1,0,
														 0,0,0,0,0,0,0,1);

			KF.controlMatrix = (Mat_<float>(8, 2) << 0,0,
													 0,0,
													 0,0,
													 0,0,
													 0.002,0,
													 0,0.002,
													 0,0,
													 0,0);

			KF.processNoiseCov =  (Mat_<float>(8, 8) <<  //8x8
					                                     0.6,0,0,0,0,0,0,0,
														 0,0.6,0,0,0,0,0,0,
														 0,0,0.6,0,0,0,0,0,
														 0,0,0,0.6,0,0,0,0,
														 0,0,0,0,0.6,0,0,0,
														 0,0,0,0,0,0.6,0,0,
														 0,0,0,0,0,0,0.6,0,
														 0,0,0,0,0,0,0,0.6);

			KF.measurementMatrix=  (Mat_<float>(14, 8) << 1,0,0,0,0,0,0,0, //14x8
														  1,0,0,0,0,0,0,0,
														  1,0,0,0,0,0,0,0,
														  1,0,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,0,1,0,0,0,0,0,
														  0,0,1,0,0,0,0,0,
														  0,0,1,0,0,0,0,0,
														  0,0,0,1,0,0,0,0,
														  0,0,0,1,0,0,0,0,
														  0,0,0,1,0,0,0,0);
		}

		cout << "I am in track "<<endl;
		cout << "***************************************************************" << endl;
		// The number of distances is n!/(n-1)

		Mat old_estimated = estimated;


		Mat local_TLD;
		mu2.lock();
		if (!estimated_TLD.empty())
			local_TLD = estimated_TLD;
		else
			local_TLD = estimated;
		mu2.unlock();

		Mat local_STRUCK;
		mu3.lock();
		if (!estimated_STRUCK.empty())
			local_STRUCK = estimated_STRUCK;
		else
			local_STRUCK = estimated;
		mu3.unlock();





		Mat local_GOTURN;
		//cout << estimated_GOTURN << endl;
		mu4.lock();
		if (!estimated_GOTURN.empty())
			local_GOTURN = estimated_GOTURN;
		else
			local_GOTURN = estimated;
		mu4.unlock();
//


		Mat local_CMT;
		mu1.lock();
		if (!estimated_CMT.empty())
			local_CMT = estimated_CMT;
		else
			local_CMT = estimated;
		mu1.unlock();


//		cout << "Crazy numbers!!!" << endl;
//
//		cout << local_TLD.at<float>(0) << endl;
//		cout << local_CMT.at<float>(0) << endl;
//		cout << local_TLD.at<float>(1) << endl;
//		cout << local_CMT.at<float>(1) << endl;
//		cout << local_TLD.at<float>(2) << endl;
//		cout << local_CMT.at<float>(2) << endl;
//		cout << local_TLD.at<float>(3) << endl;
//		cout << local_CMT.at<float>(3) << endl;
//		cout << "*****" << endl;



		double dist_TLD_CMT=distanceCalculate(local_TLD.at<float>(0),local_CMT.at<float>(0),local_TLD.at<float>(1),local_CMT.at<float>(1),local_TLD.at<float>(2),local_CMT.at<float>(2),local_TLD.at<float>(3),local_CMT.at<float>(3));
		cout << "dist_TLD_CMT "<< dist_TLD_CMT <<endl;
		double dist_TLD_ST=distanceCalculate(local_TLD.at<float>(0),local_STRUCK.at<float>(0),local_TLD.at<float>(1),local_STRUCK.at<float>(1),local_TLD.at<float>(2),local_STRUCK.at<float>(2),local_TLD.at<float>(3),local_STRUCK.at<float>(3));
		double dist_ST_CMT=distanceCalculate(local_CMT.at<float>(0),local_STRUCK.at<float>(0),local_CMT.at<float>(1),local_STRUCK.at<float>(1),local_CMT.at<float>(2),local_STRUCK.at<float>(2),local_CMT.at<float>(3),local_STRUCK.at<float>(3));

		double dist_GOTURN_CMT=distanceCalculate(local_CMT.at<float>(0),local_GOTURN.at<float>(0),local_CMT.at<float>(1),local_GOTURN.at<float>(1),local_CMT.at<float>(2),local_GOTURN.at<float>(2),local_CMT.at<float>(3),local_GOTURN.at<float>(3));
		double dist_GOTURN_TLD=distanceCalculate(local_TLD.at<float>(0),local_GOTURN.at<float>(0),local_TLD.at<float>(1),local_GOTURN.at<float>(1),local_TLD.at<float>(2),local_GOTURN.at<float>(2),local_TLD.at<float>(3),local_GOTURN.at<float>(3));
		double dist_GOTURN_ST=distanceCalculate(local_GOTURN.at<float>(0),local_STRUCK.at<float>(0),local_GOTURN.at<float>(1),local_STRUCK.at<float>(1),local_GOTURN.at<float>(2),local_STRUCK.at<float>(2),local_GOTURN.at<float>(3),local_STRUCK.at<float>(3));



		cout << "dist_TLD_ST "<< dist_TLD_ST <<endl;
		cout << "dist_ST_CMT "<< dist_ST_CMT <<endl;
		cout << "dist_GOTURN_CMT "<< dist_GOTURN_CMT <<endl;


		//TODO: use a better variable name
//		double A = min(dist_TLD_ST,dist_GOTURN_TLD);
//		TLD_min=min(dist_TLD_CMT,A);
		//CMT_min=min(dist_TLD_CMT,min(dist_ST_CMT,dist_GOTURN_CMT));
		//STRUCK_min=min(dist_ST_CMT,min(dist_TLD_ST,dist_GOTURN_ST));
		//GOTURN_min=min(dist_GOTURN_CMT,min(dist_GOTURN_TLD,dist_GOTURN_ST));

		TLD_min=min(dist_TLD_CMT,(min(dist_TLD_ST,dist_GOTURN_TLD)));
		CMT_min=min(dist_TLD_CMT,(min(dist_ST_CMT,dist_GOTURN_CMT)));
		STRUCK_min=min(dist_ST_CMT,(min(dist_TLD_ST,dist_GOTURN_ST)));
		GOTURN_min=min(dist_GOTURN_CMT,(min(dist_GOTURN_TLD,dist_GOTURN_ST)));

		//cout << "TLD_min " << TLD_min << endl;
		//cout << "CMT_min " << CMT_min << endl;
		//cout << "STRUCK_min " << STRUCK_min << endl;
		//cout << "GOTURN min " << GOTURN_min << endl;

		cout << "***************************************************************" << endl;


		//TODO: use a better variable name
		TLD_min = 10 + 4000*(1+tanh((TLD_min-100)/10))/2;
		CMT_min = 10 + 4000*(1+tanh((CMT_min-100)/10))/2;
		STRUCK_min = 10 + 4000*(1+tanh((STRUCK_min-80)/10))/2;
		GOTURN_min = 10 + 4000*(1+tanh((GOTURN_min-90)/10))/2;

		//cout << "TLD_min " << TLD_min << endl;
		//cout << "CMT_min " << CMT_min << endl;
		//cout << "STRUCK_min " << STRUCK_min << endl;
		//cout << "GOTURN_min " << GOTURN_min << endl;
//		cout << "TLD_min " << TLD_min << endl;
//		cout << "CMT_min " << CMT_min << endl;
//		cout << "STRUCK_min " << STRUCK_min << endl;



		cout << "***************************************************************" << endl;


//
//		cout<<"TLD_Con :"<<(1/confidence_TLD)<<endl;

//		cout<<"KF_C: "<<KF.measurementNoiseCov<<endl;
		//---TLD_x
		measurement(0) =  local_TLD.at<float>(0);
		//---CMT_x
		measurement(1) =  local_CMT.at<float>(0);
		//---STRUCK_x
		measurement(2) =  local_STRUCK.at<float>(0);
		//---GOTURN_x
		measurement(3) =  local_GOTURN.at<float>(0);

		//---TLD_y
		measurement(4) =  local_TLD.at<float>(1);
		//---CMT_y
		measurement(5) =  local_CMT.at<float>(1);
		//---STRUCK_y
		measurement(6) =  local_STRUCK.at<float>(1);
		//--GOTURN_y
		measurement(7) =  local_GOTURN.at<float>(1);

		//---TLD_h
		measurement(8) =  local_TLD.at<float>(2);
		//---CMT_h
		measurement(9) =  local_CMT.at<float>(2);
		//---GOTURN_h
		measurement(10) =  local_GOTURN.at<float>(2);



		//---TLD_w
		measurement(11) =  local_TLD.at<float>(3);
		//---CMT_w
		measurement(12) =  local_CMT.at<float>(3);
		//---GOTURN_w
		measurement(13) =  local_GOTURN.at<float>(3);
		//-----Provide the observation and iterate
		prediction = KF.predict(PID_control_matrix);

		//cout << "measurement " << measurement << endl;

		//cout<<"CONFIDENCE_GOTURN   "<<confidence_GOTURN<<endl;
		KF.measurementNoiseCov=  (Mat_<float>(14, 14) <<confidence_TLD+TLD_min,0,0,0,0,0,0,0,0,0,0,0,0,0,					//---C_TLD_x 		(0,0)
														 0,confidence_CMT+CMT_min,0,0,0,0,0,0,0,0,0,0,0,0,					//---C_CMT_x  		(1,1)
														 0,0,confidence_STRUCK+STRUCK_min,0,0,0,0,0,0,0,0,0,0,0,			//---C_STRUCK_x		(2,2)
														 0,0,0,(confidence_GOTURN+GOTURN_min),0,0,0,0,0,0,0,0,0,0,	        //---C_GOTURN_x     (3,3)

														 0,0,0,0,confidence_TLD+TLD_min,0,0,0,0,0,0,0,0,0,						//---C_TLD_y		(4,4)
														 0,0,0,0,0,confidence_CMT+CMT_min,0,0,0,0,0,0,0,0,						//---C_CMT_y		(5,5)
														 0,0,0,0,0,0,confidence_STRUCK+STRUCK_min,0,0,0,0,0,0,0,			    //---C_STRUCK_y		(6,6)
														 0,0,0,0,0,0,0,(confidence_GOTURN+GOTURN_min),0,0,0,0,0,0,				//---C_GOTURN_y		(7,7)

														 0,0,0,0,0,0,0,0,confidence_TLD+TLD_min,0,0,0,0,0,					    //---C_TLD_h		(8,8)
														 0,0,0,0,0,0,0,0,0,(confidence_CMT+CMT_min),0,0,0,0,					//---C_CMT_h		(9,9)
														 0,0,0,0,0,0,0,0,0,0,(confidence_GOTURN+GOTURN_min),0,0,0,				//---C_GOTURN_h		(10,10)

														 0,0,0,0,0,0,0,0,0,0,0,confidence_TLD+TLD_min,0,0,					//---C_TLD_w		(11,11)
														 0,0,0,0,0,0,0,0,0,0,0,0,(confidence_CMT+CMT_min),0,                //---C_CMT_w		(12,12)
														 0,0,0,0,0,0,0,0,0,0,0,0,0,(confidence_GOTURN+GOTURN_min));			//---C_GOTURN_w		(13,13)


//

		              cout << "KF.measurementNoiseCov = " << KF.measurementNoiseCov << endl;




		estimated=KF.correct(measurement);
		//cout << "Estimated " << estimated <<endl;
//		cout<<"KG :"<<KF.gain<<endl;

//		cout<<"tld :"<< prediction.at<float>(0)<<"    "<<measurement(0)<<endl;
//		cout<<"tld_e :"<< prediction.at<float>(0)-measurement(0)<<endl;;
//		cout<<"cmt_e :"<< prediction.at<float>(1)-measurement(1)<<endl;;
//		cout<<"struck_e :"<< prediction.at<float>(2)-measurement(2)<<endl;;


#ifdef D_acq




#endif

		//----Estimated fusion
		rectangle( img, Point((int)(estimated.at<float>(0)- estimated.at<float>(3)/2) ,(int)(estimated.at<float>(1)- estimated.at<float>(2)/2)), Point((int)(estimated.at<float>(3)/2 + estimated.at<float>(0)),(int)(estimated.at<float>(2)/2 + estimated.at<float>(1))), Scalar(0,255,255), 3 );
		//Center of the target
		circle(img, Point(estimated.at<float>(0),estimated.at<float>(1)), 5,Scalar(0,255,255), 3);
		//Line pointing the center
		//arrowedLine( img,Point(estimated.at<float>(0),estimated.at<float>(1)),Point(img.cols/2, img.rows/2 ),color,  2, 8,0,0.05 );

		//upper part
		//line(img, Point(estimated.at<float>(0)- estimated.at<float>(3)/2 ,estimated.at<float>(1)- estimated.at<float>(2)/2), Point(estimated.at<float>(0)-rect.width/2,estimated.at<float>(1)-rect.height/2),color, 3);
		//line(img, Point(estimated.at<float>(0)+ estimated.at<float>(3)/2 ,estimated.at<float>(1)- estimated.at<float>(2)/2), Point(estimated.at<float>(0)+rect.width/2,estimated.at<float>(1)-rect.height/2),color, 3);

		//lower part
		//line(img, Point(estimated.at<float>(0)- estimated.at<float>(3)/2 ,estimated.at<float>(1)+ estimated.at<float>(2)/2), Point(estimated.at<float>(0)-rect.width/2,estimated.at<float>(1)+rect.height/2),color, 3);
		//line(img, Point(estimated.at<float>(0)+ estimated.at<float>(3)/2 ,estimated.at<float>(1)+ estimated.at<float>(2)/2), Point(estimated.at<float>(0)+rect.width/2,estimated.at<float>(1)+rect.height/2),color, 3);




	}

}
*/
void track()
{

	const double Set_point_x=img.cols/2;
	double error_x = 0.0;

	const double Set_point_y=img.rows/2;
	double error_y = 0.0;

	static KalmanFilter KF(8, 10, 2);
	static Mat_<float> state(8, 1);
	static Mat processNoise(8, 1, CV_32F);
	static Mat_<float> measurement(10,1);
	static Mat_<float> prev_measurement(10,1); prev_measurement.setTo(Scalar(0));
	Mat prediction;
	int M_D_flag=0;

	if (!start)
	{
		if (init_cond)
		{
			init_cond=false;

			Set_point_depth_TLD=sqrt(img.cols*img.rows)/sqrt(cftld_h*cftld_w);

			KF.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
														 0,1,0,0,0,1,0,0,
														 0,0,1,0,0,0,1,0,
														 0,0,0,1,0,0,0,1,
														 0,0,0,0,1,0,0,0,
														 0,0,0,0,0,1,0,0,
														 0,0,0,0,0,0,1,0,
														 0,0,0,0,0,0,0,1);

			KF.controlMatrix = (Mat_<float>(8, 2) << 0,0,
													 0,0,
													 0,0,
													 0,0,
													 0.002,0,
													 0,0.002,
													 0,0,
													 0,0);

			KF.processNoiseCov =  (Mat_<float>(8, 8) <<  0,0,0,0,0,0,0,0,
														 0,0,0,0,0,0,0,0,
														 0,0,0,0,0,0,0,0,
														 0,0,0,0,0,0,0,0,
														 0,0,0,0,0.6,0,0,0,
														 0,0,0,0,0,0.6,0,0,
														 0,0,0,0,0,0,0.6,0,
														 0,0,0,0,0,0,0,0.6);

			KF.measurementMatrix=  (Mat_<float>(10, 8) << 1,0,0,0,0,0,0,0,
														  1,0,0,0,0,0,0,0,
														  1,0,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,0,1,0,0,0,0,0,
														  0,0,1,0,0,0,0,0,
														  0,0,0,1,0,0,0,0,
														  0,0,0,1,0,0,0,0);
		}


		cout << "***************************************************************" << endl;
		Mat old_estimated = estimated;


		Mat local_TLD;
		mu2.lock();
		if (!estimated_TLD.empty())
			local_TLD = estimated_TLD;
		else
			local_TLD = estimated;
		mu2.unlock();

		Mat local_STRUCK;
		mu3.lock();
		if (!estimated_STRUCK.empty())
			local_STRUCK = estimated_STRUCK;
		else
			local_STRUCK = estimated;
		mu3.unlock();



		Mat local_CMT;
		mu1.lock();
		if (!estimated_CMT.empty())
			local_CMT = estimated_CMT;
		else
			local_CMT = estimated;
		mu1.unlock();



		double dist_TLD_CMT=distanceCalculate(local_TLD.at<float>(0),local_CMT.at<float>(0),local_TLD.at<float>(1),local_CMT.at<float>(1),local_TLD.at<float>(2),local_CMT.at<float>(2),local_TLD.at<float>(3),local_CMT.at<float>(3));
		double dist_TLD_ST=distanceCalculate(local_TLD.at<float>(0),local_STRUCK.at<float>(0),local_TLD.at<float>(1),local_STRUCK.at<float>(1),local_TLD.at<float>(2),local_STRUCK.at<float>(2),local_TLD.at<float>(3),local_STRUCK.at<float>(3));
		double dist_ST_CMT=distanceCalculate(local_CMT.at<float>(0),local_STRUCK.at<float>(0),local_CMT.at<float>(1),local_STRUCK.at<float>(1),local_CMT.at<float>(2),local_STRUCK.at<float>(2),local_CMT.at<float>(3),local_STRUCK.at<float>(3));


		//TODO: use a better variable name
		TLD_min=min(dist_TLD_CMT,dist_TLD_ST);
		CMT_min=min(dist_TLD_CMT,dist_ST_CMT);
		STRUCK_min=min(dist_ST_CMT,dist_TLD_ST);

		cout << "TLD_min " << TLD_min << endl;
		cout << "CMT_min " << CMT_min << endl;
		cout << "STRUCK_min " << STRUCK_min << endl;


		cout << "***************************************************************" << endl;


		//TODO: use a better variable name
		TLD_min = 10 + 4000*(1+tanh((TLD_min-100)/10))/2;
		CMT_min = 10 + 4000*(1+tanh((CMT_min-100)/10))/2;
		STRUCK_min = 10 + 4000*(1+tanh((STRUCK_min-80)/10))/2;

		cout << "TLD_min " << TLD_min << endl;
		cout << "CMT_min " << CMT_min << endl;
		cout << "STRUCK_min " << STRUCK_min << endl;

//		cout << "TLD_min " << TLD_min << endl;
//		cout << "CMT_min " << CMT_min << endl;
//		cout << "STRUCK_min " << STRUCK_min << endl;



		cout << "***************************************************************" << endl;


//
//		cout<<"TLD_Con :"<<(1/confidence_TLD)<<endl;

//		cout<<"KF_C: "<<KF.measurementNoiseCov<<endl;
		//---TLD_x
		measurement(0) =  local_TLD.at<float>(0);
		//---CMT_x
		measurement(1) =  local_CMT.at<float>(0);
		//---STRUCK_x
		measurement(2) =  local_STRUCK.at<float>(0);
		//---TLD_y
		measurement(3) =  local_TLD.at<float>(1);
		//---CMT_y
		measurement(4) =  local_CMT.at<float>(1);
		//---STRUCK_y
		measurement(5) =  local_STRUCK.at<float>(1);
		//---TLD_h
		measurement(6) =  local_TLD.at<float>(2);
		//---CMT_h
		measurement(7) =  local_CMT.at<float>(2);
		//---TLD_w
		measurement(8) =  local_TLD.at<float>(3);
		//---CMT_h
		measurement(9) =  local_CMT.at<float>(3);

		//-----Provide the observation and iterate
		prediction = KF.predict(PID_control_matrix);


		KF.measurementNoiseCov=  (Mat_<float>(10, 10) << confidence_TLD+TLD_min,0,0,0,0,0,0,0,0,0,					//---C_TLD_x 		(0,0)
														 0,confidence_CMT+CMT_min,0,0,0,0,0,0,0,0,					//---C_CMT_x  		(1,1)
														 0,0,confidence_STRUCK+STRUCK_min,0,0,0,0,0,0,0,			//---C_STRUCK_x		(2,2)
														 0,0,0,confidence_TLD+TLD_min,0,0,0,0,0,0,					//---C_TLD_y		(3,3)
														 0,0,0,0,confidence_CMT+CMT_min,0,0,0,0,0,					//---C_CMT_y		(4,4)
														 0,0,0,0,0,confidence_STRUCK+STRUCK_min,0,0,0,0,			//---C_STRUCK_y		(5,5)
														 0,0,0,0,0,0,confidence_TLD+TLD_min,0,0,0,					//---C_TLD_h		(6,6)
														 0,0,0,0,0,0,0,(confidence_CMT+CMT_min),0,0,				//---C_CMT_h		(7,7)
														 0,0,0,0,0,0,0,0,confidence_TLD+TLD_min,0,					//---C_TLD_w		(8,8)
														 0,0,0,0,0,0,0,0,0,(confidence_CMT+CMT_min));			//---C_CMT_w		(9,9)


		cout << "KF.measurementNoiseCov = " << KF.measurementNoiseCov << endl;




		estimated=KF.correct(measurement);
//		cout<<"KG :"<<KF.gain<<endl;

//		cout<<"tld :"<< prediction.at<float>(0)<<"    "<<measurement(0)<<endl;
//		cout<<"tld_e :"<< prediction.at<float>(0)-measurement(0)<<endl;;
//		cout<<"cmt_e :"<< prediction.at<float>(1)-measurement(1)<<endl;;
//		cout<<"struck_e :"<< prediction.at<float>(2)-measurement(2)<<endl;;


#ifdef D_acq




#endif

		//----Estimated fusion
		rectangle( img, Point((int)(estimated.at<float>(0)- estimated.at<float>(3)/2) ,(int)(estimated.at<float>(1)- estimated.at<float>(2)/2)), Point((int)(estimated.at<float>(3)/2 + estimated.at<float>(0)),(int)(estimated.at<float>(2)/2 + estimated.at<float>(1))), Scalar(0,255,255), 3 );
		//Center of the target
		circle(img, Point(estimated.at<float>(0),estimated.at<float>(1)), 5,Scalar(0,255,255), 3);
		//Line pointing the center
		//arrowedLine( img,Point(estimated.at<float>(0),estimated.at<float>(1)),Point(img.cols/2, img.rows/2 ),color,  2, 8,0,0.05 );

		//upper part
		//line(img, Point(estimated.at<float>(0)- estimated.at<float>(3)/2 ,estimated.at<float>(1)- estimated.at<float>(2)/2), Point(estimated.at<float>(0)-rect.width/2,estimated.at<float>(1)-rect.height/2),color, 3);
		//line(img, Point(estimated.at<float>(0)+ estimated.at<float>(3)/2 ,estimated.at<float>(1)- estimated.at<float>(2)/2), Point(estimated.at<float>(0)+rect.width/2,estimated.at<float>(1)-rect.height/2),color, 3);

		//lower part
		//line(img, Point(estimated.at<float>(0)- estimated.at<float>(3)/2 ,estimated.at<float>(1)+ estimated.at<float>(2)/2), Point(estimated.at<float>(0)-rect.width/2,estimated.at<float>(1)+rect.height/2),color, 3);
		//line(img, Point(estimated.at<float>(0)+ estimated.at<float>(3)/2 ,estimated.at<float>(1)+ estimated.at<float>(2)/2), Point(estimated.at<float>(0)+rect.width/2,estimated.at<float>(1)+rect.height/2),color, 3);




	}

}
void init_proccess()
{
    start = !start;

    cout << "start is " << start << endl;

	if(start)
	   cout << "Started tracking" << endl;
	else {
	  init_cond=true;
	  cout << "Stopped tracking" << endl;
	}
}

int main(int argc, char *argv[])
{
	//---local variables and declarations for main
	//TODO: If optimization is on. Time has increase to allow the entrace in thread
	long delay_time=2;
	//CMT_x_ratio=640.0/360.0;
	//CMT_y_ratio=480.0/240.0;
	CMT_x_ratio=1;
	CMT_y_ratio=1;
//	CMT_x_ratio=640.0/360.0;
//	CMT_y_ratio=480.0/240.0;



	//VOT vot; // Initialize the communcation

    //VOTRegion region = vot.region(); // Get region and first frame
    //string path = vot.frame();

	//



#ifdef D_acq
					//data_t.open ( "ME_4.txt", ios::out);
					data_t.open ("/home/yevgeniy/Desktop/OTBResultsMarch72017/Output.txt");
					data_t <<"Tl_x_cFTLD"<<'\t'
						   <<"Tl_y_cFTLD"<<'\t'
						   <<"w_cFTLD"<<'\t'
						   <<"h_cFTLD"<<'\t'
						   <<"dx_cfTLD"<<'\t'//Added by Yev
						   <<"dy_cfTLD"<<'\t'//Added by Yev
						   <<"dw_cfTLD"<<'\t'//Added by Yev
						   <<"dh_cfTLD"<<'\t'//Added by Yev
						   <<"Tl_x_CMT"<<'\t'
						   <<"Tl_y_CMT"<<'\t'
						   <<"w_CMT"<<'\t'
						   <<"h_CMT"<<'\t'
						   <<"dx_CMT"<<'\t'//Added by Yev
						   <<"dy_CMT"<<'\t'//Added by Yev
						   <<"dw_CMT"<<'\t'//Added by Yev
						   <<"dh_CMT"<<'\t'//Added by Yev
						   <<"Tl_x_ST"<<'\t'
						   <<"Tl_y_ST"<<'\t'
						   <<"w_ST"<<'\t'
						   <<"h_ST"<<'\t'
						   <<"dx_ST"<<'\t'//Added by Yev
						   <<"dy_ST"<<'\t'//Added by Yev
						   <<"dw_ST"<<'\t'//Added by Yev
						   <<"dh_ST"<<'\t'//Added by Yev
						   //GOTURN
						   //<<"Tl_x_GT"<<'\t'
						   //<<"Tl_y_GT"<<'\t'
						   //<<"w_GT"<<'\t'
						   //<<"h_GT"<<'\t'
						   //<<"dx_GT"<<'\t'//Added by Yev
						   //<<"dy_GT"<<'\t'//Added by Yev
						   //<<"dw_GT"<<'\t'//Added by Yev
						   //<<"dh_GT"<<'\t'//Added by Yev
						   <<"Tl_x"<<'\t'
						   <<"Tl_y"<<'\t'
						   <<"w"<<'\t'
						   <<"h"<<'\t'
						   <<"cFTLD_cov"<<'\t'
						   <<"CMT_cov"<<'\t'
						   <<"STRUCK_cov"<<endl;
#endif




	//TODO: Check which of this variables can be removed
	cftld = new TLD();
	cftld->init(true);
	cftld->learning=true;
	cftld->detectorEnabled=true;
	cftld->learningEnabled=true;
	cftld->detectorCascade->varianceFilter->enabled = true;
	cftld->detectorCascade->ensembleClassifier->enabled = true;
	cftld->detectorCascade->nnClassifier->enabled = true;

 	//Struck

	if (conf.features.size() == 0)
	{
		cout << "error: no features specified in config" << endl;
		exit(-1);
	}




	//namedWindow( "camera", WINDOW_AUTOSIZE );
	//cvSetMouseCallback("camera", mouseHandler, NULL );





	//Read argv[1]
	char fname[256];
	char fnameappend[] = "%04d.jpg";
	std::sprintf(fname,"/home/yevgeniy/Desktop/OTBResultsMarch72017/%s/img/",argv[1]);
    char * newArray = new char[std::strlen(fname)+std::strlen(fnameappend)+1];
    std::strcpy(newArray,fname);
    std::strcat(newArray,fnameappend);
	cout << newArray <<endl;
	//cap.open(newArray);

	//Read argv[1] > For Output File
	char fname2[256];
	char fnameappend2[] = "Test/%05d.jpg";
	std::sprintf(fname2,"/home/yevgeniy/Desktop/AnImages/%s",argv[1]);
	char * newArray2 = new char[std::strlen(fname2)+std::strlen(fnameappend2)+1];
    std::strcpy(newArray2,fname2);
    std::strcat(newArray2,fnameappend2);
	//"/home/yevgeniy/Desktop/OTBResultsMarch72017/AnImages/DavidTest/%05d.jpg"
	cout<< newArray2 << endl;

	//Read argv[1] > For Annotated Output File
	char fname3[256];
	char fnameappend3[] = "TestAn/%05d.jpg";
	std::sprintf(fname3,"/home/yevgeniy/Desktop/AnImages/%s",argv[1]);
	char * newArray3 = new char[std::strlen(fname3)+std::strlen(fnameappend3)+1];
    std::strcpy(newArray3,fname3);
    std::strcat(newArray3,fnameappend3);
	//"/home/yevgeniy/Desktop/OTBResultsMarch72017/AnImages/DavidTest/%05d.jpg"
	cout<< newArray3 << endl;

	//BoundingBox argv[2] = x, argv[3] = y....
	int x = atoi(argv[2]);
	int y = atoi(argv[3]);
	int w = atoi(argv[4]);
	int h = atoi(argv[5]);

	cout<<"x,y,h,w"<< x << " " <<y  << " " <<h  << " " <<w<<endl;

	cv::VideoCapture cap;
	cap.open(newArray);
	//cap.open("/home/yevgeniy/Desktop/OTBResultsMarch72017/David/img/David.mp4");
	//cap.open("/home/yevgeniy/Desktop/OTBResultsMarch72017/David/img/%04d.jpg");
	//cap.open("/home/yevgeniy/Desktop/OTBResultsMarch72017/Basketball/img/bball.avi");
	cout << "I can open file!" << endl;
	int i = 0;

	//cap.open(0); //For capturing video real time.
	//cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);//normal 640
	//cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);//normal 480


	if ( !cap.isOpened() )
	 {   cout << "Unable to open video file" << endl;    return -1;    }

	cap >> img;
//	Mat nimg;
//	flip(img, nimg, 0);
//	img = nimg;
//	cap.retrieve(img);
//	cap.read(img);
//  i++;
	std::cerr << "initial cap" << cap.get(CV_CAP_PROP_FRAME_COUNT)<< std::endl;
	/// IF BOUNDINGBOX is already given. Initialize it here!
//	int x = 129;
//	int y = 80;
//	int w = 64; //These are the numbers for david
//	int h = 78;

	//Current - Crossing
//	int x = 205;
//	int y = 151;
//	int w = 17;
//	int h = 50;


	Point center;
	center.x = x;
	center.y = y;
	point1.x = x+w;
	point1.y = y+h;
	point2.x = x;
	point2.y = y;
//	Mat img1;
//	img.copyTo(img1);
	//circle(img1, center, 5, CV_RGB(255, 255, 255), FILLED, LINE_8);
	//rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
	//imshow("habibi", img1);
	cout << "I got here :)" << endl;

	rect = cv::Rect(x, y,w, h);
	rect_CMT=cv::Rect(point1.x/CMT_x_ratio, point1.y/CMT_y_ratio, (w)/CMT_x_ratio, (h)/CMT_y_ratio);
	drag = 0;
//	roiImg=img(rect);//This is the image in the bounding box
//	//imshow("HABIBI",roiImg);
//
//
//
//
//	roiImg.copyTo(mytemplate);
//	imshow("MOUSE roiImg", mytemplate);

	select_flag = 1;
	flag_detector_enable=!flag_detector_enable;






	Initialbox(0)=x+w/2;
	Initialbox(1)=y+h/2;
	Initialbox(2)=h;
	Initialbox(3)=w;

 	//-----Launch the threads before everything start
 	//std::thread GOTURN_T(GOTURN_THREAD);
	/*
	bool KF_init_GOTURN=false;
	static KalmanFilter KF_GOTURN(8, 4, 0);
	static Mat_<float> state_GOTURN(8, 1);
	static Mat processNoise_GOTURN(8, 1, CV_32F);
	static Mat_<float> measurement_GOTURN(4,1);
	static Mat_<float> prev_measurement_GOTURN(4,1);

	Mat prediction_GOTURN,Ht_GOTURN,Ree_GOTURN;
	color_GOTURN = PURPLE;
	*/

	std::thread TLD_T(TLD_THREAD);
 	std::thread CMT_T(CMT_THREAD);
 	std::thread STRUCK_T(STRUCK_THREAD);


 	//Need this
 	/*
 	//GO-TURN Initialization
	//BoundingBox bbox_gt;
	bbox_gt.x2_ = point1.x;
	bbox_gt.y2_ = point1.y;
	bbox_gt.x1_ = point2.x;
	bbox_gt.y1_ = point2.y;
	cout << "x1, y1 Bbox  " << bbox_gt.x1_ <<"  "<< bbox_gt.y1_ << endl;
	cout << "x2, y2 Bbox  " << bbox_gt.x2_ <<"  "<< bbox_gt.y2_ << endl;
	cout << "h, w Bbox  " << bbox_gt.get_height() <<"  "<< bbox_gt.get_width() << endl;
	*/

	//tracker.Init(roiImg, bbox_gt, &regressor);
	cout << "Started tracking" << endl;
	//tracker.Track(img1, &regressor, &bbox_estimate);
	//cout << "Bbox h and w " << bbox_estimate.get_height() <<"  "<< bbox_estimate.get_width() << endl;
	//return 1;


 	static bool go_fast=false;
 	char key;
 	//key = 'p';
 	data_t <<(int) (Initialbox(0))<<'\t'
 		   <<(int) (Initialbox(1))<<'\t'
 		   <<(int) (Initialbox(2))<<'\t'
 		   <<(int) (Initialbox(3))<<'\t'

 		   <<(double) (0)<<'\t'//ADDED by YEV
 		   <<(double) (0)<<'\t'
 		   <<(double) (0)<<'\t'
 		   <<(double) (0)<<'\t'

 		   <<(int) (Initialbox(0))<<'\t'
 		   <<(int) (Initialbox(1))<<'\t'
 		   <<(int) (Initialbox(2))<<'\t'
 		   <<(int) (Initialbox(3))<<'\t'

 		   <<(double) (0)<<'\t'//ADDED by Yev
 		   <<(double) (0)<<'\t'
 		   <<(double) (0)<<'\t'
 		   <<(double) (0)<<'\t'


 		   <<(int) (Initialbox(0))<<'\t'
 		   <<(int) (Initialbox(1))<<'\t'
 		   <<(int) (Initialbox(2))<<'\t'
 		   <<(int) (Initialbox(3))<<'\t'

 		   <<(double) (0)<<'\t'//ADDED by Yev
 		   <<(double) (0)<<'\t'
 		   <<(double) (0)<<'\t'
 		   <<(double) (0)<<'\t'

 		   //<<(int) (estimated_GOTURN.at<float>(0)- estimated_GOTURN.at<float>(3)/2)<<'\t'
 		   //<<(int) (estimated_GOTURN.at<float>(1)- estimated_GOTURN.at<float>(2)/2)<<'\t'
 		   //<<(int) (estimated_GOTURN.at<float>(3))<<'\t'
 		   //<<(int) (estimated_GOTURN.at<float>(2))<<'\t'

 		   //<<(double) (estimated_GOTURN.at<float>(4))<<'\t'//ADDED by Yev
 		   //<<(double) (estimated_GOTURN.at<float>(5))<<'\t'
 		   //<<(double) (estimated_GOTURN.at<float>(7))<<'\t'
 		   //<<(double) (estimated_GOTURN.at<float>(6))<<'\t'


 		   <<(int) (Initialbox(0))<<'\t'
 		   <<(int) (Initialbox(0))<<'\t'
 		   <<(int) (Initialbox(0))<<'\t'
 		   <<(int) (Initialbox(0))<<'\t'
 		   <<10<<'\t'
 		   <<10<<'\t'
 		   <<10<<endl;





 	//sleep(1.6);
	while (1) {



		//key = cv::waitKey(delay_time);
		//cap >> img;
		cap.retrieve(img);
		cap.read(img);
//		Mat nimg;
//		flip(img, nimg, 0);
//		img = nimg;


		i++;
		if (!((img.size().width > 0) & (img.size().height > 0))) {
			std::cerr << "Something is wrong with the JPEG, could not get frame "<< cap.get(CAP_PROP_POS_AVI_RATIO)<< std::endl;
			std::cerr << "Something is wrong with the JPEG, could not get frame "<< i << std::endl;
			break;

		}


		Mat img_sv_c=img.clone();

        /*
    	  TODO: Check if Copying this images are OK. It might avoid data races
		  inside of the of the threads.
	  	*/
		mu2.try_lock();
		img.copyTo(img_cftld);
		mu2.unlock();


		//mu4.try_lock();
		//img.copyTo(imageGOTURN);
		//mu4.unlock();

		//resize(img,CMT_image,CMT_image.size(),0,0,INTER_NEAREST);
		cvtColor(img, grey, CV_BGR2GRAY);
		//std::cin.ignore();


/*
		std::cerr << "flag_detector_enable  " << flag_detector_enable << std::endl;
		std::cerr << "select   " << flag_detector_enable << std::endl;
*/

        if(flag_detector_enable)
		{

        	if (select_flag == 1)
    		 {


        		rect = cv::Rect(x, y,w, h);
        		rect_CMT=cv::Rect(point1.x/CMT_x_ratio, point1.y/CMT_y_ratio, (x - point1.x)/CMT_x_ratio, (y - point1.y)/CMT_y_ratio);
        		drag = 0;
        		roiImg=img(rect);//This is the image in the bounding box
        		//imshow("HABIBI",roiImg);

    			//No print any information of CMT
    			FILELog::ReportingLevel() = logINFO;
    			//------TLD

    			mu2.lock();
    			cftld->selectObject(img, &rect);
    			mu2.unlock();

    			//------CMT
    			mu1.lock();
    			cmtobj.points_active.clear();
				cmtobj.initialize(grey,rect);
				CMT_init_points=cmtobj.points_active.size();
				mu1.unlock();

				mu3.lock();
				struck_1->Reset();
				FloatRect initBB_vot = FloatRect(rect.x,rect.y,rect.width,rect.height);
				struck_1->Initialise(grey, initBB_vot);
				mu3.unlock();


				//------GOTURN Initialize
				//tracker.Init(roiImg, bbox_gt, &regressor);
				std::cout << "You made it this far..." << endl;
				//tracker.Init(roiImg, bbox_gt, &regressor); //Need this
				//cout << "Started tracking" << endl;
				//tracker.Track(img1, &regressor, &bbox_estimate);
				//cout << "Bbox h and w " << bbox_estimate.get_height() <<"  "<< bbox_estimate.get_width() << endl; //Need this
				//imshow("camera1", roiImg);
				//cout << "BOUNDING BOX  " << bbox_gt.x1_ <<" "<<bbox_gt.y1_ << endl;


				select_flag = 2;

				//start = false;
				//init_proccess();
				//cout << "init_process called" << endl;
				//init_cond=true;
				key ='t';

			 	data_t <<(int) (Initialbox(0))<<'\t'
			 		   <<(int) (Initialbox(1))<<'\t'
			 		   <<(int) (Initialbox(2))<<'\t'
			 		   <<(int) (Initialbox(3))<<'\t'

			 		   <<(double) (0)<<'\t'//ADDED by YEV
			 		   <<(double) (0)<<'\t'
			 		   <<(double) (0)<<'\t'
			 		   <<(double) (0)<<'\t'

			 		   <<(int) (Initialbox(0))<<'\t'
			 		   <<(int) (Initialbox(1))<<'\t'
			 		   <<(int) (Initialbox(2))<<'\t'
			 		   <<(int) (Initialbox(3))<<'\t'

			 		   <<(double) (0)<<'\t'//ADDED by Yev
			 		   <<(double) (0)<<'\t'
			 		   <<(double) (0)<<'\t'
			 		   <<(double) (0)<<'\t'


			 		   <<(int) (Initialbox(0))<<'\t'
			 		   <<(int) (Initialbox(1))<<'\t'
			 		   <<(int) (Initialbox(2))<<'\t'
			 		   <<(int) (Initialbox(3))<<'\t'

			 		   <<(double) (0)<<'\t'//ADDED by Yev
			 		   <<(double) (0)<<'\t'
			 		   <<(double) (0)<<'\t'
			 		   <<(double) (0)<<'\t'

			 		   //<<(int) (estimated_GOTURN.at<float>(0)- estimated_GOTURN.at<float>(3)/2)<<'\t'
			 		   //<<(int) (estimated_GOTURN.at<float>(1)- estimated_GOTURN.at<float>(2)/2)<<'\t'
			 		   //<<(int) (estimated_GOTURN.at<float>(3))<<'\t'
			 		   //<<(int) (estimated_GOTURN.at<float>(2))<<'\t'

			 		   //<<(double) (estimated_GOTURN.at<float>(4))<<'\t'//ADDED by Yev
			 		   //<<(double) (estimated_GOTURN.at<float>(5))<<'\t'
			 		   //<<(double) (estimated_GOTURN.at<float>(7))<<'\t'
			 		   //<<(double) (estimated_GOTURN.at<float>(6))<<'\t'


			 		   <<(int) (Initialbox(0))<<'\t'
			 		   <<(int) (Initialbox(0))<<'\t'
			 		   <<(int) (Initialbox(0))<<'\t'
			 		   <<(int) (Initialbox(0))<<'\t'
			 		   <<10<<'\t'
			 		   <<10<<'\t'
			 		   <<10<<endl;






    		 }

        	else if(select_flag == 2)
        	{
        		/*
        		BoundingBox result;
				//BoundingBox bbox_estimate;
				//double e1 = cv::getTickCount();


				tracker.Track(imageGOTURN, &regressor, &bbox_estimate);
				//double e2 = cv::getTickCount();
				//double timetrack = (e2 - e1)/ (cv::getTickFrequency());
				//cout << timetrack << endl;
				Point matchGOTURN;
				result = bbox_estimate;

				matchGOTURN.x = result.get_center_x();
				matchGOTURN.y = result.get_center_y();

				GOTURN_x=matchGOTURN.x;
				GOTURN_y=matchGOTURN.y;
				//cout << GOTURN_x << "  " << GOTURN_y << endl;
				GOTURN_w=result.get_width();
				GOTURN_h=result.get_height();
				//cout << "GOTURN Vals " << GOTURN_w<< " " << GOTURN_h << " " << endl;
				//break;
				//mu4.unlock();
				//time_t end = time(0);
				//double time = difftime(end, start) * 1000.0;

				//TODO: Make sure to release the memumory or lock the variable

				//mu4.lock();
				if(KF_init_GOTURN==false)
				{
					KF_init_GOTURN=true;

					//initialize previous state
				    KF_GOTURN.statePost.at<float>(0) = x+w/2; //State x
				    KF_GOTURN.statePost.at<float>(1) = y+h/2; //State y
				    KF_GOTURN.statePost.at<float>(2) = h; //State Vx
				    KF_GOTURN.statePost.at<float>(3) = w; //State Vy


					KF_GOTURN.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																	0,1,0,0,0,1,0,0,
																	0,0,1,0,0,0,1,0,
																	0,0,0,1,0,0,0,1,
																	0,0,0,0,1,0,0,0,
																	0,0,0,0,0,1,0,0,
																	0,0,0,0,0,0,1,0,
																	0,0,0,0,0,0,0,1);

					KF_GOTURN.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0, //R
																	0,0.0001,0,0,0,0,0,0,
																	0,0,0.0001,0,0,0,0,0,
																	0,0,0,0.0001,0,0,0,0,
																	0,0,0,0,0.0008,0,0,0,
																	0,0,0,0,0,0.0008,0,0,
																	0,0,0,0,0,0,0.0008,0,
																	0,0,0,0,0,0,0,0.0008);

					KF_GOTURN.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																	 0,1,0,0,0,0,0,0,
																	 0,0,1,0,0,0,0,0,
																	 0,0,0,1,0,0,0,0);

					KF_GOTURN.measurementNoiseCov=  (Mat_<float>(4, 4) << 0.002,0,0,0, //Q
																	   0,0.004,0,0,
																	   0,0,0.002,0,
																	   0,0,0,0.004);



					}

				measurement_GOTURN(0) =  GOTURN_x;
				measurement_GOTURN(1) =  GOTURN_y;
				measurement_GOTURN(2) =  GOTURN_h;
				measurement_GOTURN(3) =  GOTURN_w;

				//cout << "GOTURN Vals " << GOTURN_x<< " " << GOTURN_y << " " << endl;

				prediction_GOTURN = KF_GOTURN.predict();

				transpose(KF_GOTURN.measurementMatrix, Ht_GOTURN);

				Ree_GOTURN= (KF_GOTURN.measurementMatrix*KF_GOTURN.errorCovPre*Ht_GOTURN)+KF_GOTURN.measurementNoiseCov;

				err_x_GOTURN=GOTURN_x-prediction_GOTURN.at<float>(0);
				err_y_GOTURN=GOTURN_y-prediction_GOTURN.at<float>(1);
				err_h_GOTURN=GOTURN_h-prediction_GOTURN.at<float>(2);
				err_w_GOTURN=GOTURN_w-prediction_GOTURN.at<float>(3);

				M_D_GOTURN=sqrt((pow(err_x_GOTURN,2)/Ree_GOTURN.at<float>(0,0))+(pow(err_y_GOTURN,2)/Ree_GOTURN.at<float>(1,1))+(pow(err_h_GOTURN,2)/Ree_GOTURN.at<float>(2,2))+(pow(err_w_GOTURN,2)/Ree_GOTURN.at<float>(3,3)));


			    confidence_GOTURN=(10/(1+exp(-M_D_GOTURN+10)));


				estimated_GOTURN=KF_GOTURN.correct(measurement_GOTURN);

				*/

				display_detectors();
				//track();

        	}
		}


        //-----Cases for exit
        if (key == 0x1b) break;



        //------Activate detectors

        if(key=='s')
        {
        	flag_detector_enable=!flag_detector_enable;
        }

        if(key=='p')
        {
        	cout << "kick push" << endl;
        }

        //------Switch cfTLD learning
        if(key == 'r')
        {
//            cftld->learningEnabled = !cftld->learningEnabled;
//            if(!cftld->learningEnabled) {cout<<"Learning Disable"<<endl;}
//            else{cout<<"Learning Enable"<<endl;}
        	go_fast=!go_fast;
        }
	     if (rect.width > 0 && rect.height >0 && !start)
	    {

		    track();
		    cout <<"just returned from track()" << endl;
		}

        if(key=='t')
        {
        	init_proccess();
        	init_cond=true;
        }

        key = 'p';


//     	//TODO: Check the size of the vectors, these have problems when the code is -O3
		//double toc = (cvGetTickCount()- tic) / cvGetTickFrequency();
		//toc = toc / 1000000;
		//float fps = 1 / toc;
		char FPS[50];
		//char FPS[60];

static char c_o[100];

if(!start)
{
#ifdef save_img
static int c_r=0;

if(c>=1)
{

 //if (estimated_TLD.empty() | estimated_CMT.empty() | estimated_STRUCK.empty() | estimated_GOTURN.empty())
	 //continue;

data_t <<(int) (estimated_TLD.at<float>(0)- estimated_TLD.at<float>(3)/2)<<'\t'
	   <<(int) (estimated_TLD.at<float>(1)- estimated_TLD.at<float>(2)/2)<<'\t'
	   <<(int) (estimated_TLD.at<float>(3))<<'\t'
	   <<(int) (estimated_TLD.at<float>(2))<<'\t'

	   <<(double) (estimated_TLD.at<float>(4))<<'\t'//ADDED by YEV
	   <<(double) (estimated_TLD.at<float>(5))<<'\t'
	   <<(double) (estimated_TLD.at<float>(7))<<'\t'
	   <<(double) (estimated_TLD.at<float>(6))<<'\t'

	   <<(int) (estimated_CMT.at<float>(0)- estimated_CMT.at<float>(3)/2)<<'\t'
	   <<(int) (estimated_CMT.at<float>(1)- estimated_CMT.at<float>(2)/2)<<'\t'
	   <<(int) (estimated_CMT.at<float>(3))<<'\t'
	   <<(int) (estimated_CMT.at<float>(2))<<'\t'

	   <<(double) (estimated_CMT.at<float>(4))<<'\t'//ADDED by Yev
	   <<(double) (estimated_CMT.at<float>(5))<<'\t'
	   <<(double) (estimated_CMT.at<float>(7))<<'\t'
	   <<(double) (estimated_CMT.at<float>(6))<<'\t'


	   <<(int) (estimated_STRUCK.at<float>(0)- estimated_STRUCK.at<float>(3)/2)<<'\t'
	   <<(int) (estimated_STRUCK.at<float>(1)- estimated_STRUCK.at<float>(2)/2)<<'\t'
	   <<(int) (estimated_STRUCK.at<float>(3))<<'\t'
	   <<(int) (estimated_STRUCK.at<float>(2))<<'\t'

	   <<(double) (estimated_STRUCK.at<float>(4))<<'\t'//ADDED by Yev
	   <<(double) (estimated_STRUCK.at<float>(5))<<'\t'
	   <<(double) (estimated_STRUCK.at<float>(7))<<'\t'
	   <<(double) (estimated_STRUCK.at<float>(6))<<'\t'

	   //<<(int) (estimated_GOTURN.at<float>(0)- estimated_GOTURN.at<float>(3)/2)<<'\t'
	   //<<(int) (estimated_GOTURN.at<float>(1)- estimated_GOTURN.at<float>(2)/2)<<'\t'
	   //<<(int) (estimated_GOTURN.at<float>(3))<<'\t'
	   //<<(int) (estimated_GOTURN.at<float>(2))<<'\t'

	   //<<(double) (estimated_GOTURN.at<float>(4))<<'\t'//ADDED by Yev
	   //<<(double) (estimated_GOTURN.at<float>(5))<<'\t'
	   //<<(double) (estimated_GOTURN.at<float>(7))<<'\t'
	   //<<(double) (estimated_GOTURN.at<float>(6))<<'\t'


	   <<(int) (estimated.at<float>(0)- estimated.at<float>(3)/2)<<'\t'
	   <<(int) (estimated.at<float>(1)- estimated.at<float>(2)/2)<<'\t'
	   <<(int) (estimated.at<float>(3))<<'\t'
	   <<(int) (estimated.at<float>(2))<<'\t'
	   <<confidence_TLD+TLD_min<<'\t'
	   <<confidence_CMT+CMT_min<<'\t'
	   <<confidence_STRUCK+STRUCK_min<<endl;

	//----For printing in the screen
	int pix_x=10, pix_y=470;
	char TestStr_1[20];
	sprintf(TestStr_1,"FRAME: %d", c_r );
	putText(img, TestStr_1, Point(pix_x,pix_y), CV_FONT_NORMAL, 1.5, Scalar(255,255,255),3,3);

	sprintf(c_o,newArray2,c_r);
	imwrite(c_o,img_sv_c);
	sprintf(c_o,newArray3,(c_r));
	imwrite(c_o,img);

	c_r++;
}
	c++;

//if(!start)
//{
//	c++;
//}

#endif
}



//		int pix_x=10, pix_y=440, pix_ofs=14;
//		char TestStr_1[20];
//		sprintf(TestStr_1,"Pitch : %lf", pan );
//		putText(img, TestStr_1, Point(pix_x,pix_y), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);
//
//		char TestStr_2[20];
//		sprintf(TestStr_2,"Roll : %lf", tilt );
//		putText(img, TestStr_2, Point(pix_x,pix_y+pix_ofs), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);
//
//		char TestStr_3[20];
//		sprintf(TestStr_3,"TLD-Con :  %lf", cftld->currConf );
//		putText(img, TestStr_3, Point(pix_x+440,20), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);
//
//		char TestStr_4[20];
//		if(cftld->learningEnabled)
//		{
//			sprintf(TestStr_4,"Learn : on");
//		}
//		else
//		{
//			sprintf(TestStr_4,"Learn : off");
//		}
//
//		putText(img, TestStr_4, Point(pix_x+440,20+pix_ofs), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);
//
//		sprintf(FPS,"FPS : %lf", fps );
//		putText(img, FPS, Point(10,20), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);
		//cout << "GOTURN X THINKS " << GOTURN_x << endl;
		//cout << "GOTURN Y THINKS " << GOTURN_y << endl;
		//cout << "GOTURN h THINKS " << GOTURN_h << endl;
		//cout << "GOTURN W THINKS " << GOTURN_w << endl;

		//if(code <= 3)
		//{
			//sleep(5);
			//code = code+1;
		//}
		//system("PAUSE");
		imshow("camera", img);
		if (i<=1)
			cv::waitKey(10000);
		else
			cv::waitKey(3000);

		//
		//moveWindow("camera", 10, 10);


    }

	data_t.close();
    return 0;

}

int struck_x=0;
int struck_y=0;
int struck_x_tl=0;
int struck_y_tl=0;
int struck_x_br=0;
int struck_y_br=0;
int struck_w=0;
int struck_h=0;

//NORMALLY IMAGES ARE 640,480 -> Im trying 360,240

//----Images and Matrices
Mat img(640,480,CV_8UC1);
Mat CMT_image(240, 360, CV_8UC1);
Mat grey;
Mat roiImg;
Mat mytemplate;
Mat img_cftld;
Mat estimated_TLD;
Mat estimated_CMT;
Mat estimated_STRUCK;
Mat_<float> PID_control_matrix(2,1);
//Mat estimated;
Mat estimated;
//Mat grey_CMT1;

//----Mutex for threads
std::mutex mu1;
std::mutex mu2;
std::mutex mu3;

//------BB colors
#define GREEN  cv::Scalar (0,255,0)
#define RED    cv::Scalar (0,0,255)
#define BLUE   cv::Scalar (255,0,0)
#define YELLOW cv::Scalar(0,125,255)
#define WHITE cv::Scalar(255,255,255)
//Added by Yev
#define PURPLE cv::Scalar(128,0,128)

//------BB colors for differen detectors
cv::Scalar color;
cv::Scalar color_TLD;
cv::Scalar color_CMT;
cv::Scalar color_STRUCK;
//Added by Yev
cv::Scalar color_GOTURN;

//------Declaration of variable for saving .txt files
ofstream data_t;
ofstream data_t_cov;
static int c=0;

//----Serial Port declaration (Arduino)
#ifdef serial_com
char serialPortFilename[] = "/dev/ttyACM0";
#endif


//--------GOTURN DECLARATIONS
const bool show_intermediate_output = false;
const string model_file  = "/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/nets/tracker.prototxt";
const string trained_file = "/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/nets/models/pretrained_model/tracker.caffemodel";
const string videos_folder = "/home/yevgeniy/Desktop/Datasets/alov300++_rectangleAnnotation_full";
const bool do_train = false;
int gpu_id = 0;
Regressor regressor(model_file, trained_file, gpu_id, do_train);
Tracker tracker(show_intermediate_output);
BoundingBox bbox_gt;
Mat imageGOTURN(640,480,CV_8UC3);
//----GOTURN display variables and calculation variables
int GOTURN_x=0;
int GOTURN_y=0;
int GOTURN_w=0;
int GOTURN_h=0;
double M_D_GOTURN=0.0;
double confidence_GOTURN=0.0;
double err_x_GOTURN=0.0;
double err_y_GOTURN=0.0;
double err_w_GOTURN=0.0;
double err_h_GOTURN=0.0;
Point GOTURN_br;
Point GOTURN_tl;
Mat estimated_GOTURN;
Point matchGOTURN;
BoundingBox result;
BoundingBox bbox_estimate;
//BoundingBox bbox_gt;
//---------------------------

struck_con::Tracker *struck_1;
string configPath = "config.txt";
Config conf(configPath);
struck_con::Tracker main_struck(conf);

int code;


// CODE ADDED
#define YELLOW cv::Scalar(0,125,255);
cv::Scalar color_Tracker;
std::mutex mu4; //For tracking thread
std::mutex mu5;




void tic() {
    tictoc_stack.push(clock());
}

void toc() {
    std::cout << "Time elapsed: "
              << ((double)(clock() - tictoc_stack.top())) / CLOCKS_PER_SEC
              << std::endl;
    tictoc_stack.pop();
}
double distanceCalculate(double x1, double x2, double y1, double y2,double h1, double h2, double w1, double w2)
{
    double x = x1 - x2;
    double y = y1 - y2;
    double h = h1 - h2;
    double w = w1 - w2;
    double dist;

    dist = pow(x,2)+pow(y,2)+pow(h,2)+pow(w,2);
    dist = sqrt(dist);

    return dist;
}



void rectangle(Mat& rMat, const FloatRect& rRect, const Scalar& rColour)
{
	IntRect r(rRect);
	rectangle(rMat, Point(r.XMin(), r.YMin()), Point(r.XMax(), r.YMax()), rColour);
}


void display_detectors()
{
//	 //-------TLD Display
//	 rectangle(img, cftld_tl, cftld_br, color_TLD, 3);
//	 circle(img, Point(cftld_x,cftld_y), 5, color_TLD, 3);
//	 //-------CMT Display
//	 circle(img, Point(cmt_x,cmt_y), 5, color_CMT, 3);
//	 rectangle(img, Point(cmt_x_tl,cmt_y_tl), Point(cmt_x_br,cmt_y_br), color_CMT, 3);


		//cout << "Crashed somewhere inside display_detectors() " << endl;

		color=color_TLD;
		if(estimated_TLD.empty()==false)
		{
			rectangle( img, Point(estimated_TLD.at<float>(0)- estimated_TLD.at<float>(3)/2 ,estimated_TLD.at<float>(1)- estimated_TLD.at<float>(2)/2), Point(estimated_TLD.at<float>(3)/2 + estimated_TLD.at<float>(0),  estimated_TLD.at<float>(2)/2 + estimated_TLD.at<float>(1)),color, 3 );
			circle(img, Point(estimated_TLD.at<float>(0),estimated_TLD.at<float>(1)), 5, color, 3);
		}
		color=color_CMT;
		if(estimated_CMT.empty()==false)
		{
			rectangle( img, Point(estimated_CMT.at<float>(0)- estimated_CMT.at<float>(3)/2 ,estimated_CMT.at<float>(1)- estimated_CMT.at<float>(2)/2), Point(estimated_CMT.at<float>(3)/2 + estimated_CMT.at<float>(0),  estimated_CMT.at<float>(2)/2 + estimated_CMT.at<float>(1)), color, 3 );
			circle(img, Point(estimated_CMT.at<float>(0),estimated_CMT.at<float>(1)), 5, color, 3);

			 //-------CMT Display Points
			for(size_t i = 0; i < cmtobj.points_active.size(); i++)
			{
				circle(img, Point(cmtobj.points_active[i].x,cmtobj.points_active[i].y),  1, color_CMT);
				target_frame.push_back(cmtobj.points_active[i]);
			}

		}
		color=color_STRUCK;
		if(estimated_STRUCK.empty()==false)
		{
			rectangle( img, Point(estimated_STRUCK.at<float>(0)- estimated_STRUCK.at<float>(3)/2 ,estimated_STRUCK.at<float>(1)- estimated_STRUCK.at<float>(2)/2), Point(estimated_STRUCK.at<float>(3)/2 + estimated_STRUCK.at<float>(0),  estimated_STRUCK.at<float>(2)/2 + estimated_STRUCK.at<float>(1)), color, 3 );
			circle(img, Point(estimated_STRUCK.at<float>(0),estimated_STRUCK.at<float>(1)), 5, color, 3);
		}

		color=color_GOTURN;
		if(estimated_GOTURN.empty()==false)
		{

			rectangle( img, Point(estimated_GOTURN.at<float>(0)- estimated_GOTURN.at<float>(3)/2 ,estimated_GOTURN.at<float>(1)- estimated_GOTURN.at<float>(2)/2), Point(estimated_GOTURN.at<float>(3)/2 + estimated_GOTURN.at<float>(0),  estimated_GOTURN.at<float>(2)/2 + estimated_GOTURN.at<float>(1)),color, 3 );
			circle(img, Point(estimated_GOTURN.at<float>(0),estimated_GOTURN.at<float>(1)), 5, color, 3);
		}

		color=color_Tracker;
		if(estimated.empty()==false)
		{
			//cout << " I MADE TO THIS TRACKER THANG "<< endl;
			rectangle( img, Point((int)(estimated.at<float>(0)- estimated.at<float>(3)/2) ,(int)(estimated.at<float>(1)- estimated.at<float>(2)/2)), Point((int)(estimated.at<float>(3)/2 + estimated.at<float>(0)),(int)(estimated.at<float>(2)/2 + estimated.at<float>(1))), Scalar(0,255,255), 3 );
			//Center of the target
			circle(img, Point(estimated.at<float>(0),estimated.at<float>(1)), 5, color, 3);
		}

		//cout << "Crashed somewhere outside display_detectors() " << endl;

}
void TLD_THREAD()
{

	//TODO: ALl this variables can be declared in a different file.
	bool KF_init_TLD=false;
	static KalmanFilter KF_TLD(8, 4, 0);
	static Mat_<float> state_TLD(8, 1);
	static Mat processNoise_TLD(8, 1, CV_32F);
	static Mat_<float> measurement_TLD(4,1);
	static Mat_<float> prev_measurement_TLD(4,1); prev_measurement_TLD.setTo(Scalar(0));
	Mat prediction_TLD,Ht_TLD,Ree_TLD;

	while(1)
	{
		double e1 = cv::getTickCount();
		if(select_flag !=2)
		{

			cout<<"select 2"<<endl;

		}

		if(select_flag ==2)
		{
			mu2.lock();
			cftld->processImage(img_cftld);\
			mu2.unlock();

			if(cftld->currBB != NULL)
			{
			  color_TLD=RED;
			  cftld_x=cftld->currBB->x + (cftld->currBB->width/2);
			  cftld_y=cftld->currBB->y + (cftld->currBB->height/2);
			  cftld_tl=cftld->currBB->tl();
			  cftld_br=cftld->currBB->br();
			  cftld_w=cftld->currBB->width;
			  cftld_h=cftld->currBB->height;
			}
			else
			{
				color_TLD=BLUE;
			}
			//TODO: Make sure to release the memory or lock the variable


			if(KF_init_TLD==false)
			{
				KF_init_TLD=true;

//				KF_TLD.statePre.at<float>(0) =  cftld_x;
//				KF_TLD.statePre.at<float>(1) =  cftld_y;
//				KF_TLD.statePre.at<float>(2) =  cftld_h;
//				KF_TLD.statePre.at<float>(3) =  cftld_w;
//				KF_TLD.statePre.at<float>(4) =  0.0;
//				KF_TLD.statePre.at<float>(5) =  0.0;
//				KF_TLD.statePre.at<float>(6) =  0.0;
//				KF_TLD.statePre.at<float>(7) =  0.0;
//
//				KF_TLD.statePost.at<float>(0) =  cftld_x;
//				KF_TLD.statePost.at<float>(1) =  cftld_y;
//				KF_TLD.statePost.at<float>(2) =  cftld_h;
//				KF_TLD.statePost.at<float>(3) =  cftld_w;
//				KF_TLD.statePost.at<float>(4) =  0.0;
//				KF_TLD.statePost.at<float>(5) =  0.0;
//				KF_TLD.statePost.at<float>(6) =  0.0;
//				KF_TLD.statePost.at<float>(7) =  0.0;

				KF_TLD.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																0,1,0,0,0,1,0,0,
																0,0,1,0,0,0,1,0,
																0,0,0,1,0,0,0,1,
																0,0,0,0,1,0,0,0,
																0,0,0,0,0,1,0,0,
																0,0,0,0,0,0,1,0,
																0,0,0,0,0,0,0,1);

				KF_TLD.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0,
																0,0.0001,0,0,0,0,0,0,
																0,0,0.0001,0,0,0,0,0,
																0,0,0,0.0001,0,0,0,0,
																0,0,0,0,0.008,0,0,0,
																0,0,0,0,0,0.008,0,0,
																0,0,0,0,0,0,0.008,0,
																0,0,0,0,0,0,0,0.008);

				KF_TLD.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																 0,1,0,0,0,0,0,0,
																 0,0,1,0,0,0,0,0,
																 0,0,0,1,0,0,0,0);

				KF_TLD.measurementNoiseCov=  (Mat_<float>(4, 4) << 0.24,0,0,0,
																   0,0.24,0,0,
																   0,0,0.24,0,
																   0,0,0,0.24);
				}

			measurement_TLD(0) =  cftld_x;
			measurement_TLD(1) =  cftld_y;
			measurement_TLD(2) =  cftld_h;
			measurement_TLD(3) =  cftld_w;

			prediction_TLD = KF_TLD.predict();

			transpose(KF_TLD.measurementMatrix, Ht_TLD);
			Ree_TLD= (KF_TLD.measurementMatrix*KF_TLD.errorCovPre*Ht_TLD)+KF_TLD.measurementNoiseCov;

			err_x_TLD=cftld_x-prediction_TLD.at<float>(0);
			err_y_TLD=cftld_y-prediction_TLD.at<float>(1);
			err_h_TLD=cftld_h-prediction_TLD.at<float>(2);
			err_w_TLD=cftld_w-prediction_TLD.at<float>(3);

			M_D_TLD=sqrt((pow(err_x_TLD,2)/Ree_TLD.at<float>(0,0))+(pow(err_y_TLD,2)/Ree_TLD.at<float>(1,1))+(pow(err_h_TLD,2)/Ree_TLD.at<float>(2,2))+(pow(err_w_TLD,2)/Ree_TLD.at<float>(3,3)));

			if(cftld->currBB == NULL)
			{
				M_D_TLD=20;
				confidence_TLD=(100000/(1+exp(-M_D_TLD+10)));
			}
			else
			{
				confidence_TLD=(10/(1+exp(-M_D_TLD+10)));
			}

			estimated_TLD=KF_TLD.correct(measurement_TLD);
		}
		double e2 = cv::getTickCount();
		double timetrack = (e2 - e1)/ (cv::getTickFrequency());
		//cout << "TLD TIME: " << timetrack << endl;
	}

}
/*
BoundingBox GOTURN( Mat &img, Mat &mytemplate )
{

  Mat result;
  //GO-Turn
  const cv::Mat& image = img;
  //Result goes in bbox_estimate/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/src/tracker/tracker_manager.h:14:44: error: ‘Tracker’ has not been declared

  BoundingBox bbox_estimate;
  tracker.Track(image, &regressor, &bbox_estimate);
  std::cout << "GOTURN thinks: " << bbox_estimate.get_center_x() << bbox_estimate.get_center_y() << endl;

  //matchTemplate( img, mytemplate, result, CV_TM_SQDIFF_NORMED );
  //normalize( result, result, 0, 1, NORM_MINMAX, -1, Mat() );



  return bbox_estimate;
}
*/
/*
void GOTURN_THREAD()
{
	//TODO: ALl this variables can be declared in a different file.
	bool KF_init_GOTURN=false;
	static KalmanFilter KF_GOTURN(8, 4, 0);
	static Mat_<float> state_GOTURN(8, 1);
	static Mat processNoise_GOTURN(8, 1, CV_32F);
	static Mat_<float> measurement_GOTURN(4,1);
	static Mat_<float> prev_measurement_GOTURN(4,1); prev_measurement_GOTURN.setTo(Scalar(0));
	Mat prediction_GOTURN,Ht_GOTURN,Ree_GOTURN;
	color_GOTURN = PURPLE;
	while(1)
	{
	//tic();

		if(select_flag !=2)
		{

			cout<<""<<endl;

		}

		if(select_flag ==2)
		{

			//mu4.lock();
			//const cv::Mat& image = img;
			//Mat imageGOTURN(640,480,CV_8UC3);
			//imageGOTURN = img.clone();

			BoundingBox result;
			//BoundingBox bbox_estimate;
			//double e1 = cv::getTickCount();
			tracker.Track(imageGOTURN, &regressor, &bbox_estimate);
			//double e2 = cv::getTickCount();
			//double timetrack = (e2 - e1)/ (cv::getTickFrequency());
			//cout << timetrack << endl;
			Point matchGOTURN;
			result = bbox_estimate;

		    matchGOTURN.x = result.get_center_x();
			matchGOTURN.y = result.get_center_y();

			GOTURN_x=matchGOTURN.x;
			GOTURN_y=matchGOTURN.y;
			//cftld_tl=
			//cftld_br=
			GOTURN_w=result.get_width();
			GOTURN_h=result.get_height();
			//mu4.unlock();
			//time_t end = time(0);
			//double time = difftime(end, start) * 1000.0;

			//TODO: Make sure to release the memumory or lock the variable

			//mu4.lock();
			if(KF_init_GOTURN==false)
			{
				KF_init_GOTURN=true;


				KF_GOTURN.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																0,1,0,0,0,1,0,0,
																0,0,1,0,0,0,1,0,
																0,0,0,1,0,0,0,1,
																0,0,0,0,1,0,0,0,
																0,0,0,0,0,1,0,0,
																0,0,0,0,0,0,1,0,
																0,0,0,0,0,0,0,1);

				KF_GOTURN.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0, //R
																0,0.0001,0,0,0,0,0,0,
																0,0,0.0001,0,0,0,0,0,
																0,0,0,0.0001,0,0,0,0,
																0,0,0,0,0.0008,0,0,0,
																0,0,0,0,0,0.0008,0,0,
																0,0,0,0,0,0,0.0008,0,
																0,0,0,0,0,0,0,0.0008);

				KF_GOTURN.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																 0,1,0,0,0,0,0,0,
																 0,0,1,0,0,0,0,0,
																 0,0,0,1,0,0,0,0);

				KF_GOTURN.measurementNoiseCov=  (Mat_<float>(4, 4) << 0.02,0,0,0, //Q
																   0,0.04,0,0,
																   0,0,0.02,0,
																   0,0,0,0.04);
				}

			measurement_GOTURN(0) =  GOTURN_x;
			measurement_GOTURN(1) =  GOTURN_y;
			measurement_GOTURN(2) =  GOTURN_h;
			measurement_GOTURN(3) =  GOTURN_w;

			prediction_GOTURN = KF_GOTURN.predict();

			transpose(KF_GOTURN.measurementMatrix, Ht_GOTURN);

			Ree_GOTURN= (KF_GOTURN.measurementMatrix*KF_GOTURN.errorCovPre*Ht_GOTURN)+KF_GOTURN.measurementNoiseCov;

			err_x_GOTURN=GOTURN_x-prediction_GOTURN.at<float>(0);
			err_y_GOTURN=GOTURN_y-prediction_GOTURN.at<float>(1);
			err_h_GOTURN=GOTURN_h-prediction_GOTURN.at<float>(2);
			err_w_GOTURN=GOTURN_w-prediction_GOTURN.at<float>(3);

			M_D_GOTURN=sqrt((pow(err_x_GOTURN,2)/Ree_GOTURN.at<float>(0,0))+(pow(err_y_GOTURN,2)/Ree_GOTURN.at<float>(1,1))+(pow(err_h_GOTURN,2)/Ree_GOTURN.at<float>(2,2))+(pow(err_w_GOTURN,2)/Ree_GOTURN.at<float>(3,3)));

		//	if(cftld->currBB == NULL)
		//	{
		//		M_D_GOTURN=20;
		//		confidence_GOTURN=(100000/(1+exp(-M_D_GOTURN+10)));

		//	}mage, &regressor, &bbox_estim
		//	else
		//	{
				confidence_GOTURN=(10/(1+exp(-M_D_GOTURN+10)));

		//	}
			//estimated_GOTURN = measurement_GOTURN;
			estimated_GOTURN=KF_GOTURN.correct(measurement_GOTURN);
			//double e2 = cv::getTickCount();
			//double timetrack = (e2 - e1)/ (cv::getTickFrequency());
			//cout << timetrack << endl;
			//mu4.unlock();
		}
	//toc();
	}


}
*/

void CMT_THREAD()
{

	//-----CMT display variables and calculation variables
	int cmt_x=0;
	int cmt_y=0;
	int cmt_x_tl=0;
	int cmt_y_tl=0;
	int cmt_x_br=0;
	int cmt_y_br=0;
	int cmt_w=0;
	int cmt_h=0;
	double M_D_CMT=0;
	double err_x_CMT=0;
	double err_y_CMT=0;
	double err_w_CMT=0;
	double err_h_CMT=0;
	double cv_CMT=0.2;
	double CMT_confidence=0.0;


	bool KF_init_CMT=false;
	static KalmanFilter KF_CMT(8, 4, 0);
	static Mat_<float> state_CMT(8, 1);
	static Mat processNoise_CMT(8, 1, CV_32F);
	static Mat_<float> measurement_CMT(4,1);
	static Mat_<float> prev_measurement_CMT(4,1); prev_measurement_CMT.setTo(Scalar(0));
	Mat prediction_CMT,Ht_CMT,Ree_CMT;
	sleep(.5);
	while(1){
	if(select_flag !=2)
	{
		cout<<"select2"<<endl;
	}
		if(select_flag ==2)
		{
			//TODO: Why do we have to copy this image here?
			mu1.lock();
			//double e1 = cv::getTickCount();
			Mat grey_CMT1;
			grey.copyTo(grey_CMT1);
			cmtobj.processFrame(grey_CMT1);
			//double e2 = cv::getTickCount();
			//double timetrack = (e2 - e1)/ (cv::getTickFrequency());
			//cout << timetrack << endl;
			mu1.unlock();

			CMT_confidence=cmtobj.points_active.size()/CMT_init_points;
			 if(std::isnan(CMT_confidence))
			 {
				 CMT_confidence=0.01;
			 }

			 if(CMT_confidence>=0.00)
			 {
				color_CMT=GREEN;
				cmt_x_tl=min(max((int)cmtobj.bb_rot.boundingRect().tl().x,0),(int)img.cols);
				cmt_y_tl=min(max((int)cmtobj.bb_rot.boundingRect().tl().y,0),(int)img.rows);
				cmt_x_br=min(max((int)cmtobj.bb_rot.boundingRect().br().x,10),(int)img.cols);
				cmt_y_br=min(max((int)cmtobj.bb_rot.boundingRect().br().y,10),(int)img.rows);


				cmt_w=(cmt_x_br-cmt_x_tl);
				cmt_h=(cmt_y_br-cmt_y_tl);
				cmt_x=cmt_x_tl+(cmt_w/2);
				cmt_y=cmt_y_tl+(cmt_h/2);

			 }
			 else
			 {
				 color_CMT=BLUE;
			 }

			 if(KF_init_CMT==false)
			 {
				KF_init_CMT=true;

//				KF_CMT.statePre.at<float>(0) =  cmt_x;
//				KF_CMT.statePre.at<float>(1) =  cmt_y;
//				KF_CMT.statePre.at<float>(2) =  cmt_h;
//				KF_CMT.statePre.at<float>(3) =  cmt_w;
//				KF_CMT.statePre.at<float>(4) =  0.0;
//				KF_CMT.statePre.at<float>(5) =  0.0;
//				KF_CMT.statePre.at<float>(6) =  0.0;
//				KF_CMT.statePre.at<float>(7) =  0.0;
//
//				KF_CMT.statePost.at<float>(0) =  cmt_x;
//				KF_CMT.statePost.at<float>(1) =  cmt_y;
//				KF_CMT.statePost.at<float>(2) =  cmt_h;
//				KF_CMT.statePost.at<float>(3) =  cmt_w;
//				KF_CMT.statePost.at<float>(4) =  0.0;
//				KF_CMT.statePost.at<float>(5) =  0.0;
//				KF_CMT.statePost.at<float>(6) =  0.0;
//				KF_CMT.statePost.at<float>(7) =  0.0;



				KF_CMT.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																0,1,0,0,0,1,0,0,
																0,0,1,0,0,0,1,0,
																0,0,0,1,0,0,0,1,
																0,0,0,0,1,0,0,0,
																0,0,0,0,0,1,0,0,
																0,0,0,0,0,0,1,0,
																0,0,0,0,0,0,0,1);


				KF_CMT.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0,
																0,0.0001,0,0,0,0,0,0,
																0,0,0.0001,0,0,0,0,0,
																0,0,0,0.0001,0,0,0,0,
																0,0,0,0,0.008,0,0,0,
																0,0,0,0,0,0.008,0,0,
																0,0,0,0,0,0,0.008,0,
																0,0,0,0,0,0,0,0.008);

				KF_CMT.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																 0,1,0,0,0,0,0,0,
																 0,0,1,0,0,0,0,0,
																 0,0,0,1,0,0,0,0);

				KF_CMT.measurementNoiseCov=  (Mat_<float>(4, 4) << 1.6,0,0,0,
																   0,1.6,0,0,
																   0,0,1.6,0,
																   0,0,0,1.6);

			 }

			 measurement_CMT(0) =  cmt_x;
			 measurement_CMT(1) =  cmt_y;
			 measurement_CMT(2) =  cmt_h;
			 measurement_CMT(3) =  cmt_w;


			 prediction_CMT = KF_CMT.predict();

			 transpose(KF_CMT.measurementMatrix, Ht_CMT);
			 Ree_CMT= (KF_CMT.measurementMatrix*KF_CMT.errorCovPre*Ht_CMT)+KF_CMT.measurementNoiseCov;

			 err_x_CMT=cmt_x-prediction_CMT.at<float>(0);
			 err_y_CMT=cmt_y-prediction_CMT.at<float>(1);
			 err_h_CMT=cmt_h-prediction_CMT.at<float>(2);
			 err_w_CMT=cmt_w-prediction_CMT.at<float>(3);

			 M_D_CMT=sqrt((pow(err_x_CMT,2)/Ree_CMT.at<float>(0,0))+(pow(err_y_CMT,2)/Ree_CMT.at<float>(1,1))+(pow(err_h_CMT,2)/Ree_CMT.at<float>(2,2))+(pow(err_w_CMT,2)/Ree_CMT.at<float>(3,3)));



			 //if(CMT_confidence<cv_CMT)
			 if(CMT_confidence<0.00)
			 {
				 M_D_CMT=20;
				 confidence_CMT=(10000/(1+exp(-M_D_CMT+10)));
				 //confidence_CMT=(exp(-(-M_D_CMT-3)));
			 }
			 else
			 {

				confidence_CMT=(1000/(1+exp(-M_D_CMT+10)));
			 }

			 cout << "confidence_CMT = " << CMT_confidence << endl;


			 estimated_CMT=KF_CMT.correct(measurement_CMT);
		}
	}
}


void STRUCK_THREAD()
{

	struck_1 = &main_struck;
	//-----CMT display variables and calculation variables
	int struck_x=0;
	int struck_y=0;
	int struck_x_tl=0;
	int struck_y_tl=0;
	int struck_x_br=0;
	int struck_y_br=0;
	int struck_w=0;
	int struck_h=0;
	double M_D_STRUCK=0;
	double err_x_STRUCK=0;
	double err_y_STRUCK=0;
	double err_w_STRUCK=0;
	double err_h_STRUCK=0;

	bool KF_init_STRUCK=false;
	static KalmanFilter KF_STRUCK(8, 4, 0);
	static Mat_<float> state_STRUCK(8, 1);
	static Mat processNoise_STRUCK(8, 1, CV_32F);
	static Mat_<float> measurement_STRUCK(4,1);
	static Mat_<float> prev_measurement_STRUCK(4,1); prev_measurement_STRUCK.setTo(Scalar(0));
	Mat prediction_STRUCK,Ht_STRUCK,Ree_STRUCK;

	while(1){
	//tic();
		if(select_flag !=2)
		{
			cout<<"  "<<endl;
		}
		if(select_flag ==2)
		{
			//TODO: Why do we have to copy this image here?
			mu3.lock();
			Mat grey_ST(360,240,CV_8UC1);
			grey.copyTo(grey_ST);
			struck_1->Track(grey_ST);
			mu3.unlock();

			FloatRect bb = struck_1->GetBB();

			color_STRUCK=WHITE;
			struck_x_tl=min(max((int)bb.XMin(),0),(int)img.cols);
			struck_y_tl=min(max((int)bb.YMin(),0),(int)img.rows);
			struck_x_br=min(max((int)bb.XMax(),10),(int)img.cols);
			struck_y_br=min(max((int)bb.YMax(),10),(int)img.rows);


			struck_w=(struck_x_br-struck_x_tl);
			struck_h=(struck_y_br-struck_y_tl);
			struck_x=struck_x_tl+(struck_w/2);
			struck_y=struck_y_tl+(struck_h/2);

			 if(KF_init_STRUCK==false)
			 {
				KF_init_STRUCK=true;

//				KF_STRUCK.statePre.at<float>(0) =  struck_x;
//				KF_STRUCK.statePre.at<float>(1) =  struck_y;
//				KF_STRUCK.statePre.at<float>(2) =  struck_h;
//				KF_STRUCK.statePre.at<float>(3) =  struck_w;
//				KF_STRUCK.statePre.at<float>(4) =  0.0;
//				KF_STRUCK.statePre.at<float>(5) =  0.0;
//				KF_STRUCK.statePre.at<float>(6) =  0.0;
//				KF_STRUCK.statePre.at<float>(7) =  0.0;
//
//				KF_STRUCK.statePost.at<float>(0) =  struck_x;
//				KF_STRUCK.statePost.at<float>(1) =  struck_y;
//				KF_STRUCK.statePost.at<float>(2) =  struck_h;
//				KF_STRUCK.statePost.at<float>(3) =  struck_w;
//				KF_STRUCK.statePost.at<float>(4) =  0.0;
//				KF_STRUCK.statePost.at<float>(5) =  0.0;
//				KF_STRUCK.statePost.at<float>(6) =  0.0;
//				KF_STRUCK.statePost.at<float>(7) =  0.0;
//


				KF_STRUCK.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																   0,1,0,0,0,1,0,0,
																   0,0,1,0,0,0,1,0,
																   0,0,0,1,0,0,0,1,
																   0,0,0,0,1,0,0,0,
																   0,0,0,0,0,1,0,0,
																   0,0,0,0,0,0,1,0,
																   0,0,0,0,0,0,0,1);


				KF_STRUCK.processNoiseCov =  (Mat_<float>(8, 8) << 0.001,0,0,0,0,0,0,0,
																   0,0.001,0,0,0,0,0,0,
																   0,0,0.001,0,0,0,0,0,
																   0,0,0,0.001,0,0,0,0,
																   0,0,0,0,0.08,0,0,0,
																   0,0,0,0,0,0.08,0,0,
																   0,0,0,0,0,0,0.08,0,
																   0,0,0,0,0,0,0,0.08);


				KF_STRUCK.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																	 0,1,0,0,0,0,0,0,
																	 0,0,1,0,0,0,0,0,
																	 0,0,0,1,0,0,0,0);

				KF_STRUCK.measurementNoiseCov=  (Mat_<float>(4, 4) << 1.2,0,0,0,
																	   0,1.2,0,0,
																	   0,0,1.2,0,
																	   0,0,0,1.2);

			 }

			 measurement_STRUCK(0) =  struck_x;
			 measurement_STRUCK(1) =  struck_y;
			 measurement_STRUCK(2) =  struck_h;
			 measurement_STRUCK(3) =  struck_w;

			 prediction_STRUCK = KF_STRUCK.predict();

			 transpose(KF_STRUCK.measurementMatrix, Ht_STRUCK);
			 Ree_STRUCK= (KF_STRUCK.measurementMatrix*KF_STRUCK.errorCovPre*Ht_STRUCK)+KF_STRUCK.measurementNoiseCov;

			 err_x_STRUCK=struck_x-prediction_STRUCK.at<float>(0);
			 err_y_STRUCK=struck_y-prediction_STRUCK.at<float>(1);
			 err_h_STRUCK=struck_h-prediction_STRUCK.at<float>(2);
			 err_w_STRUCK=struck_w-prediction_STRUCK.at<float>(3);

			 M_D_STRUCK=sqrt((pow(err_x_STRUCK,2)/Ree_STRUCK.at<float>(0,0))+(pow(err_y_STRUCK,2)/Ree_STRUCK.at<float>(1,1))+(pow(err_h_STRUCK,2)/Ree_STRUCK.at<float>(2,2))+(pow(err_w_STRUCK,2)/Ree_STRUCK.at<float>(3,3)));

			confidence_STRUCK=(10/(1+exp(-M_D_STRUCK+10)));

			estimated_STRUCK=KF_STRUCK.correct(measurement_STRUCK);
		}
	//toc();
	}
}



void mouseHandler(int event, int x, int y, int flags, void *param)
{
    if (event == CV_EVENT_LBUTTONDOWN && !drag)
    {
        //-----left button clicked. ROI selection begins
    	point1 = Point(x, y);
        drag = 1;
    }

    if (event == CV_EVENT_MOUSEMOVE && drag)
    {
        //----mouse dragged. ROI being selected
        Mat img1;
        img.copyTo(img1);
        point2 = Point(x, y);
        rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
        imshow("camera", img1);
    }

    if (event == CV_EVENT_LBUTTONUP && drag)
    {
        point2 = Point(x, y);
        rect = cv::Rect(point1.x, point1.y, x - point1.x, y - point1.y);
        rect_CMT=cv::Rect(point1.x/CMT_x_ratio, point1.y/CMT_y_ratio, (x - point1.x)/CMT_x_ratio, (y - point1.y)/CMT_y_ratio);
        drag = 0;
        roiImg=img(rect);//This is the image in the bounding box


        //GO-TURN Initialization
        //BoundingBox bbox_gt;
        bbox_gt.x1_ = point2.x;
        bbox_gt.y1_ = point2.y;
        bbox_gt.x2_ = point1.x;
        bbox_gt.y2_ = point1.y;

        tracker.Init(roiImg, bbox_gt, &regressor);
        cout << "Started tracking" << endl;

		roiImg.copyTo(mytemplate);
		imshow("MOUSE roiImg", mytemplate);
    }

    if (event == CV_EVENT_LBUTTONUP)
    {
    	init_cond=true;
    	select_flag = 1;
        drag = 0;
    }

}

void track_thread()
{

	const double Set_point_x=img.cols/2;
	double error_x = 0.0;

	const double Set_point_y=img.rows/2;
	double error_y = 0.0;

	cout << "I made it to level 1" <<endl;

	static KalmanFilter KF(8, 14, 2);
	static Mat_<float> state(8, 1);
	static Mat processNoise(14, 1, CV_32F);
	static Mat_<float> measurement(14,1);
	static Mat_<float> prev_measurement(14,1); prev_measurement.setTo(Scalar(0));

	//static Mat estimated;


	Mat prediction;
	int M_D_flag=0;
	while(1)
	{
		//mu4.lock();

		cout << "I made it to level 1.5  " << start <<endl;
		if (!start)
		{

			cout << "I made it to level 2" <<endl;
			if (init_cond)
			{
				color_Tracker = YELLOW;
				init_cond=false;
				//cout << "I made it to level 3 " << endl;
				Set_point_depth_TLD=sqrt(img.cols*img.rows)/sqrt(cftld_h*cftld_w);

				KF.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
															 0,1,0,0,0,1,0,0,
															 0,0,1,0,0,0,1,0,
															 0,0,0,1,0,0,0,1,
															 0,0,0,0,1,0,0,0,
															 0,0,0,0,0,1,0,0,
															 0,0,0,0,0,0,1,0,
															 0,0,0,0,0,0,0,1);

				KF.controlMatrix = (Mat_<float>(8, 2) << 0,0,
														 0,0,
														 0,0,
														 0,0,
														 0.002,0,
														 0,0.002,
														 0,0,
														 0,0);

				KF.processNoiseCov =  (Mat_<float>(8, 8) <<  //8x8
															 0.6,0,0,0,0,0,0,0,
															 0,0.6,0,0,0,0,0,0,
															 0,0,0.6,0,0,0,0,0,
															 0,0,0,0.6,0,0,0,0,
															 0,0,0,0,0.6,0,0,0,
															 0,0,0,0,0,0.6,0,0,
															 0,0,0,0,0,0,0.6,0,
															 0,0,0,0,0,0,0,0.6);

				KF.measurementMatrix=  (Mat_<float>(14, 8) << 1,0,0,0,0,0,0,0, //14x8
															  1,0,0,0,0,0,0,0,
															  1,0,0,0,0,0,0,0,
															  1,0,0,0,0,0,0,0,
															  0,1,0,0,0,0,0,0,
															  0,1,0,0,0,0,0,0,
															  0,1,0,0,0,0,0,0,
															  0,1,0,0,0,0,0,0,
															  0,0,1,0,0,0,0,0,
															  0,0,1,0,0,0,0,0,
															  0,0,1,0,0,0,0,0,
															  0,0,0,1,0,0,0,0,
															  0,0,0,1,0,0,0,0,
															  0,0,0,1,0,0,0,0);
			}


			cout << "***************************************************************" << endl;
			// The number of distances is n!/(n-1)!

//			cout<<"GOTURN is " <<estimated_GOTURN <<endl;
//			cout<<"CMT is " <<estimated_CMT <<endl;
//			cout<<"TLD is " <<estimated_TLD <<endl;
//			cout<<"STRUCK is " <<estimated_STRUCK <<endl;




			Mat old_estimated = estimated;



			Mat local_GOTURN;
			//cout << estimated_GOTURN << endl;
			mu5.lock();
			if (!estimated_GOTURN.empty())
				local_GOTURN = estimated_GOTURN;
			else
				local_GOTURN = estimated;
			mu5.unlock();
//


			Mat local_CMT= estimated_CMT;
			mu1.lock();
			if (!estimated_CMT.empty())
				local_CMT = estimated_CMT;
			else
				local_CMT = estimated;
			mu1.unlock();




			Mat local_TLD= estimated_TLD;
			mu2.lock();
			if (!estimated_TLD.empty())
				local_TLD = estimated_TLD;
			else
				local_TLD = estimated;
			mu2.unlock();


			Mat local_STRUCK = estimated_STRUCK;
			mu3.lock();
			if (!estimated_STRUCK.empty())
				local_STRUCK = estimated_STRUCK;
			else
				local_STRUCK = estimated;
			mu3.unlock();




			double dist_TLD_CMT;
			double dist_TLD_ST;
			double dist_ST_CMT;

			double dist_GOTURN_CMT;
			double dist_GOTURN_TLD;
			double dist_GOTURN_ST;






			//cout << "Crashed while computing the estimates " << endl;
//
//				cout << " Im just here to calculate Distance  "<< endl;
//
//			cout<<"GOTURN is " <<local_GOTURN << endl;
//			cout<<"CMT is " <<local_CMT <<endl;
//			cout<<"TLD is " <<local_TLD <<endl;
//			cout<<"STRUCK is " <<local_STRUCK <<endl;
//
//

			//mu4.lock();
			dist_TLD_CMT=distanceCalculate(local_TLD.at<float>(0),local_CMT.at<float>(0),local_TLD.at<float>(1),local_CMT.at<float>(1),local_TLD.at<float>(2),local_CMT.at<float>(2),local_TLD.at<float>(3),local_CMT.at<float>(3));
			dist_TLD_ST=distanceCalculate(local_TLD.at<float>(0),local_STRUCK.at<float>(0),local_TLD.at<float>(1),local_STRUCK.at<float>(1),local_TLD.at<float>(2),local_STRUCK.at<float>(2),local_TLD.at<float>(3),local_STRUCK.at<float>(3));
			dist_ST_CMT=distanceCalculate(local_CMT.at<float>(0),local_STRUCK.at<float>(0),local_CMT.at<float>(1),local_STRUCK.at<float>(1),local_CMT.at<float>(2),local_STRUCK.at<float>(2),local_CMT.at<float>(3),local_STRUCK.at<float>(3));

			dist_GOTURN_CMT=distanceCalculate(local_CMT.at<float>(0),local_GOTURN.at<float>(0),local_CMT.at<float>(1),local_GOTURN.at<float>(1),local_CMT.at<float>(2),local_GOTURN.at<float>(2),local_CMT.at<float>(3),local_GOTURN.at<float>(3));
			dist_GOTURN_TLD=distanceCalculate(local_TLD.at<float>(0),local_GOTURN.at<float>(0),local_TLD.at<float>(1),local_GOTURN.at<float>(1),local_TLD.at<float>(2),local_GOTURN.at<float>(2),local_TLD.at<float>(3),local_GOTURN.at<float>(3));
			dist_GOTURN_ST=distanceCalculate(local_TLD.at<float>(0),local_STRUCK.at<float>(0),local_GOTURN.at<float>(1),local_STRUCK.at<float>(1),local_GOTURN.at<float>(2),local_STRUCK.at<float>(2),local_GOTURN.at<float>(3),local_STRUCK.at<float>(3));


			//DEBUG!!

//			cout << "estimated_GOTURN.at<float>(0) = " << estimated_GOTURN.at<float>(0)  << endl;
//			cout << "estimated_GOTURN.at<float>(1) = " << estimated_GOTURN.at<float>(1)  << endl;
//			cout << "estimated_GOTURN.at<float>(2) = " << estimated_GOTURN.at<float>(2)  << endl;
//			cout << "estimated_GOTURN.at<float>(3) = " << estimated_GOTURN.at<float>(3)  << endl;

//			cout << "estimated_CMT.at<float>(0) = " << estimated_CMT.at<float>(0)  << endl;
//			cout << "estimated_CMT.at<float>(1) = " << estimated_CMT.at<float>(1)  << endl;
//			cout << "estimated_CMT.at<float>(2) = " << estimated_CMT.at<float>(2)  << endl;
//			cout << "estimated_CMT.at<float>(3) = " << estimated_CMT.at<float>(3)  << endl;

//

			//DEBUG!!
//			dist_TLD_CMT=distanceCalculate(0,0,0,0,0,0,0,0);
//			dist_TLD_ST=distanceCalculate(0,0,0,0,0,0,0,0);
//			dist_ST_CMT=distanceCalculate(0,0,0,0,0,0,0,0);
//
//			dist_GOTURN_CMT=distanceCalculate(0,0,0,0,0,0,0,0);
//			dist_GOTURN_TLD=distanceCalculate(0,0,0,0,0,0,0,0);
//			dist_GOTURN_ST=distanceCalculate(0,0,0,0,0,0,0,0);
			//END DEBUG!!

			//cout << "Crashed after computing the estimates " << endl;

			//TODO: use a better variable name
	//		double A = min(dist_TLD_ST,dist_GOTURN_TLD);
	//		TLD_min=min(dist_TLD_CMT,A);
			//CMT_min=min(dist_TLD_CMT,min(dist_ST_CMT,dist_GOTURN_CMT));
			//STRUCK_min=min(dist_ST_CMT,min(dist_TLD_ST,dist_GOTURN_ST));
			//GOTURN_min=min(dist_GOTURN_CMT,min(dist_GOTURN_TLD,dist_GOTURN_ST));

			TLD_min=min(dist_TLD_CMT,(min(dist_TLD_ST,dist_GOTURN_TLD)));
			CMT_min=min(dist_TLD_CMT,(min(dist_ST_CMT,dist_GOTURN_CMT)));
			STRUCK_min=min(dist_ST_CMT,(min(dist_TLD_ST,dist_GOTURN_ST)));
			GOTURN_min=min(dist_GOTURN_CMT,(min(dist_GOTURN_TLD,dist_GOTURN_ST)));

			cout << "TLD_min " << TLD_min << endl;
			cout << "CMT_min " << CMT_min << endl;
			cout << "STRUCK_min " << STRUCK_min << endl;
			cout << "GOTURN min " << GOTURN_min << endl;

			cout << "***************************************************************" << endl;


			//TODO: use a better variable name
			TLD_min = 10 + 1000*(1+tanh((TLD_min-100)/10))/2;
			CMT_min = 10 + 2000*(1+tanh((CMT_min-100)/10))/2;
			STRUCK_min = 10 + 40000*(1+tanh((STRUCK_min-80)/10))/2;
			GOTURN_min = 10 + 6000*(1+tanh((GOTURN_min-90)/10))/2;

//			cout << "TLD_min " << TLD_min << endl;
//			cout << "CMT_min " << CMT_min << endl;
//			cout << "STRUCK_min " << STRUCK_min << endl;
//			cout << "GOTURN_min " << GOTURN_min << endl;
	//		cout << "TLD_min " << TLD_min << endl;
	//		cout << "CMT_min " << CMT_min << endl;
	//		cout << "STRUCK_min " << STRUCK_min << endl;



			cout << "***************************************************************" << endl;

			//cout << " maybe Im crashing..." << endl;
//			cout << "Image size " << img.cols<<" "<<img.rows << endl;
	//
	//		cout<<"TLD_Con :"<<(1/confidence_TLD)<<endl;

	//		cout<<"KF_C: "<<KF.measurementNoiseCov<<endl;
			//---TLD_x
			measurement(0) =  estimated_TLD.at<float>(0);
			//---CMT_x
			measurement(1) =  estimated_CMT.at<float>(0);
			//---STRUCK_x
			measurement(2) =  estimated_STRUCK.at<float>(0);
			//---GOTURN_x
			measurement(3) =  estimated_GOTURN.at<float>(0);

			//---TLD_y
			measurement(4) =  estimated_TLD.at<float>(1);
			//---CMT_y
			measurement(5) =  estimated_CMT.at<float>(1);
			//---STRUCK_y
			measurement(6) =  estimated_STRUCK.at<float>(1);
			//--GOTURN_y
			measurement(7) =  estimated_GOTURN.at<float>(1);

			//---TLD_h
			measurement(8) =  estimated_TLD.at<float>(2);
			//---CMT_h
			measurement(9) =  estimated_CMT.at<float>(2);
			//---GOTURN_h
			measurement(10) =  estimated_GOTURN.at<float>(2);



			//---TLD_w
			measurement(11) =  estimated_TLD.at<float>(3);
			//---CMT_w
			measurement(12) =  estimated_CMT.at<float>(3);
			//---GOTURN_w
			measurement(13) =  estimated_GOTURN.at<float>(3);
			//-----Provide the observation and iterate

			//cout << "Image size " << img.cols<<" "<<img.rows << endl;

			prediction = KF.predict(PID_control_matrix);




			cout<<"CONFIDENCE_GOTURN   "<<confidence_GOTURN<<endl;
			KF.measurementNoiseCov=  (Mat_<float>(14, 14) <<confidence_TLD+TLD_min,0,0,0,0,0,0,0,0,0,0,0,0,0,					//---C_TLD_x 		(0,0)
															 0,confidence_CMT+CMT_min,0,0,0,0,0,0,0,0,0,0,0,0,					//---C_CMT_x  		(1,1)
															 0,0,confidence_STRUCK+STRUCK_min,0,0,0,0,0,0,0,0,0,0,0,			//---C_STRUCK_x		(2,2)
															 0,0,0,(confidence_GOTURN+GOTURN_min),0,0,0,0,0,0,0,0,0,0,	        //---C_GOTURN_x     (3,3)

															 0,0,0,0,confidence_TLD+TLD_min,0,0,0,0,0,0,0,0,0,						//---C_TLD_y		(4,4)
															 0,0,0,0,0,confidence_CMT+CMT_min,0,0,0,0,0,0,0,0,						//---C_CMT_y		(5,5)
															 0,0,0,0,0,0,confidence_STRUCK+STRUCK_min,0,0,0,0,0,0,0,			    //---C_STRUCK_y		(6,6)
															 0,0,0,0,0,0,0,(confidence_GOTURN+GOTURN_min),0,0,0,0,0,0,				//---C_GOTURN_y		(7,7)

															 0,0,0,0,0,0,0,0,confidence_TLD+TLD_min,0,0,0,0,0,					    //---C_TLD_h		(8,8)
															 0,0,0,0,0,0,0,0,0,(confidence_CMT+CMT_min),0,0,0,0,					//---C_CMT_h		(9,9)
															 0,0,0,0,0,0,0,0,0,0,(confidence_GOTURN+GOTURN_min),0,0,0,				//---C_GOTURN_h		(10,10)

															 0,0,0,0,0,0,0,0,0,0,0,confidence_TLD+TLD_min,0,0,					//---C_TLD_w		(11,11)
															 0,0,0,0,0,0,0,0,0,0,0,0,(confidence_CMT+CMT_min),0,                //---C_CMT_w		(12,12)
															 0,0,0,0,0,0,0,0,0,0,0,0,0,(confidence_GOTURN+GOTURN_min));			//---C_GOTURN_w		(13,13)



	//

			cout << "KF.measurementNoiseCov = " << KF.measurementNoiseCov << endl;





			estimated=KF.correct(measurement);

		}
//		cout<<"KG :"<<KF.gain<<endl;

//		cout<<"tld :"<< prediction.at<float>(0)<<"    "<<measurement(0)<<endl;
//		cout<<"tld_e :"<< prediction.at<float>(0)-measurement(0)<<endl;;
//		cout<<"cmt_e :"<< prediction.at<float>(1)-measurement(1)<<endl;;
//		cout<<"struck_e :"<< prediction.at<float>(2)-measurement(2)<<endl;;


#ifdef D_acq



//		rectangle( img, Point(estimated_STRUCK.at<float>(0)- estimated_STRUCK.at<float>(3)/2 ,estimated_STRUCK.at<float>(1)- estimated_STRUCK.at<float>(2)/2), Point(estimated_STRUCK.at<float>(3)/2 + estimated_STRUCK.at<float>(0),  estimated_STRUCK.at<float>(2)/2 + estimated_STRUCK.at<float>(1)), color, 3 );
//		circle(img, Point(estimated_STRUCK.at<float>(0),estimated_STRUCK.at<float>(1)), 5, color, 3);


//		data_t <<measurement(0)<<'\t'			//---TLD_x
//			   <<measurement(1)<<'\t'			//---CMT_x
//			   <<measurement(2)<<'\t'			//---STRUCK_x
//			   <<measurement(3)<<'\t'			//---TLD_y
//			   <<measurement(4)<<'\t'			//---CMT_y
//			   <<measurement(5)<<'\t'			//---STRUCK_y
//			   <<measurement(6)<<'\t'			//---TLD_h
//			   <<measurement(7)<<'\t'			//---CMT_h
//			   <<measurement(8)<<'\t'			//---TLD_w
//			   <<measurement(9)<<'\t'			//---CMT_w
//			   <<estimated.at<float>(0)<<'\t'
//			   <<estimated.at<float>(1)<<'\t'
//			   <<estimated.at<float>(2)<<'\t'
//			   <<estimated.at<float>(3)<<'\t'
//			   <<estimated.at<float>(4)<<'\t'
//			   <<estimated.at<float>(5)<<'\t'
//			   <<estimated.at<float>(6)<<'\t'
//			   <<estimated.at<float>(7)<<'\t'
//			   <<KF.measurementNoiseCov.at<float>(0,0)<<'\t' 		//---C_TLD_x 		(0,0)
//			   <<KF.measurementNoiseCov.at<float>(1,1)<<'\t'		//---C_CMT_x  		(1,1)
//			   <<KF.measurementNoiseCov.at<float>(2,2)<<'\t'		//---C_STRUCK_x  	(2,2)
//			   <<KF.measurementNoiseCov.at<float>(3,3)<<'\t'		//---C_TLD_y  		(3,3)
//			   <<KF.measurementNoiseCov.at<float>(4,4)<<'\t'		//---C_CMT_y		(4,4)
//			   <<KF.measurementNoiseCov.at<float>(5,5)<<'\t'		//---C_STRUCK_y		(5,5)
//			   <<KF.measurementNoiseCov.at<float>(6,6)<<'\t'		//---C_TLD_h		(6,6)
//			   <<KF.measurementNoiseCov.at<float>(7,7)<<'\t'		//---C_CMT_h		(7,7)
//			   <<KF.measurementNoiseCov.at<float>(8,8)<<'\t'		//---C_TLD_w		(8,8)
//			   <<KF.measurementNoiseCov.at<float>(9,9)<<endl;		//---C_CMT_w		(9,9)
#endif

		//----Estimated fusion
		rectangle( img, Point((int)(estimated.at<float>(0)- estimated.at<float>(3)/2) ,(int)(estimated.at<float>(1)- estimated.at<float>(2)/2)), Point((int)(estimated.at<float>(3)/2 + estimated.at<float>(0)),(int)(estimated.at<float>(2)/2 + estimated.at<float>(1))), Scalar(0,255,255), 3 );
		//Center of the target
		circle(img, Point(estimated.at<float>(0),estimated.at<float>(1)), 5,Scalar(0,255,255), 3);
		//Line pointing the center
		//arrowedLine( img,Point(estimated.at<float>(0),estimated.at<float>(1)),Point(img.cols/2, img.rows/2 ),color,  2, 8,0,0.05 );

		//upper part
		//line(img, Point(estimated.at<float>(0)- estimated.at<float>(3)/2 ,estimated.at<float>(1)- estimated.at<float>(2)/2), Point(estimated.at<float>(0)-rect.width/2,estimated.at<float>(1)-rect.height/2),color, 3);
		//line(img, Point(estimated.at<float>(0)+ estimated.at<float>(3)/2 ,estimated.at<float>(1)- estimated.at<float>(2)/2), Point(estimated.at<float>(0)+rect.width/2,estimated.at<float>(1)-rect.height/2),color, 3);

		//lower part
		//line(img, Point(estimated.at<float>(0)- estimated.at<float>(3)/2 ,estimated.at<float>(1)+ estimated.at<float>(2)/2), Point(estimated.at<float>(0)-rect.width/2,estimated.at<float>(1)+rect.height/2),color, 3);
		//line(img, Point(estimated.at<float>(0)+ estimated.at<float>(3)/2 ,estimated.at<float>(1)+ estimated.at<float>(2)/2), Point(estimated.at<float>(0)+rect.width/2,estimated.at<float>(1)+rect.height/2),color, 3);




	}

}

void init_proccess()
{
    start = !start;

	if(start)
	   cout << "Started tracking" << endl;
	else
	  init_cond=true;
	  cout << "Stopped tracking" << endl;
}

int main(int argc, char *argv[])
{
	//---local variables and declarations for main
	//TODO: If optimization is on. Time has increase to allow the entrace in thread
	long delay_time=2;
	//CMT_x_ratio=640.0/360.0;
	//CMT_y_ratio=480.0/240.0;
	CMT_x_ratio=640.0/360.0;
	CMT_y_ratio=480.0/240.0;

#ifdef D_acq
					//data_t.open ( "ME_4.txt", ios::out);
					data_t.open ("/home/yevgeniy/Desktop/tovarish/yalla3.txt");
					data_t <<"Tl_x_cFTLD"<<'\t'
						   <<"Tl_y_cFTLD"<<'\t'
						   <<"w_cFTLD"<<'\t'
						   <<"h_cFTLD"<<'\t'
						   <<"dx_cfTLD"<<'\t'//Added by Yev
						   <<"dy_cfTLD"<<'\t'//Added by Yev
						   <<"dw_cfTLD"<<'\t'//Added by Yev
						   <<"dh_cfTLD"<<'\t'//Added by Yev
						   <<"Tl_x_CMT"<<'\t'
						   <<"Tl_y_CMT"<<'\t'
						   <<"w_CMT"<<'\t'
						   <<"h_CMT"<<'\t'
						   <<"dx_CMT"<<'\t'//Added by Yev
						   <<"dy_CMT"<<'\t'//Added by Yev
						   <<"dw_CMT"<<'\t'//Added by Yev
						   <<"dh_CMT"<<'\t'//Added by Yev
						   <<"Tl_x_ST"<<'\t'
						   <<"Tl_y_ST"<<'\t'
						   <<"w_ST"<<'\t'
						   <<"h_ST"<<'\t'
						   <<"dx_ST"<<'\t'//Added by Yev
						   <<"dy_ST"<<'\t'//Added by Yev
						   <<"dw_ST"<<'\t'//Added by Yev
						   <<"dh_ST"<<'\t'//Added by Yev
						   //GOTURN
						   <<"Tl_x_GT"<<'\t'
						   <<"Tl_y_GT"<<'\t'
						   <<"w_GT"<<'\t'
						   <<"h_GT"<<'\t'
						   <<"dx_GT"<<'\t'//Added by Yev
						   <<"dy_GT"<<'\t'//Added by Yev
						   <<"dw_GT"<<'\t'//Added by Yev
						   <<"dh_GT"<<'\t'//Added by Yev
						   <<"Tl_x"<<'\t'
						   <<"Tl_y"<<'\t'
						   <<"w"<<'\t'
						   <<"h"<<'\t'
						   <<"cFTLD_cov"<<'\t'
						   <<"CMT_cov"<<'\t'
						   <<"STRUCK_cov"<<endl;
#endif


#ifdef serial_com
	//---Serial Communication configuration
	FILE *serPort=fopen(serialPortFilename,"w");
	system("stty -F /dev/ttyACM0 57600");

    if(serPort == NULL)
    {
		printf("ERROR");
    }
#endif

	//TODO: Check which of this variables can be removed
	cftld = new TLD();
	cftld->init(true);
	cftld->learning=true;
	cftld->detectorEnabled=true;
	cftld->learningEnabled=true;
	cftld->detectorCascade->varianceFilter->enabled = true;
	cftld->detectorCascade->ensembleClassifier->enabled = true;
	cftld->detectorCascade->nnClassifier->enabled = true;

 	//Struck

	if (conf.features.size() == 0)
	{
		cout << "error: no features specified in config" << endl;
		exit(-1);
	}





	//cvSetMouseCallback("camera", mouseHandler, NULL );



	cv::VideoCapture cap;
	//cap.open("/home/anfedres/git/ME_STRUCK/ME_T4/img_3.mp4");
	cout <<"I Am here..." <<endl;
	cap.open("/home/yevgeniy/Desktop/Mhyang/img/0%03d.jpg");
	cout << "I can open file!" << endl;
	//cap.open(0); //For capturing video real time.
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);//normal 640
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);//normal 480


	if ( !cap.isOpened() )
	 {   cout << "Unable to open video file" << endl;    return -1;    }

	cap >> img;
	cap >> img;
    for(;;)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
        cvtColor(frame, edges, COLOR_BGR2GRAY);
        GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        Canny(edges, edges, 0, 30, 3);
        imshow("edges", edges);
        if(waitKey(30) >= 0) break;
    }
}
