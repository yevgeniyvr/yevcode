#include <thread>
#include <mutex>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video.hpp"
#include <iostream>
#include "opencv2/video/tracking.hpp"


#include <opencv/cv.h>
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <math.h>

#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <vector>







#include <stdio.h>
#include "opencv2/imgproc/imgproc.hpp"

//Includes for GOTURN

#include "network/regressor.h"
#include "loader/loader_alov.h"
#include "loader/loader_vot.h"
#include "tracker/tracker.h"
#include "tracker/tracker_manager.h"
#include "tracker_manager.h"
#include "helper/helper.h"
#include "train/tracker_trainer.h"

//#include <python2.7/Python.h>
#include <iostream>
#include <string>
#include <sstream>
#include <stack>
#include <ctime>

std::stack<clock_t> tictoc_stack;




using namespace cv;
using namespace std;

std::mutex mu1;
static std::mutex barrier;


//Base of code from http://stackoverflow.com/questions/20180073/real-time-template-matching-opencv-c

Point point1, point2; /* vertical points of the bounding box */
int drag = 0;
Rect rect; /* bounding box */

Mat img(640,480,CV_8UC1), roiImg(640,480,CV_8UC1); /* roiImg - the part of the image in the bounding box */
int select_flag = 0;
bool go_fast = false;
//bool Kalman_flag = false;
//char code;
//Mat mytemplate;
//Mat measurement;

//GO-Turn Declarations

const bool show_intermediate_output = false;
const string model_file  = "/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/nets/tracker.prototxt";
const string trained_file = "/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/nets/models/pretrained_model/tracker.caffemodel";
const string videos_folder = "/home/yevgeniy/Desktop/Datasets/alov300++_rectangleAnnotation_full";
const bool do_train = false;
int gpu_id = 0;
Regressor regressor(model_file, trained_file, gpu_id, do_train);
Tracker tracker(show_intermediate_output);
Mat image(640,480,CV_8UC3);
bool Kalman_flag = false;
char code;
Mat mytemplate;
//Mat measurement;
Mat_<float> measurement(2,1); //measurement.setTo(Scalar(0));

// I want to implement a Kalman Filter here. First Initialize
//KalmanFilter KF1;
Mat imageGOTURN(640,480,CV_8UC3);
int GOTURN_x=0;
int GOTURN_y=0;
int GOTURN_w=0;
int GOTURN_h=0;
double M_D_GOTURN=0.0;
double confidence_GOTURN=0.0;
double err_x_GOTURN=0.0;
double err_y_GOTURN=0.0;
double err_w_GOTURN=0.0;
double err_h_GOTURN=0.0;
Point GOTURN_br;
Point GOTURN_tl;
Mat estimated_GOTURN;
Point matchGOTURN;

//static KalmanFilter KF1(8, 4, 0);
//static Mat_<float> state_GOTURN(8, 1);
//static Mat processNoise_GOTURN(8, 1, CV_32F);
//static Mat_<float> measurement_GOTURN(4,1);
//static Mat_<float> prev_measurement_GOTURN(4,1);prev_measurement_GOTURN.setTo(Scalar(0));
//Mat prediction_GOTURN,Ht_GOTURN,Ree_GOTURN;
//cv::Scalar color_GOTURN;
//#define PURPLE cv::Scalar(128,0,128)

///------- initialize Kalman -----------------------------------------------------------------------------------------------
KalmanFilter init_Kalman(Point pointigave)
{
	std::cout << "Running Kalman initialization with 1st point: " << pointigave << endl;

	//This part I am not sure about...
	KalmanFilter KF(4, 2, 0);
	KF.transitionMatrix = (Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);

	KF.statePost.at<float>(0) = pointigave.x;
	KF.statePost.at<float>(1) = pointigave.y;
	KF.statePre.at<float>(2) = 0;
	KF.statePre.at<float>(3) = 0;


	setIdentity(KF.measurementMatrix);
	setIdentity(KF.processNoiseCov, Scalar::all(1e-1));//1e-5
	setIdentity(KF.measurementNoiseCov, Scalar::all(1e-1));//1e-1
	setIdentity(KF.errorCovPost, Scalar::all(1));

	return KF;

}

///------- running the Kalman -----------------------------------------------------------------------------------------------
Point run_Kalman(Point mymatch, KalmanFilter KF)
{
	//std::cout << "Running Kalman on: "<< mymatch << endl;
	Mat prediction = KF.predict();
	Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
	std::cout << "PredictPt: "<< predictPt << endl;


	//Point KalmanPt;
	measurement(0) = float (mymatch.x);
	measurement(1) = float (mymatch.y);
	Mat estimated = KF.correct(measurement);
	Point KalmanPt(estimated.at<float>(0),estimated.at<float>(1));

	//Should return Kalman smooved point.
	return KalmanPt;
}
///------- GO TURN TRACKER-----------------------------------------------------------------------------------------------
void tic() {
    tictoc_stack.push(clock());
}

void toc() {
    std::cout << "Time elapsed: "
              << ((double)(clock() - tictoc_stack.top())) / CLOCKS_PER_SEC
              << std::endl;
    tictoc_stack.pop();
}
//Mat TplMatch( Mat &img, Mat &mytemplate )







BoundingBox GOTURN( Mat &img, Mat &mytemplate )
{
  //time_t start = time(0);
  Mat result;
  //GO-Turn
  //const cv::Mat& image = img;
  //Mat image(640,480,CV_8UC3);
  image = img.clone();

  //Result goes in bbox_estimate/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/src/tracker/tracker_manager.h:14:44: error: ‘Tracker’ has not been declared

  BoundingBox bbox_estimate;
  //double e1 = cv::getTickCount();
  tracker.Track(image, &regressor, &bbox_estimate);
  std::lock_guard<std::mutex> block_threads_until_finish_this_job(barrier);
  //double e2 = cv::getTickCount();
  //double timetrack = (e2 - e1)/ (cv::getTickFrequency());
  //cout << timetrack << endl;
  std::cout << "GOTURN thinks: " << bbox_estimate.get_center_x() <<" "<<bbox_estimate.get_center_y() << endl;

  //matchTemplate( img, mytemplate, result, CV_TM_SQDIFF_NORMED );
  //normalize( result, result, 0, 1, NORM_MINMAX, -1, Mat() );

  //time_t end = time(0);
  //double time = difftime(start, end) * 1000.0;
  //cout << "time is..." << time*1000000 << endl;
  return bbox_estimate;
}


///------- tracking --------------------------------------------------------------------------------------------------------
// Added char to initialize Kalman filter
/*
void track(char go)
{
    if (select_flag)
    {
        //roiImg.copyTo(mytemplate);
//         select_flag = false;
        go_fast = true;
    }

//     imshow( "mytemplate", mytemplate ); waitKey(0);

	mu1.lock();

	cout << " NEW STUFF ADDED " << endl;
    BoundingBox result  =  GOTURN( img, mytemplate );
    //std::lock_guard<std::mutex> block_threads_until_finish_this_job(barrier);
    Point match;
	match.x = result.x1_;
	match.y = result.y1_;

    //Added by Yev
    Point Kpoint;


    //KalmanFilter KF1;
    if (go == 'k' && !Kalman_flag)
    {
    	Kalman_flag = true;
    	//KF1 = init_Kalman(match);
    	//static KalmanFilter KF1(8, 4, 0);
    	//static Mat_<float> state_GOTURN(8, 1);
    	//static Mat processNoise_GOTURN(8, 1, CV_32F);
    	//static Mat_<float> measurement_GOTURN(4,1);
    	//static Mat_<float> prev_measurement_GOTURN(4,1); prev_measurement_GOTURN.setTo(Scalar(0));
    	//Mat prediction_GOTURN,Ht_GOTURN,Ree_GOTURN;

		KF1.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
														0,1,0,0,0,1,0,0,
														0,0,1,0,0,0,1,0,
														0,0,0,1,0,0,0,1,
														0,0,0,0,1,0,0,0,
														0,0,0,0,0,1,0,0,
														0,0,0,0,0,0,1,0,
														0,0,0,0,0,0,0,1);

		KF1.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0, //R
														0,0.0001,0,0,0,0,0,0,
														0,0,0.0001,0,0,0,0,0,
														0,0,0,0.0001,0,0,0,0,
														0,0,0,0,0.0008,0,0,0,
														0,0,0,0,0,0.0008,0,0,
														0,0,0,0,0,0,0.0008,0,
														0,0,0,0,0,0,0,0.0008);

		KF1.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
														 0,1,0,0,0,0,0,0,
														 0,0,1,0,0,0,0,0,
														 0,0,0,1,0,0,0,0);

		KF1.measurementNoiseCov=  (Mat_<float>(4, 4) << 0.02,0,0,0, //Q
														   0,0.04,0,0,
														   0,0,0.02,0,
														   0,0,0,0.04);

    }
    //
    if (Kalman_flag)
    {
    	//Kpoint = run_Kalman(match, KF1);
    	//matchGOTURN.x = result.get_center_x();
    	//matchGOTURN.y = result.get_center_y();
    	GOTURN_x = result.x1_;
    	GOTURN_y = result.y1_;
    	//GOTURN_x=matchGOTURN.x;
    	//GOTURN_y=matchGOTURN.y;
    					//cout << GOTURN_x << "  " << GOTURN_y << endl;
    	GOTURN_w=result.get_width();
    	GOTURN_h=result.get_height();

		measurement_GOTURN(0) =  GOTURN_x;
		measurement_GOTURN(1) =  GOTURN_y;
		measurement_GOTURN(2) =  GOTURN_h;
		measurement_GOTURN(3) =  GOTURN_w;

		prediction_GOTURN = KF1.predict();

		transpose(KF1.measurementMatrix, Ht_GOTURN);

		Ree_GOTURN= (KF1.measurementMatrix*KF1.errorCovPre*Ht_GOTURN)+KF1.measurementNoiseCov;

		err_x_GOTURN=GOTURN_x-prediction_GOTURN.at<float>(0);
		err_y_GOTURN=GOTURN_y-prediction_GOTURN.at<float>(1);
		err_h_GOTURN=GOTURN_h-prediction_GOTURN.at<float>(2);
		err_w_GOTURN=GOTURN_w-prediction_GOTURN.at<float>(3);

		M_D_GOTURN=sqrt((pow(err_x_GOTURN,2)/Ree_GOTURN.at<float>(0,0))+(pow(err_y_GOTURN,2)/Ree_GOTURN.at<float>(1,1))+(pow(err_h_GOTURN,2)/Ree_GOTURN.at<float>(2,2))+(pow(err_w_GOTURN,2)/Ree_GOTURN.at<float>(3,3)));


	    confidence_GOTURN=(10/(1+exp(-M_D_GOTURN+10)));


		estimated_GOTURN=KF1.correct(measurement_GOTURN);


		rectangle( img, Point(estimated_GOTURN.at<float>(0)- estimated_GOTURN.at<float>(3)/2 ,estimated_GOTURN.at<float>(1)- estimated_GOTURN.at<float>(2)/2), Point(estimated_GOTURN.at<float>(3)/2 + estimated_GOTURN.at<float>(0),  estimated_GOTURN.at<float>(2)/2 + estimated_GOTURN.at<float>(1)),PURPLE, 3 );
    	//rectangle( img, Kpoint, Point( Kpoint.x + mytemplate.cols , Kpoint.y + mytemplate.rows ), CV_RGB(255, 0, 255), 0.5 );
    	//Added for personal debugging
    	//match = Kpoint;
    //^Delete later
    }



    //match.x = result.x1_;
    //match.y = result.y1_;

    //Point match =  minmax( result );

    //BoundingBox bbox_estimate_uncentered;
    //tracker_->Track(image_curr, regressor_, &bbox_estimate_uncentered); // AND THIS


    rectangle( img, match, Point( result.x2_ , result.y2_ ), CV_RGB(255, 255, 255), 0.5 );





    //Make Point at Center of Template
    Point center;
    //center.x = (match.x + match.x + mytemplate.cols)/2;
    //center.y = (match.y + match.y + mytemplate.rows)/2;
    center.x = result.get_center_x();
    center.y = result.get_center_y() ;



    circle(img, center, 5, CV_RGB(255, 255, 255), FILLED, LINE_8);

	imshow("image", img);

    //std::cout << "center: " << center << endl;
    //END OF PART YEV ADDED.

    //std::cout << "match: " << match << endl;

    /// latest match is the new template
    Rect ROI = cv::Rect( match.x, match.y, mytemplate.cols, mytemplate.rows );
    roiImg = img( ROI );
    roiImg.copyTo(mytemplate);
    //imshow( "roiImg", roiImg ); //waitKey(0);


	mu1.unlock();

}

*/

///------- MouseCallback function ------------------------------------------------------------------------------------------

void mouseHandler(int event, int x, int y, int flags, void *param)
{
    if (event == CV_EVENT_LBUTTONDOWN && !drag)
    {
        /// left button clicked. ROI selection begins
        point1 = Point(x, y);
        drag = 1;
    }

    if (event == CV_EVENT_MOUSEMOVE && drag)
    {
        /// mouse dragged. ROI being selected
        Mat img1 = img.clone();
        point2 = Point(x, y);
        rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
        imshow("image", img1);
    }

    if (event == CV_EVENT_LBUTTONUP && drag)
    {
        point2 = Point(x, y);
        rect = Rect(point1.x, point1.y, x - point1.x, y - point1.y);

        //GO-TURN Initialization/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/src/tracker/tracker_manager.h:14:44: error: ‘Tracker’ has not been declared

        BoundingBox bbox_gt;
        bbox_gt.x1_ = point1.x;
        bbox_gt.y1_ = point1.y;
        bbox_gt.x2_ = point2.x;
        bbox_gt.y2_ = point2.y;
        drag = 0;
        roiImg = img(rect);//This is the image in the bounding box

//        imshow("ROIMG",roiImg);

        tracker.Init(roiImg, bbox_gt, &regressor);
        std::cout << "You made it this far..." << endl;


        roiImg.copyTo(mytemplate);

//  imshow("MOUSE roiImg", roiImg); waitKey(0);
    }

    if (event == CV_EVENT_LBUTTONUP)
    {
        /// ROI selected
        select_flag = 1;
        drag = 0;
    }

}

void GOTURN_THREAD()
{


	int k;

	//GaussianBlur( img, img, Size(7,7), 3.0 );

    VideoCapture cap(0);

    cap.set(CV_CAP_PROP_FRAME_WIDTH,640);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT,480);

    if (!cap.isOpened())
    	return;

    cout<<rect.size()<<endl;
    cap >> img;
    imshow("image", img);

	cvSetMouseCallback( "image", mouseHandler, NULL );

    cout << "I made it here -1"<<endl;

	cout << "I made it here -2"<<endl;
    //GOTURN_T.join();
    cout << "I made it here -3"<<endl;
    //cout << "I made it here "<<endl;

    while(1)
    {

    	cap >> img;
    	img.copyTo(imageGOTURN);

		cout << "select_flag = " << select_flag << endl;


		if (select_flag  == 1) {
			//cap >> img;
			//GaussianBlur( img, img, Size(7,7), 3.0 );
			//imshow( "image", img );
			//cv::flip( img, img, 1 );
			//GaussianBlur( img, img, Size(7,7), 3.0 );
			cout << "I made it here -4"<<endl;
			//track(0);
			cout << "I made it here -5"<<endl;
		}

		imshow("image",img);
		//    	imshow("image", img);
		    	//cout << "I made it here "<<endl;
		        k = waitKey(1);
				if (k == 27)
					break;

	}

}

///------- Main() ----------------------------------------------------------------------------------------------------------

int main()
{

	int k;

	//GaussianBlur( img, img, Size(7,7), 3.0 );

    VideoCapture cap(0);
    if (!cap.isOpened())
    	return 1;

    cout<<rect.size()<<endl;
    cap >> img;
    imshow("image", img);

	cvSetMouseCallback( "image", mouseHandler, NULL );

	static KalmanFilter KF1(8, 4, 0);
	static Mat_<float> state_GOTURN(8, 1);
	static Mat processNoise_GOTURN(8, 1, CV_32F);
	static Mat_<float> measurement_GOTURN(4,1);
	static Mat_<float> prev_measurement_GOTURN(4,1);prev_measurement_GOTURN.setTo(Scalar(0));
	Mat prediction_GOTURN,Ht_GOTURN,Ree_GOTURN;
	cv::Scalar color_GOTURN;
	#define PURPLE cv::Scalar(128,0,128)

    while(1)
    {

    	cap >> img;
    	img.copyTo(imageGOTURN);


		cout << "select_flag = " << select_flag << endl;


		// Flip the frame horizontally and add blur
		    //cv::flip( img, img, 1 );
		    GaussianBlur( img, img, Size(7,7), 3.0 );

		        if ( rect.width == 0 && rect.height == 0 )
		            cvSetMouseCallback( "image", mouseHandler, NULL );
		        else
		        {
		        	//Added by Yev ---> If user enters 'k' ---> Kalman Initialization begins.
		        	code = (char)waitKey(30);
		        	if (code == 'k')
		        	{

		        		std::cout << "You entered: " << code << endl;
		        		//waitKey(30);

		        	}
		        	double e1 = cv::getTickCount();
		            if (select_flag)
		            {
		                //roiImg.copyTo(mytemplate);
		        //         select_flag = false;
		                go_fast = true;
		            }

		        //     imshow( "mytemplate", mytemplate ); waitKey(0);
		            char go = code;
		        	mu1.lock();

		        	//cout << " NEW STUFF ADDED " << endl;
		            BoundingBox result  =  GOTURN( img, mytemplate );
		            //std::lock_guard<std::mutex> block_threads_until_finish_this_job(barrier);
		            Point match;
		        	match.x = result.x1_;
		        	match.y = result.y1_;
		        	cout << "Bbox h and w " << result.get_height() <<"  "<< result.get_width() << endl;
		        	//return 1;
		            //Added by Yev
		            Point Kpoint;


		            //KalmanFilter KF1;
		            if (go == 'k' && !Kalman_flag)
		            {
		            	Kalman_flag = true;
		            	//KF1 = init_Kalman(match);
		            	//static KalmanFilter KF1(8, 4, 0);
		            	//static Mat_<float> state_GOTURN(8, 1);
		            	//static Mat processNoise_GOTURN(8, 1, CV_32F);
		            	//static Mat_<float> measurement_GOTURN(4,1);
		            	//static Mat_<float> prev_measurement_GOTURN(4,1); prev_measurement_GOTURN.setTo(Scalar(0));
		            	//Mat prediction_GOTURN,Ht_GOTURN,Ree_GOTURN;

		        		KF1.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
		        														0,1,0,0,0,1,0,0,
		        														0,0,1,0,0,0,1,0,
		        														0,0,0,1,0,0,0,1,
		        														0,0,0,0,1,0,0,0,
		        														0,0,0,0,0,1,0,0,
		        														0,0,0,0,0,0,1,0,
		        														0,0,0,0,0,0,0,1);

		        		KF1.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0, //R
		        														0,0.0001,0,0,0,0,0,0,
		        														0,0,0.0001,0,0,0,0,0,
		        														0,0,0,0.0001,0,0,0,0,
		        														0,0,0,0,0.0008,0,0,0,
		        														0,0,0,0,0,0.0008,0,0,
		        														0,0,0,0,0,0,0.0008,0,
		        														0,0,0,0,0,0,0,0.0008);

		        		KF1.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
		        														 0,1,0,0,0,0,0,0,
		        														 0,0,1,0,0,0,0,0,
		        														 0,0,0,1,0,0,0,0);

		        		KF1.measurementNoiseCov=  (Mat_<float>(4, 4) << 0.02,0,0,0, //Q
		        														   0,0.04,0,0,
		        														   0,0,0.02,0,
		        														   0,0,0,0.04);

		            }
		            //
		            if (Kalman_flag)
		            {
		            	//Kpoint = run_Kalman(match, KF1);
		            	matchGOTURN.x = result.get_center_x();
		            	matchGOTURN.y = result.get_center_y();

		            	GOTURN_x=matchGOTURN.x;
		            	GOTURN_y=matchGOTURN.y;
		            					//cout << GOTURN_x << "  " << GOTURN_y << endl;
		            	GOTURN_w=result.get_width();
		            	GOTURN_h=result.get_height();
		            	cout << "Bbox h and w " << result.get_height() <<"  "<< result.get_width() << endl;
		            	//return 1;



		            	measurement_GOTURN(0) =  GOTURN_x;
		        		measurement_GOTURN(1) =  GOTURN_y;
		        		measurement_GOTURN(2) =  GOTURN_h;
		        		measurement_GOTURN(3) =  GOTURN_w;

		        		prediction_GOTURN = KF1.predict();

		        		transpose(KF1.measurementMatrix, Ht_GOTURN);

		        		Ree_GOTURN= (KF1.measurementMatrix*KF1.errorCovPre*Ht_GOTURN)+KF1.measurementNoiseCov;

		        		err_x_GOTURN=GOTURN_x-prediction_GOTURN.at<float>(0);
		        		err_y_GOTURN=GOTURN_y-prediction_GOTURN.at<float>(1);
		        		err_h_GOTURN=GOTURN_h-prediction_GOTURN.at<float>(2);
		        		err_w_GOTURN=GOTURN_w-prediction_GOTURN.at<float>(3);

		        		M_D_GOTURN=sqrt((pow(err_x_GOTURN,2)/Ree_GOTURN.at<float>(0,0))+(pow(err_y_GOTURN,2)/Ree_GOTURN.at<float>(1,1))+(pow(err_h_GOTURN,2)/Ree_GOTURN.at<float>(2,2))+(pow(err_w_GOTURN,2)/Ree_GOTURN.at<float>(3,3)));


		        	    confidence_GOTURN=(10/(1+exp(-M_D_GOTURN+10)));


		        		estimated_GOTURN=KF1.correct(measurement_GOTURN);


		        		rectangle( img, Point(estimated_GOTURN.at<float>(0)- estimated_GOTURN.at<float>(3)/2 ,estimated_GOTURN.at<float>(1)- estimated_GOTURN.at<float>(2)/2), Point(estimated_GOTURN.at<float>(3)/2 + estimated_GOTURN.at<float>(0),  estimated_GOTURN.at<float>(2)/2 + estimated_GOTURN.at<float>(1)),PURPLE, 3 );
		        		cout << "h =  " << estimated_GOTURN.at<float>(3) << endl;
		        		cout << "w =  " << estimated_GOTURN.at<float>(2) << endl;
		        		cout << "dx =  " << estimated_GOTURN.at<float>(4) << endl;
		        		cout << "dy =  " << estimated_GOTURN.at<float>(5) << endl;






		        		//rectangle( img, Kpoint, Point( Kpoint.x + mytemplate.cols , Kpoint.y + mytemplate.rows ), CV_RGB(255, 0, 255), 0.5 );
		            	//Added for personal debugging
		            	//match = Kpoint;
		            //^Delete later
		            }



		            //match.x = result.x1_;
		            //match.y = result.y1_;

		            //Point match =  minmax( result );

		            //BoundingBox bbox_estimate_uncentered;
		            //tracker_->Track(image_curr, regressor_, &bbox_estimate_uncentered); // AND THIS


		            rectangle( img, match, Point( result.x2_ , result.y2_ ), CV_RGB(255, 255, 255), 0.5 );





		            //Make Point at Center of Template
		            Point center;
		            //center.x = (match.x + match.x + mytemplate.cols)/2;
		            //center.y = (match.y + match.y + mytemplate.rows)/2;
		            center.x = result.get_center_x();
		            center.y = result.get_center_y() ;






		        	imshow("image", img);

		            //std::cout << "center: " << center << endl;
		            //END OF PART YEV ADDED.

		            //std::cout << "match: " << match << endl;

		            /// latest match is the new template
		            Rect ROI = cv::Rect( match.x, match.y, mytemplate.cols, mytemplate.rows );
		            roiImg = img( ROI );
		            roiImg.copyTo(mytemplate);
		            //imshow( "roiImg", roiImg ); //waitKey(0);


		        	mu1.unlock();

		        	double e2 = cv::getTickCount();
		        	double timetrack = (e2 - e1)/ (cv::getTickFrequency());
		        	//cout << timetrack << endl;
		}

		imshow("image",img);
		//    	imshow("image", img);
		    	//cout << "I made it here "<<endl;
	k = waitKey(go_fast ? 30 : 10000);
			if (k == 27)
				break;

	}




    return 0;
}

