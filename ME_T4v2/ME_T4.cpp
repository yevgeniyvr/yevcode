//sudo chmod 666 /dev/ttyACM0

#define serial_com
#define D_acq
#define save_img

////------TLD libraries
#include "TLDUtil.h"
#include <string>
#include "TLD.h"


//------CMT libraries
#include "CMT.h"
#include "gui.h"

//------Struck
#include "/home/yevgeniy/Desktop/tovarish/ME_T4/src/Tracker1.h"
#include "/home/yevgeniy/Desktop/tovarish/ME_T4/src/Config.h"
#include "/home/yevgeniy/Desktop/tovarish/ME_T4/src/vot.hpp"

//-----GOTURN
#include "network/regressor.h"
#include "loader/loader_alov.h"
#include "loader/loader_vot.h"
#include "tracker/tracker.h"
#include "tracker/tracker_manager.h"
#include "tracker_manager.h"
#include "helper/helper.h"
//#include "train/tracker_trainer.h"



//------OPENCV libraries
#include <iostream>
#include <opencv/cv.h>
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <math.h>

#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <vector>


//-----Multi thread libraries
#include <thread>
#include <mutex>
#include "Tracker.h"
#include "Tracker1.h"

//------namespace declarations
//using cmt::CMT;
using namespace tld;
using namespace std;
using namespace cv;
TLD *cftld;
cmt::CMT cmtobj;




//-----Variable Declaration
bool flag_detector_enable=false;
bool init_cond=true;
bool start=true;
int pan_slide = 1500;
int tilt_slide = 1500;
int max_antiwindup=2600;
int min_antiwindup=200;
int drag = 0, select_flag = 0;

Point point1, point2;
cv::Rect rect;
cv::Rect rect_CMT;

double tilt = 1500.0;
double pan=1500.0;
double Set_point_depth_TLD=0.0;


//-----TLD display variables and calculation variables
int cftld_x=0;
int cftld_y=0;
int cftld_w=0;
int cftld_h=0;
double M_D_TLD=0.0;
double confidence_TLD=0.0;
double err_x_TLD=0.0;
double err_y_TLD=0.0;
double err_w_TLD=0.0;
double err_h_TLD=0.0;
Point cftld_br;
Point cftld_tl;
//---------------------------


double TLD_min=0;
double CMT_min=0;
double STRUCK_min=0;

double confidence_CMT=0;
double CMT_init_points=0;
float CMT_x_ratio=0.0;
float CMT_y_ratio=0.0;
std::vector<Point2f> target_frame;

double confidence_STRUCK=0.0;


int struck_x=0;
int struck_y=0;
int struck_x_tl=0;
int struck_y_tl=0;
int struck_x_br=0;
int struck_y_br=0;
int struck_w=0;
int struck_h=0;


//----Images and Matrices
Mat img(640,480,CV_8UC1);
Mat CMT_image(240, 360, CV_8UC1);
Mat grey;
Mat roiImg;
Mat mytemplate;
Mat img_cftld;
Mat estimated_TLD;
Mat estimated_CMT;
Mat estimated_STRUCK;
Mat_<float> PID_control_matrix(2,1);
//Mat estimated;
Mat estimated;
//Mat grey_CMT1;

//----Mutex for threads
std::mutex mu1;
std::mutex mu2;
std::mutex mu3;
//------BB colors
#define GREEN  cv::Scalar (0,255,0)
#define RED    cv::Scalar (0,0,255)
#define BLUE   cv::Scalar (255,0,0)
#define YELLOW cv::Scalar(0,125,255)
#define WHITE cv::Scalar(255,255,255)

//------BB colors for differen detectors
cv::Scalar color;
cv::Scalar color_TLD;
cv::Scalar color_CMT;
cv::Scalar color_STRUCK;

//------Declaration of variable for saving .txt files
ofstream data_t;
ofstream data_t_cov;
static int c=0;

//----Serial Port declaration (Arduino)
#ifdef serial_com
char serialPortFilename[] = "/dev/ttyACM0";
#endif


//--------GOTURN DECLARATIONS
//const bool show_intermediate_output = false;
//const string model_file  = "/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/nets/tracker.prototxt";
//const string trained_file = "/home/yevgeniy/Desktop/tovarish/ExtraLibraries/GOTURN-master/nets/models/pretrained_model/tracker.caffemodel";
//const string videos_folder = "/home/yevgeniy/Desktop/Datasets/alov300++_rectangleAnnotation_full";
//const bool do_train = false;
//int gpu_id = 0;
//Regressor regressor(model_file, trained_file, gpu_id, do_train);

//BoundingBox bbox_gt;

struck_con::Tracker *struck_1;
string configPath = "config.txt";
Config conf(configPath);
struck_con::Tracker main_struck(conf);




double distanceCalculate(double x1, double x2, double y1, double y2,double h1, double h2, double w1, double w2)
{
    double x = x1 - x2;
    double y = y1 - y2;
    double h = h1 - h2;
    double w = w1 - w2;
    double dist;

    dist = pow(x,2)+pow(y,2)+pow(h,2)+pow(w,2);
    dist = sqrt(dist);

    return dist;
}

double pan_control(double error_pan)
{
	//1-This constants works better
	const double kp_pan =35;
	const double ki_pan =3.4;
	const double kd_pan =8;


	static int64 last_t = 0.0;
	double dt = (cv::getTickCount() - last_t) / cv::getTickFrequency();

	last_t = cv::getTickCount();

	static double integral_x = 0.0;
	         if (dt > 0.1) {
	             // Reset
	             integral_x = 0.0;
	         }
	         integral_x += error_pan * dt;
	         //----Antiwindup
	         if(integral_x>0.1)
	         {
	        	 integral_x=0.1;
	         }
	         if(integral_x<-0.1)
			 {
				 integral_x=-0.1;
			 }

	static double previous_error_pan = 0.0;
		 if (dt > 0.1) {
			 // Reset
			 previous_error_pan = 0.0;
		 }

	double derivative_x = (error_pan -  previous_error_pan) / dt;
	previous_error_pan = error_pan;

	double pan_c = (kp_pan * error_pan + ki_pan * integral_x + kd_pan * derivative_x);

return pan_c;

}

double tilt_control(double error_tilt)
{

	const double kp_tilt = 35;
	const double ki_tilt =3.4;
	const double kd_tilt = 8;


	static int64 last_t1 = 0.0;
	double dt1 = (cv::getTickCount() - last_t1) / cv::getTickFrequency();
	last_t1 = cv::getTickCount();

	static double integral_x1 = 0.0;
	         if (dt1 > 0.1) {
	             integral_x1 = 0.0;
	         }
	         integral_x1 += error_tilt * dt1;
	         //----Antiwindup
	         if(integral_x1>0.1)
	         {
	        	 integral_x1=0.1;
	         }
	         if(integral_x1<-0.1)
			 {
				 integral_x1=-0.1;
			 }

	static double previous_error_tilt = 0.0;
		 if (dt1 > 0.1) {
			 // Reset
			 previous_error_tilt = 0.0;
		 }

	double derivative_x1 = (error_tilt -  previous_error_tilt) / dt1;
	previous_error_tilt = error_tilt;


	double tilt = -(kp_tilt * error_tilt + ki_tilt * integral_x1 + kd_tilt * derivative_x1);

return tilt;

}


void rectangle(Mat& rMat, const FloatRect& rRect, const Scalar& rColour)
{
	IntRect r(rRect);
	rectangle(rMat, Point(r.XMin(), r.YMin()), Point(r.XMax(), r.YMax()), rColour);
}


void display_detectors()
{
//	 //-------TLD Display
//	 rectangle(img, cftld_tl, cftld_br, color_TLD, 3);
//	 circle(img, Point(cftld_x,cftld_y), 5, color_TLD, 3);
//	 //-------CMT Display
//	 circle(img, Point(cmt_x,cmt_y), 5, color_CMT, 3);
//	 rectangle(img, Point(cmt_x_tl,cmt_y_tl), Point(cmt_x_br,cmt_y_br), color_CMT, 3);
	 	color=color_TLD;
		if(estimated_TLD.empty()==false)
		{
			rectangle( img, Point(estimated_TLD.at<float>(0)- estimated_TLD.at<float>(3)/2 ,estimated_TLD.at<float>(1)- estimated_TLD.at<float>(2)/2), Point(estimated_TLD.at<float>(3)/2 + estimated_TLD.at<float>(0),  estimated_TLD.at<float>(2)/2 + estimated_TLD.at<float>(1)),color, 3 );
			circle(img, Point(estimated_TLD.at<float>(0),estimated_TLD.at<float>(1)), 5, color, 3);
		}
		color=color_CMT;
		if(estimated_CMT.empty()==false)
		{
			rectangle( img, Point(estimated_CMT.at<float>(0)- estimated_CMT.at<float>(3)/2 ,estimated_CMT.at<float>(1)- estimated_CMT.at<float>(2)/2), Point(estimated_CMT.at<float>(3)/2 + estimated_CMT.at<float>(0),  estimated_CMT.at<float>(2)/2 + estimated_CMT.at<float>(1)), color, 3 );
			circle(img, Point(estimated_CMT.at<float>(0),estimated_CMT.at<float>(1)), 5, color, 3);

			 //-------CMT Display Points
			for(size_t i = 0; i < cmtobj.points_active.size(); i++)
			{
				circle(img, Point(cmtobj.points_active[i].x,cmtobj.points_active[i].y),  1, color_CMT);
				target_frame.push_back(cmtobj.points_active[i]);
			}

		}
		color=color_STRUCK;
		if(estimated_STRUCK.empty()==false)
		{
			rectangle( img, Point(estimated_STRUCK.at<float>(0)- estimated_STRUCK.at<float>(3)/2 ,estimated_STRUCK.at<float>(1)- estimated_STRUCK.at<float>(2)/2), Point(estimated_STRUCK.at<float>(3)/2 + estimated_STRUCK.at<float>(0),  estimated_STRUCK.at<float>(2)/2 + estimated_STRUCK.at<float>(1)), color, 3 );
			circle(img, Point(estimated_STRUCK.at<float>(0),estimated_STRUCK.at<float>(1)), 5, color, 3);
		}
}

void TLD_THREAD()
{

	//TODO: ALl this variables can be declared in a different file.
	bool KF_init_TLD=false;
	static KalmanFilter KF_TLD(8, 4, 0);
	static Mat_<float> state_TLD(8, 1);
	static Mat processNoise_TLD(8, 1, CV_32F);
	static Mat_<float> measurement_TLD(4,1);
	static Mat_<float> prev_measurement_TLD(4,1); prev_measurement_TLD.setTo(Scalar(0));
	Mat prediction_TLD,Ht_TLD,Ree_TLD;

	while(1)
	{

		if(select_flag !=2)
		{

			cout<<""<<endl;

		}

		if(select_flag ==2)
		{
			mu2.lock();
			cftld->processImage(img_cftld);\
			mu2.unlock();

			if(cftld->currBB != NULL)
			{
			  color_TLD=RED;
			  cftld_x=cftld->currBB->x + (cftld->currBB->width/2);
			  cftld_y=cftld->currBB->y + (cftld->currBB->height/2);
			  cftld_tl=cftld->currBB->tl();
			  cftld_br=cftld->currBB->br();
			  cftld_w=cftld->currBB->width;
			  cftld_h=cftld->currBB->height;
			}
			else
			{
				color_TLD=BLUE;
			}
			//TODO: Make sure to release the memory or lock the variable


			if(KF_init_TLD==false)
			{
				KF_init_TLD=true;

//				KF_TLD.statePre.at<float>(0) =  cftld_x;
//				KF_TLD.statePre.at<float>(1) =  cftld_y;
//				KF_TLD.statePre.at<float>(2) =  cftld_h;
//				KF_TLD.statePre.at<float>(3) =  cftld_w;
//				KF_TLD.statePre.at<float>(4) =  0.0;
//				KF_TLD.statePre.at<float>(5) =  0.0;
//				KF_TLD.statePre.at<float>(6) =  0.0;
//				KF_TLD.statePre.at<float>(7) =  0.0;
//
//				KF_TLD.statePost.at<float>(0) =  cftld_x;
//				KF_TLD.statePost.at<float>(1) =  cftld_y;
//				KF_TLD.statePost.at<float>(2) =  cftld_h;
//				KF_TLD.statePost.at<float>(3) =  cftld_w;
//				KF_TLD.statePost.at<float>(4) =  0.0;
//				KF_TLD.statePost.at<float>(5) =  0.0;
//				KF_TLD.statePost.at<float>(6) =  0.0;
//				KF_TLD.statePost.at<float>(7) =  0.0;

				KF_TLD.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																0,1,0,0,0,1,0,0,
																0,0,1,0,0,0,1,0,
																0,0,0,1,0,0,0,1,
																0,0,0,0,1,0,0,0,
																0,0,0,0,0,1,0,0,
																0,0,0,0,0,0,1,0,
																0,0,0,0,0,0,0,1);

				KF_TLD.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0,
																0,0.0001,0,0,0,0,0,0,
																0,0,0.0001,0,0,0,0,0,
																0,0,0,0.0001,0,0,0,0,
																0,0,0,0,0.008,0,0,0,
																0,0,0,0,0,0.008,0,0,
																0,0,0,0,0,0,0.008,0,
																0,0,0,0,0,0,0,0.008);

				KF_TLD.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																 0,1,0,0,0,0,0,0,
																 0,0,1,0,0,0,0,0,
																 0,0,0,1,0,0,0,0);

				KF_TLD.measurementNoiseCov=  (Mat_<float>(4, 4) << 0.24,0,0,0,
																   0,0.24,0,0,
																   0,0,0.24,0,
																   0,0,0,0.24);
				}

			measurement_TLD(0) =  cftld_x;
			measurement_TLD(1) =  cftld_y;
			measurement_TLD(2) =  cftld_h;
			measurement_TLD(3) =  cftld_w;

			prediction_TLD = KF_TLD.predict();

			transpose(KF_TLD.measurementMatrix, Ht_TLD);
			Ree_TLD= (KF_TLD.measurementMatrix*KF_TLD.errorCovPre*Ht_TLD)+KF_TLD.measurementNoiseCov;

			err_x_TLD=cftld_x-prediction_TLD.at<float>(0);
			err_y_TLD=cftld_y-prediction_TLD.at<float>(1);
			err_h_TLD=cftld_h-prediction_TLD.at<float>(2);
			err_w_TLD=cftld_w-prediction_TLD.at<float>(3);

			M_D_TLD=sqrt((pow(err_x_TLD,2)/Ree_TLD.at<float>(0,0))+(pow(err_y_TLD,2)/Ree_TLD.at<float>(1,1))+(pow(err_h_TLD,2)/Ree_TLD.at<float>(2,2))+(pow(err_w_TLD,2)/Ree_TLD.at<float>(3,3)));

			if(cftld->currBB == NULL)
			{
				M_D_TLD=20;
				confidence_TLD=(100000/(1+exp(-M_D_TLD+10)));
			}
			else
			{
				confidence_TLD=(10/(1+exp(-M_D_TLD+10)));
			}

			estimated_TLD=KF_TLD.correct(measurement_TLD);
		}
	}

}
/*
void GOTURN_THREAD()
{

	//TODO: ALl this variables can be declared in a different file.
	bool KF_init_TLD=false;
	static KalmanFilter KF_TLD(8, 4, 0);
	static Mat_<float> state_TLD(8, 1);
	static Mat processNoise_TLD(8, 1, CV_32F);
	static Mat_<float> measurement_TLD(4,1);
	static Mat_<float> prev_measurement_TLD(4,1); prev_measurement_TLD.setTo(Scalar(0));
	Mat prediction_TLD,Ht_TLD,Ree_TLD;

	while(1)
	{

		if(select_flag !=2)
		{

			cout<<""<<endl;

		}

		if(select_flag ==2)
		{
			mu2.lock();
			cftld->processImage(img_cftld);\
			mu2.unlock();

			if(cftld->currBB != NULL)
			{
			  color_TLD=RED;
			  cftld_x=cftld->currBB->x + (cftld->currBB->width/2);
			  cftld_y=cftld->currBB->y + (cftld->currBB->height/2);
			  cftld_tl=cftld->currBB->tl();
			  cftld_br=cftld->currBB->br();
			  cftld_w=cftld->currBB->width;
			  cftld_h=cftld->currBB->height;
			}
			else
			{
				color_TLD=BLUE;
			}
			//TODO: Make sure to release the memory or lock the variable


			if(KF_init_TLD==false)
			{
				KF_init_TLD=true;


				KF_TLD.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																0,1,0,0,0,1,0,0,
																0,0,1,0,0,0,1,0,
																0,0,0,1,0,0,0,1,
																0,0,0,0,1,0,0,0,
																0,0,0,0,0,1,0,0,
																0,0,0,0,0,0,1,0,
																0,0,0,0,0,0,0,1);

				KF_TLD.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0,
																0,0.0001,0,0,0,0,0,0,
																0,0,0.0001,0,0,0,0,0,
																0,0,0,0.0001,0,0,0,0,
																0,0,0,0,0.008,0,0,0,
																0,0,0,0,0,0.008,0,0,
																0,0,0,0,0,0,0.008,0,
																0,0,0,0,0,0,0,0.008);

				KF_TLD.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																 0,1,0,0,0,0,0,0,
																 0,0,1,0,0,0,0,0,
																 0,0,0,1,0,0,0,0);

				KF_TLD.measurementNoiseCov=  (Mat_<float>(4, 4) << 0.24,0,0,0,
																   0,0.24,0,0,
																   0,0,0.24,0,
																   0,0,0,0.24);
				}

			measurement_TLD(0) =  cftld_x;
			measurement_TLD(1) =  cftld_y;
			measurement_TLD(2) =  cftld_h;
			measurement_TLD(3) =  cftld_w;

			prediction_TLD = KF_TLD.predict();

			transpose(KF_TLD.measurementMatrix, Ht_TLD);
			Ree_TLD= (KF_TLD.measurementMatrix*KF_TLD.errorCovPre*Ht_TLD)+KF_TLD.measurementNoiseCov;

			err_x_TLD=cftld_x-prediction_TLD.at<float>(0);
			err_y_TLD=cftld_y-prediction_TLD.at<float>(1);
			err_h_TLD=cftld_h-prediction_TLD.at<float>(2);
			err_w_TLD=cftld_w-prediction_TLD.at<float>(3);

			M_D_TLD=sqrt((pow(err_x_TLD,2)/Ree_TLD.at<float>(0,0))+(pow(err_y_TLD,2)/Ree_TLD.at<float>(1,1))+(pow(err_h_TLD,2)/Ree_TLD.at<float>(2,2))+(pow(err_w_TLD,2)/Ree_TLD.at<float>(3,3)));

			if(cftld->currBB == NULL)
			{
				M_D_TLD=20;
				confidence_TLD=(100000/(1+exp(-M_D_TLD+10)));
			}
			else
			{
				confidence_TLD=(10/(1+exp(-M_D_TLD+10)));
			}

			estimated_TLD=KF_TLD.correct(measurement_TLD);
		}
	}

}
*/
void CMT_THREAD()
{


	//-----CMT display variables and calculation variables
	int cmt_x=0;
	int cmt_y=0;
	int cmt_x_tl=0;
	int cmt_y_tl=0;
	int cmt_x_br=0;
	int cmt_y_br=0;
	int cmt_w=0;
	int cmt_h=0;
	double M_D_CMT=0;
	double err_x_CMT=0;
	double err_y_CMT=0;
	double err_w_CMT=0;
	double err_h_CMT=0;
	double cv_CMT=0.2;
	double CMT_confidence=0.0;


	bool KF_init_CMT=false;
	static KalmanFilter KF_CMT(8, 4, 0);
	static Mat_<float> state_CMT(8, 1);
	static Mat processNoise_CMT(8, 1, CV_32F);
	static Mat_<float> measurement_CMT(4,1);
	static Mat_<float> prev_measurement_CMT(4,1); prev_measurement_CMT.setTo(Scalar(0));
	Mat prediction_CMT,Ht_CMT,Ree_CMT;

	while(1){
	if(select_flag !=2)
	{
		cout<<"select2"<<endl;
	}
		if(select_flag ==2)
		{
			//TODO: Why do we have to copy this image here?
			mu1.lock();
			Mat grey_CMT1;
			grey.copyTo(grey_CMT1);
			cmtobj.processFrame(grey_CMT1);
			mu1.unlock();
			CMT_confidence=cmtobj.points_active.size()/CMT_init_points;
			 if(std::isnan(CMT_confidence))
			 {
				 CMT_confidence=0.01;
			 }

			 if(CMT_confidence>=0.00)
			 {
				color_CMT=GREEN;
				cmt_x_tl=min(max((int)cmtobj.bb_rot.boundingRect().tl().x,0),(int)img.cols);
				cmt_y_tl=min(max((int)cmtobj.bb_rot.boundingRect().tl().y,0),(int)img.rows);
				cmt_x_br=min(max((int)cmtobj.bb_rot.boundingRect().br().x,10),(int)img.cols);
				cmt_y_br=min(max((int)cmtobj.bb_rot.boundingRect().br().y,10),(int)img.rows);


				cmt_w=(cmt_x_br-cmt_x_tl);
				cmt_h=(cmt_y_br-cmt_y_tl);
				cmt_x=cmt_x_tl+(cmt_w/2);
				cmt_y=cmt_y_tl+(cmt_h/2);

			 }
			 else
			 {
				 color_CMT=BLUE;
			 }

			 if(KF_init_CMT==false)
			 {
				KF_init_CMT=true;

//				KF_CMT.statePre.at<float>(0) =  cmt_x;
//				KF_CMT.statePre.at<float>(1) =  cmt_y;
//				KF_CMT.statePre.at<float>(2) =  cmt_h;
//				KF_CMT.statePre.at<float>(3) =  cmt_w;
//				KF_CMT.statePre.at<float>(4) =  0.0;
//				KF_CMT.statePre.at<float>(5) =  0.0;
//				KF_CMT.statePre.at<float>(6) =  0.0;
//				KF_CMT.statePre.at<float>(7) =  0.0;
//
//				KF_CMT.statePost.at<float>(0) =  cmt_x;
//				KF_CMT.statePost.at<float>(1) =  cmt_y;
//				KF_CMT.statePost.at<float>(2) =  cmt_h;
//				KF_CMT.statePost.at<float>(3) =  cmt_w;
//				KF_CMT.statePost.at<float>(4) =  0.0;
//				KF_CMT.statePost.at<float>(5) =  0.0;
//				KF_CMT.statePost.at<float>(6) =  0.0;
//				KF_CMT.statePost.at<float>(7) =  0.0;



				KF_CMT.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																0,1,0,0,0,1,0,0,
																0,0,1,0,0,0,1,0,
																0,0,0,1,0,0,0,1,
																0,0,0,0,1,0,0,0,
																0,0,0,0,0,1,0,0,
																0,0,0,0,0,0,1,0,
																0,0,0,0,0,0,0,1);


				KF_CMT.processNoiseCov =  (Mat_<float>(8, 8) << 0.0001,0,0,0,0,0,0,0,
																0,0.0001,0,0,0,0,0,0,
																0,0,0.0001,0,0,0,0,0,
																0,0,0,0.0001,0,0,0,0,
																0,0,0,0,0.008,0,0,0,
																0,0,0,0,0,0.008,0,0,
																0,0,0,0,0,0,0.008,0,
																0,0,0,0,0,0,0,0.008);

				KF_CMT.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																 0,1,0,0,0,0,0,0,
																 0,0,1,0,0,0,0,0,
																 0,0,0,1,0,0,0,0);

				KF_CMT.measurementNoiseCov=  (Mat_<float>(4, 4) << 1.6,0,0,0,
																   0,1.6,0,0,
																   0,0,1.6,0,
																   0,0,0,1.6);

			 }

			 measurement_CMT(0) =  cmt_x;
			 measurement_CMT(1) =  cmt_y;
			 measurement_CMT(2) =  cmt_h;
			 measurement_CMT(3) =  cmt_w;


			 prediction_CMT = KF_CMT.predict();

			 transpose(KF_CMT.measurementMatrix, Ht_CMT);
			 Ree_CMT= (KF_CMT.measurementMatrix*KF_CMT.errorCovPre*Ht_CMT)+KF_CMT.measurementNoiseCov;

			 err_x_CMT=cmt_x-prediction_CMT.at<float>(0);
			 err_y_CMT=cmt_y-prediction_CMT.at<float>(1);
			 err_h_CMT=cmt_h-prediction_CMT.at<float>(2);
			 err_w_CMT=cmt_w-prediction_CMT.at<float>(3);

			 M_D_CMT=sqrt((pow(err_x_CMT,2)/Ree_CMT.at<float>(0,0))+(pow(err_y_CMT,2)/Ree_CMT.at<float>(1,1))+(pow(err_h_CMT,2)/Ree_CMT.at<float>(2,2))+(pow(err_w_CMT,2)/Ree_CMT.at<float>(3,3)));



			 //if(CMT_confidence<cv_CMT)
			 if(CMT_confidence<0.00)
			 {
				 M_D_CMT=20;
				 confidence_CMT=(10000/(1+exp(-M_D_CMT+10)));
				 //confidence_CMT=(exp(-(-M_D_CMT-3)));
			 }
			 else
			 {

				confidence_CMT=(1000/(1+exp(-M_D_CMT+10)));
			 }

			 cout << "confidence_CMT = " << CMT_confidence << endl;


			 estimated_CMT=KF_CMT.correct(measurement_CMT);
		}
	}
}


void STRUCK_THREAD()
{

	struck_1 = &main_struck;
	//-----CMT display variables and calculation variables
	int struck_x=0;
	int struck_y=0;
	int struck_x_tl=0;
	int struck_y_tl=0;
	int struck_x_br=0;
	int struck_y_br=0;
	int struck_w=0;
	int struck_h=0;
	double M_D_STRUCK=0;
	double err_x_STRUCK=0;
	double err_y_STRUCK=0;
	double err_w_STRUCK=0;
	double err_h_STRUCK=0;

	bool KF_init_STRUCK=false;
	static KalmanFilter KF_STRUCK(8, 4, 0);
	static Mat_<float> state_STRUCK(8, 1);
	static Mat processNoise_STRUCK(8, 1, CV_32F);
	static Mat_<float> measurement_STRUCK(4,1);
	static Mat_<float> prev_measurement_STRUCK(4,1); prev_measurement_STRUCK.setTo(Scalar(0));
	Mat prediction_STRUCK,Ht_STRUCK,Ree_STRUCK;

	while(1){

		if(select_flag !=2)
		{
			cout<<"  "<<endl;
		}
		if(select_flag ==2)
		{
			//TODO: Why do we have to copy this image here?
			mu3.lock();
			Mat grey_ST;
			grey.copyTo(grey_ST);
			struck_1->Track(grey_ST);
			mu3.unlock();

			FloatRect bb = struck_1->GetBB();

			color_STRUCK=WHITE;
			struck_x_tl=min(max((int)bb.XMin(),0),(int)img.cols);
			struck_y_tl=min(max((int)bb.YMin(),0),(int)img.rows);
			struck_x_br=min(max((int)bb.XMax(),10),(int)img.cols);
			struck_y_br=min(max((int)bb.YMax(),10),(int)img.rows);


			struck_w=(struck_x_br-struck_x_tl);
			struck_h=(struck_y_br-struck_y_tl);
			struck_x=struck_x_tl+(struck_w/2);
			struck_y=struck_y_tl+(struck_h/2);

			 if(KF_init_STRUCK==false)
			 {
				KF_init_STRUCK=true;

//				KF_STRUCK.statePre.at<float>(0) =  struck_x;
//				KF_STRUCK.statePre.at<float>(1) =  struck_y;
//				KF_STRUCK.statePre.at<float>(2) =  struck_h;
//				KF_STRUCK.statePre.at<float>(3) =  struck_w;
//				KF_STRUCK.statePre.at<float>(4) =  0.0;
//				KF_STRUCK.statePre.at<float>(5) =  0.0;
//				KF_STRUCK.statePre.at<float>(6) =  0.0;
//				KF_STRUCK.statePre.at<float>(7) =  0.0;
//
//				KF_STRUCK.statePost.at<float>(0) =  struck_x;
//				KF_STRUCK.statePost.at<float>(1) =  struck_y;
//				KF_STRUCK.statePost.at<float>(2) =  struck_h;
//				KF_STRUCK.statePost.at<float>(3) =  struck_w;
//				KF_STRUCK.statePost.at<float>(4) =  0.0;
//				KF_STRUCK.statePost.at<float>(5) =  0.0;
//				KF_STRUCK.statePost.at<float>(6) =  0.0;
//				KF_STRUCK.statePost.at<float>(7) =  0.0;
//


				KF_STRUCK.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
																   0,1,0,0,0,1,0,0,
																   0,0,1,0,0,0,1,0,
																   0,0,0,1,0,0,0,1,
																   0,0,0,0,1,0,0,0,
																   0,0,0,0,0,1,0,0,
																   0,0,0,0,0,0,1,0,
																   0,0,0,0,0,0,0,1);


				KF_STRUCK.processNoiseCov =  (Mat_<float>(8, 8) << 0.001,0,0,0,0,0,0,0,
																   0,0.001,0,0,0,0,0,0,
																   0,0,0.001,0,0,0,0,0,
																   0,0,0,0.001,0,0,0,0,
																   0,0,0,0,0.08,0,0,0,
																   0,0,0,0,0,0.08,0,0,
																   0,0,0,0,0,0,0.08,0,
																   0,0,0,0,0,0,0,0.08);


				KF_STRUCK.measurementMatrix=  (Mat_<float>(4, 8) << 1,0,0,0,0,0,0,0,
																	 0,1,0,0,0,0,0,0,
																	 0,0,1,0,0,0,0,0,
																	 0,0,0,1,0,0,0,0);

				KF_STRUCK.measurementNoiseCov=  (Mat_<float>(4, 4) << 1.2,0,0,0,
																	   0,1.2,0,0,
																	   0,0,1.2,0,
																	   0,0,0,1.2);

			 }

			 measurement_STRUCK(0) =  struck_x;
			 measurement_STRUCK(1) =  struck_y;
			 measurement_STRUCK(2) =  struck_h;
			 measurement_STRUCK(3) =  struck_w;

			 prediction_STRUCK = KF_STRUCK.predict();

			 transpose(KF_STRUCK.measurementMatrix, Ht_STRUCK);
			 Ree_STRUCK= (KF_STRUCK.measurementMatrix*KF_STRUCK.errorCovPre*Ht_STRUCK)+KF_STRUCK.measurementNoiseCov;

			 err_x_STRUCK=struck_x-prediction_STRUCK.at<float>(0);
			 err_y_STRUCK=struck_y-prediction_STRUCK.at<float>(1);
			 err_h_STRUCK=struck_h-prediction_STRUCK.at<float>(2);
			 err_w_STRUCK=struck_w-prediction_STRUCK.at<float>(3);

			 M_D_STRUCK=sqrt((pow(err_x_STRUCK,2)/Ree_STRUCK.at<float>(0,0))+(pow(err_y_STRUCK,2)/Ree_STRUCK.at<float>(1,1))+(pow(err_h_STRUCK,2)/Ree_STRUCK.at<float>(2,2))+(pow(err_w_STRUCK,2)/Ree_STRUCK.at<float>(3,3)));

			confidence_STRUCK=(10/(1+exp(-M_D_STRUCK+10)));

			estimated_STRUCK=KF_STRUCK.correct(measurement_STRUCK);
		}
	}
}



void mouseHandler(int event, int x, int y, int flags, void *param)
{
    if (event == CV_EVENT_LBUTTONDOWN && !drag)
    {
        //-----left button clicked. ROI selection begins
    	point1 = Point(x, y);
        drag = 1;
    }

    if (event == CV_EVENT_MOUSEMOVE && drag)
    {
        //----mouse dragged. ROI being selected
        Mat img1;
        img.copyTo(img1);
        point2 = Point(x, y);
        rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
        imshow("camera", img1);
    }

    if (event == CV_EVENT_LBUTTONUP && drag)
    {
        point2 = Point(x, y);
        rect = cv::Rect(point1.x, point1.y, x - point1.x, y - point1.y);
        rect_CMT=cv::Rect(point1.x/CMT_x_ratio, point1.y/CMT_y_ratio, (x - point1.x)/CMT_x_ratio, (y - point1.y)/CMT_y_ratio);
        drag = 0;
        roiImg=img(rect);//This is the image in the bounding box


        //GO-TURN Initialization
        //BoundingBox bbox_gt;
        //bbox_gt.x1_ = point1.x;
        //bbox_gt.y1_ = point1.y;
        //bbox_gt.x2_ = point2.x;
        //bbox_gt.y2_ = point2.y;


		roiImg.copyTo(mytemplate);
		imshow("MOUSE roiImg", mytemplate);
    }

    if (event == CV_EVENT_LBUTTONUP)
    {
    	init_cond=true;
    	select_flag = 1;
        drag = 0;
    }

}

void track()
{

	const double Set_point_x=img.cols/2;
	double error_x = 0.0;

	const double Set_point_y=img.rows/2;
	double error_y = 0.0;

	static KalmanFilter KF(8, 10, 2);
	static Mat_<float> state(8, 1);
	static Mat processNoise(8, 1, CV_32F);
	static Mat_<float> measurement(10,1);
	static Mat_<float> prev_measurement(10,1); prev_measurement.setTo(Scalar(0));
	Mat prediction;
	int M_D_flag=0;

	if (!start)
	{
		if (init_cond)
		{
			init_cond=false;

			Set_point_depth_TLD=sqrt(img.cols*img.rows)/sqrt(cftld_h*cftld_w);

			KF.transitionMatrix = (Mat_<float>(8, 8) << 1,0,0,0,1,0,0,0,
														 0,1,0,0,0,1,0,0,
														 0,0,1,0,0,0,1,0,
														 0,0,0,1,0,0,0,1,
														 0,0,0,0,1,0,0,0,
														 0,0,0,0,0,1,0,0,
														 0,0,0,0,0,0,1,0,
														 0,0,0,0,0,0,0,1);

			KF.controlMatrix = (Mat_<float>(8, 2) << 0,0,
													 0,0,
													 0,0,
													 0,0,
													 0.002,0,
													 0,0.002,
													 0,0,
													 0,0);

			KF.processNoiseCov =  (Mat_<float>(8, 8) <<  0,0,0,0,0,0,0,0,
														 0,0,0,0,0,0,0,0,
														 0,0,0,0,0,0,0,0,
														 0,0,0,0,0,0,0,0,
														 0,0,0,0,0.6,0,0,0,
														 0,0,0,0,0,0.6,0,0,
														 0,0,0,0,0,0,0.6,0,
														 0,0,0,0,0,0,0,0.6);

			KF.measurementMatrix=  (Mat_<float>(10, 8) << 1,0,0,0,0,0,0,0,
														  1,0,0,0,0,0,0,0,
														  1,0,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,1,0,0,0,0,0,0,
														  0,0,1,0,0,0,0,0,
														  0,0,1,0,0,0,0,0,
														  0,0,0,1,0,0,0,0,
														  0,0,0,1,0,0,0,0);
		}


		cout << "***************************************************************" << endl;

		double dist_TLD_CMT=distanceCalculate(estimated_TLD.at<float>(0),estimated_CMT.at<float>(0),estimated_TLD.at<float>(1),estimated_CMT.at<float>(1),estimated_TLD.at<float>(2),estimated_CMT.at<float>(2),estimated_TLD.at<float>(3),estimated_CMT.at<float>(3));
		double dist_TLD_ST=distanceCalculate(estimated_TLD.at<float>(0),estimated_STRUCK.at<float>(0),estimated_TLD.at<float>(1),estimated_STRUCK.at<float>(1),estimated_TLD.at<float>(2),estimated_STRUCK.at<float>(2),estimated_TLD.at<float>(3),estimated_STRUCK.at<float>(3));
		double dist_ST_CMT=distanceCalculate(estimated_CMT.at<float>(0),estimated_STRUCK.at<float>(0),estimated_CMT.at<float>(1),estimated_STRUCK.at<float>(1),estimated_CMT.at<float>(2),estimated_STRUCK.at<float>(2),estimated_CMT.at<float>(3),estimated_STRUCK.at<float>(3));


		//TODO: use a better variable name
		TLD_min=min(dist_TLD_CMT,dist_TLD_ST);
		CMT_min=min(dist_TLD_CMT,dist_ST_CMT);
		STRUCK_min=min(dist_ST_CMT,dist_TLD_ST);

		cout << "TLD_min " << TLD_min << endl;
		cout << "CMT_min " << CMT_min << endl;
		cout << "STRUCK_min " << STRUCK_min << endl;


		cout << "***************************************************************" << endl;


		//TODO: use a better variable name
		TLD_min = 10 + 1000*(1+tanh((TLD_min-100)/10))/2;
		CMT_min = 10 + 2000*(1+tanh((CMT_min-100)/10))/2;
		STRUCK_min = 10 + 40000*(1+tanh((STRUCK_min-80)/10))/2;

		cout << "TLD_min " << TLD_min << endl;
		cout << "CMT_min " << CMT_min << endl;
		cout << "STRUCK_min " << STRUCK_min << endl;

//		cout << "TLD_min " << TLD_min << endl;
//		cout << "CMT_min " << CMT_min << endl;
//		cout << "STRUCK_min " << STRUCK_min << endl;



		cout << "***************************************************************" << endl;


//
//		cout<<"TLD_Con :"<<(1/confidence_TLD)<<endl;

//		cout<<"KF_C: "<<KF.measurementNoiseCov<<endl;
		//---TLD_x
		measurement(0) =  estimated_TLD.at<float>(0);
		//---CMT_x
		measurement(1) =  estimated_CMT.at<float>(0);
		//---STRUCK_x
		measurement(2) =  estimated_STRUCK.at<float>(0);
		//---TLD_y
		measurement(3) =  estimated_TLD.at<float>(1);
		//---CMT_y
		measurement(4) =  estimated_CMT.at<float>(1);
		//---STRUCK_y
		measurement(5) =  estimated_STRUCK.at<float>(1);
		//---TLD_h
		measurement(6) =  estimated_TLD.at<float>(2);
		//---CMT_h
		measurement(7) =  estimated_CMT.at<float>(2);
		//---TLD_w
		measurement(8) =  estimated_TLD.at<float>(3);
		//---CMT_h
		measurement(9) =  estimated_CMT.at<float>(3);

		//-----Provide the observation and iterate
		prediction = KF.predict(PID_control_matrix);


		KF.measurementNoiseCov=  (Mat_<float>(10, 10) << confidence_TLD+TLD_min,0,0,0,0,0,0,0,0,0,					//---C_TLD_x 		(0,0)
														 0,confidence_CMT+CMT_min,0,0,0,0,0,0,0,0,					//---C_CMT_x  		(1,1)
														 0,0,confidence_STRUCK+STRUCK_min,0,0,0,0,0,0,0,			//---C_STRUCK_x		(2,2)
														 0,0,0,confidence_TLD+TLD_min,0,0,0,0,0,0,					//---C_TLD_y		(3,3)
														 0,0,0,0,confidence_CMT+CMT_min,0,0,0,0,0,					//---C_CMT_y		(4,4)
														 0,0,0,0,0,confidence_STRUCK+STRUCK_min,0,0,0,0,			//---C_STRUCK_y		(5,5)
														 0,0,0,0,0,0,confidence_TLD+TLD_min,0,0,0,					//---C_TLD_h		(6,6)
														 0,0,0,0,0,0,0,(confidence_CMT+CMT_min),0,0,				//---C_CMT_h		(7,7)
														 0,0,0,0,0,0,0,0,confidence_TLD+TLD_min,0,					//---C_TLD_w		(8,8)
														 0,0,0,0,0,0,0,0,0,(confidence_CMT+CMT_min));			//---C_CMT_w		(9,9)


		cout << "KF.measurementNoiseCov = " << KF.measurementNoiseCov << endl;




		estimated=KF.correct(measurement);
//		cout<<"KG :"<<KF.gain<<endl;

//		cout<<"tld :"<< prediction.at<float>(0)<<"    "<<measurement(0)<<endl;
//		cout<<"tld_e :"<< prediction.at<float>(0)-measurement(0)<<endl;;
//		cout<<"cmt_e :"<< prediction.at<float>(1)-measurement(1)<<endl;;
//		cout<<"struck_e :"<< prediction.at<float>(2)-measurement(2)<<endl;;


#ifdef D_acq



//		rectangle( img, Point(estimated_STRUCK.at<float>(0)- estimated_STRUCK.at<float>(3)/2 ,estimated_STRUCK.at<float>(1)- estimated_STRUCK.at<float>(2)/2), Point(estimated_STRUCK.at<float>(3)/2 + estimated_STRUCK.at<float>(0),  estimated_STRUCK.at<float>(2)/2 + estimated_STRUCK.at<float>(1)), color, 3 );
//		circle(img, Point(estimated_STRUCK.at<float>(0),estimated_STRUCK.at<float>(1)), 5, color, 3);


//		data_t <<measurement(0)<<'\t'			//---TLD_x
//			   <<measurement(1)<<'\t'			//---CMT_x
//			   <<measurement(2)<<'\t'			//---STRUCK_x
//			   <<measurement(3)<<'\t'			//---TLD_y
//			   <<measurement(4)<<'\t'			//---CMT_y
//			   <<measurement(5)<<'\t'			//---STRUCK_y
//			   <<measurement(6)<<'\t'			//---TLD_h
//			   <<measurement(7)<<'\t'			//---CMT_h
//			   <<measurement(8)<<'\t'			//---TLD_w
//			   <<measurement(9)<<'\t'			//---CMT_w
//			   <<estimated.at<float>(0)<<'\t'
//			   <<estimated.at<float>(1)<<'\t'
//			   <<estimated.at<float>(2)<<'\t'
//			   <<estimated.at<float>(3)<<'\t'
//			   <<estimated.at<float>(4)<<'\t'
//			   <<estimated.at<float>(5)<<'\t'
//			   <<estimated.at<float>(6)<<'\t'
//			   <<estimated.at<float>(7)<<'\t'
//			   <<KF.measurementNoiseCov.at<float>(0,0)<<'\t' 		//---C_TLD_x 		(0,0)
//			   <<KF.measurementNoiseCov.at<float>(1,1)<<'\t'		//---C_CMT_x  		(1,1)
//			   <<KF.measurementNoiseCov.at<float>(2,2)<<'\t'		//---C_STRUCK_x  	(2,2)
//			   <<KF.measurementNoiseCov.at<float>(3,3)<<'\t'		//---C_TLD_y  		(3,3)
//			   <<KF.measurementNoiseCov.at<float>(4,4)<<'\t'		//---C_CMT_y		(4,4)
//			   <<KF.measurementNoiseCov.at<float>(5,5)<<'\t'		//---C_STRUCK_y		(5,5)
//			   <<KF.measurementNoiseCov.at<float>(6,6)<<'\t'		//---C_TLD_h		(6,6)
//			   <<KF.measurementNoiseCov.at<float>(7,7)<<'\t'		//---C_CMT_h		(7,7)
//			   <<KF.measurementNoiseCov.at<float>(8,8)<<'\t'		//---C_TLD_w		(8,8)
//			   <<KF.measurementNoiseCov.at<float>(9,9)<<endl;		//---C_CMT_w		(9,9)
#endif

		//----Estimated fusion
		rectangle( img, Point((int)(estimated.at<float>(0)- estimated.at<float>(3)/2) ,(int)(estimated.at<float>(1)- estimated.at<float>(2)/2)), Point((int)(estimated.at<float>(3)/2 + estimated.at<float>(0)),(int)(estimated.at<float>(2)/2 + estimated.at<float>(1))), Scalar(0,255,255), 3 );
		//Center of the target
		circle(img, Point(estimated.at<float>(0),estimated.at<float>(1)), 5,Scalar(0,255,255), 3);
		//Line pointing the center
		//arrowedLine( img,Point(estimated.at<float>(0),estimated.at<float>(1)),Point(img.cols/2, img.rows/2 ),color,  2, 8,0,0.05 );

		//upper part
		//line(img, Point(estimated.at<float>(0)- estimated.at<float>(3)/2 ,estimated.at<float>(1)- estimated.at<float>(2)/2), Point(estimated.at<float>(0)-rect.width/2,estimated.at<float>(1)-rect.height/2),color, 3);
		//line(img, Point(estimated.at<float>(0)+ estimated.at<float>(3)/2 ,estimated.at<float>(1)- estimated.at<float>(2)/2), Point(estimated.at<float>(0)+rect.width/2,estimated.at<float>(1)-rect.height/2),color, 3);

		//lower part
		//line(img, Point(estimated.at<float>(0)- estimated.at<float>(3)/2 ,estimated.at<float>(1)+ estimated.at<float>(2)/2), Point(estimated.at<float>(0)-rect.width/2,estimated.at<float>(1)+rect.height/2),color, 3);
		//line(img, Point(estimated.at<float>(0)+ estimated.at<float>(3)/2 ,estimated.at<float>(1)+ estimated.at<float>(2)/2), Point(estimated.at<float>(0)+rect.width/2,estimated.at<float>(1)+rect.height/2),color, 3);


		if(c>=20)
		{

		double y_tilt_motion=0, x_pan_motion=0;

		//------Error Percentage for pan motion
		error_x = (Set_point_x-estimated.at<float>(0))/img.cols;
		x_pan_motion=pan_control(error_x);
		pan=pan+x_pan_motion;


		//Antiwindup for the controller
		 if(pan<=min_antiwindup)
		 {
		   pan=min_antiwindup;
		 }
		 if(pan>=max_antiwindup)
		 {
		   pan=max_antiwindup;
		 }


		 //------Error Percentage for tilt motion
		 error_y = (Set_point_y-estimated.at<float>(1))/img.cols;
		 y_tilt_motion=tilt_control(error_y);
		 tilt=tilt-y_tilt_motion;

		 //Antiwindup for the controller
		 if(tilt<=min_antiwindup)
		 {
		   tilt=min_antiwindup;
		 }

		 if(tilt>=max_antiwindup)
		 {
		   tilt=max_antiwindup;
		 }

		 PID_control_matrix.at<float>(1,1)=pan;
		 PID_control_matrix.at<float>(2,1)=tilt;

		}

	}

}

void init_proccess()
{
    start = !start;
	if(start)
	   cout << "Started tracking" << endl;
	else
	  init_cond=true;
	  cout << "Stopped tracking" << endl;
}

int main(int argc, char *argv[])
{
	//---local variables and declarations for main
	//TODO: If optimization is on. Time has increase to allow the entrace in thread
	long delay_time=2;
	CMT_x_ratio=640.0/360.0;
	CMT_y_ratio=480.0/240.0;

#ifdef D_acq
					//data_t.open ( "ME_4.txt", ios::out);
					data_t.open ("/home/yevgeniy/Desktop/tovarish/yalla.txt");
					data_t <<"Tl_x_cFTLD"<<'\t'
						   <<"Tl_y_cFTLD"<<'\t'
						   <<"w_cFTLD"<<'\t'
						   <<"h_cFTLD"<<'\t'
						   <<"dx_cfTLD"<<'\t'//Added by Yev
						   <<"dy_cfTLD"<<'\t'//Added by Yev
						   <<"dw_cfTLD"<<'\t'//Added by Yev
						   <<"dh_cfTLD"<<'\t'//Added by Yev
						   <<"Tl_x_CMT"<<'\t'
						   <<"Tl_y_CMT"<<'\t'
						   <<"w_CMT"<<'\t'
						   <<"h_CMT"<<'\t'
						   <<"dx_CMT"<<'\t'//Added by Yev
						   <<"dy_CMT"<<'\t'//Added by Yev
						   <<"dw_CMT"<<'\t'//Added by Yev
						   <<"dh_CMT"<<'\t'//Added by Yev
						   <<"Tl_x_ST"<<'\t'
						   <<"Tl_y_ST"<<'\t'
						   <<"w_ST"<<'\t'
						   <<"h_ST"<<'\t'
						   <<"dx_ST"<<'\t'//Added by Yev
						   <<"dy_ST"<<'\t'//Added by Yev
						   <<"dw_ST"<<'\t'//Added by Yev
						   <<"dh_ST"<<'\t'//Added by Yev
						   <<"Tl_x"<<'\t'
						   <<"Tl_y"<<'\t'
						   <<"w"<<'\t'
						   <<"h"<<'\t'
						   <<"cFTLD_cov"<<'\t'
						   <<"CMT_cov"<<'\t'
						   <<"STRUCK_cov"<<endl;
#endif


#ifdef serial_com
	//---Serial Communication configuration
	FILE *serPort=fopen(serialPortFilename,"w");
	system("stty -F /dev/ttyACM0 57600");

    if(serPort == NULL)
    {
		printf("ERROR");
    }
#endif

	//TODO: Check which of this variables can be removed
	cftld = new TLD();
	cftld->init(true);
	cftld->learning=true;
	cftld->detectorEnabled=true;
	cftld->learningEnabled=true;
	cftld->detectorCascade->varianceFilter->enabled = true;
	cftld->detectorCascade->ensembleClassifier->enabled = true;
	cftld->detectorCascade->nnClassifier->enabled = true;

 	//Struck

	if (conf.features.size() == 0)
	{
		cout << "error: no features specified in config" << endl;
		exit(-1);
	}




	namedWindow( "camera", WINDOW_AUTOSIZE );
	cvSetMouseCallback("camera", mouseHandler, NULL );
	createTrackbar("Pan", "camera",  &pan_slide, 2600);
	createTrackbar("Tilt", "camera", &tilt_slide, 2600);


	cv::VideoCapture cap;
//	cap.open("/home/anfedres/git/ME_STRUCK/ME_T4/img_3.mp4");
	cap.open(0);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);


	if ( !cap.isOpened() )
	 {   cout << "Unable to open video file" << endl;    return -1;    }

	cap >> img;

	//-----Launch the threads before everything start
 	std::thread TLD_T(TLD_THREAD);
 	std::thread CMT_T(CMT_THREAD);
 	std::thread STRUCK_T(STRUCK_THREAD);

 	static bool go_fast=false;

	while (1) {


		double tic = cvGetTickCount();
//		char key = cv::waitKey(go_fast ? 5 : 10000);
		char key = cv::waitKey(delay_time);
		cap >> img;
		Mat img_sv_c=img.clone();

        /*
    	  TODO: Check if Copying this images are OK. It might avoid data races
		  inside of the of the threads.
	  	*/
		mu2.try_lock();
		img.copyTo(img_cftld);
		mu2.unlock();

		//resize(img,CMT_image,CMT_image.size(),0,0,INTER_NEAREST);
		cvtColor(img, grey, CV_BGR2GRAY);

        if(flag_detector_enable)
		{

        	if (select_flag == 1)
    		 {
    			//No print any information of CMT
    			FILELog::ReportingLevel() = logINFO;
    			//------TLD

    			mu2.lock();
    			cftld->selectObject(img, &rect);
    			mu2.unlock();

    			//------CMT
    			cmtobj.points_active.clear();
				cmtobj.initialize(grey,rect);
				CMT_init_points=cmtobj.points_active.size();

				struck_1->Reset();
				FloatRect initBB_vot = FloatRect(rect.x,rect.y,rect.width,rect.height);
				struck_1->Initialise(grey, initBB_vot);


				//------GOTURN Initialize
				//trackerGOTURN.Init(roiImg, bbox_gt, &regressor);
				std::cout << "You made it this far..." << endl;


				select_flag = 2;




    		 }

        	else if(select_flag == 2)
        	{
        		display_detectors();
        	}
		}

        //-----Cases for exit
        if (key == 0x1b) break;

        //------Activate detectors
        if(key=='s')
        {
        	flag_detector_enable=!flag_detector_enable;
        }

        //------Switch cfTLD learning
        if(key == 'r')
        {
//            cftld->learningEnabled = !cftld->learningEnabled;
//            if(!cftld->learningEnabled) {cout<<"Learning Disable"<<endl;}
//            else{cout<<"Learning Enable"<<endl;}
        	go_fast=!go_fast;
        }
	     if (rect.width > 0 && rect.height >0 && !start)
	    {
		    track();
		 }

        if(key=='t')
        {
        	init_proccess();
        	init_cond=true;
        }

        if(tilt_slide<=min_antiwindup)
		{
		   tilt_slide=min_antiwindup;
		}
		if(pan_slide<=min_antiwindup)
		{
		   pan_slide=min_antiwindup;
		}

		if(start)
		{
			tilt=(int) tilt_slide;
			pan=(int) pan_slide;
		}

		setTrackbarPos("Tilt", "camera", tilt);
		setTrackbarPos("Pan", "camera", pan);


     #ifdef serial_com
     		char writeBuffer[13];
     		sprintf(writeBuffer, "g;%04d;%04d;", (int) pan, (int) tilt);
     		fwrite(writeBuffer, sizeof(char), sizeof(writeBuffer), serPort);
     		fflush(serPort);
     #endif
//     	//TODO: Check the size of the vectors, these have problems when the code is -O3
		double toc = (cvGetTickCount()- tic) / cvGetTickFrequency();
		toc = toc / 1000000;
		float fps = 1 / toc;
		char FPS[50];


static char c_o[100];

if(!start)
{
#ifdef save_img
static int c_r=0;


if(c>=20)
{

data_t <<(int) (estimated_TLD.at<float>(0)- estimated_TLD.at<float>(3)/2)<<'\t'
	   <<(int) (estimated_TLD.at<float>(1)- estimated_TLD.at<float>(2)/2)<<'\t'
	   <<(int) (estimated_TLD.at<float>(3))<<'\t'
	   <<(int) (estimated_TLD.at<float>(2))<<'\t'

	   <<(double) (estimated_TLD.at<float>(4))<<'\t'//ADDED by YEV
	   <<(double) (estimated_TLD.at<float>(5))<<'\t'
	   <<(double) (estimated_TLD.at<float>(7))<<'\t'
	   <<(double) (estimated_TLD.at<float>(6))<<'\t'

	   <<(int) (estimated_CMT.at<float>(0)- estimated_CMT.at<float>(3)/2)<<'\t'
	   <<(int) (estimated_CMT.at<float>(1)- estimated_CMT.at<float>(2)/2)<<'\t'
	   <<(int) (estimated_CMT.at<float>(3))<<'\t'
	   <<(int) (estimated_CMT.at<float>(2))<<'\t'

	   <<(double) (estimated_CMT.at<float>(4))<<'\t'//ADDED by Yev
	   <<(double) (estimated_CMT.at<float>(5))<<'\t'
	   <<(double) (estimated_CMT.at<float>(7))<<'\t'
	   <<(double) (estimated_CMT.at<float>(6))<<'\t'


	   <<(int) (estimated_STRUCK.at<float>(0)- estimated_STRUCK.at<float>(3)/2)<<'\t'
	   <<(int) (estimated_STRUCK.at<float>(1)- estimated_STRUCK.at<float>(2)/2)<<'\t'
	   <<(int) (estimated_STRUCK.at<float>(3))<<'\t'
	   <<(int) (estimated_STRUCK.at<float>(2))<<'\t'

	   <<(double) (estimated_STRUCK.at<float>(4))<<'\t'//ADDED by Yev
	   <<(double) (estimated_STRUCK.at<float>(5))<<'\t'
	   <<(double) (estimated_STRUCK.at<float>(7))<<'\t'
	   <<(double) (estimated_STRUCK.at<float>(6))<<'\t'


	   <<(int) (estimated.at<float>(0)- estimated.at<float>(3)/2)<<'\t'
	   <<(int) (estimated.at<float>(1)- estimated.at<float>(2)/2)<<'\t'
	   <<(int) (estimated.at<float>(3))<<'\t'
	   <<(int) (estimated.at<float>(2))<<'\t'
	   <<confidence_TLD+TLD_min<<'\t'
	   <<confidence_CMT+CMT_min<<'\t'
	   <<confidence_STRUCK+STRUCK_min<<endl;

	//----For printing in the screen
	int pix_x=10, pix_y=470;
	char TestStr_1[20];
	sprintf(TestStr_1,"FRAME: %d", c_r );
	putText(img, TestStr_1, Point(pix_x,pix_y), CV_FONT_NORMAL, 1.5, Scalar(255,255,255),3,3);

	sprintf(c_o,"/home/yevgeniy/Desktop/tovarish/AnImages/Test7/%05d.jpg",c_r);
	imwrite(c_o,img_sv_c);
	sprintf(c_o,"/home/yevgeniy/Desktop/tovarish/AnImages/Test7 an/%05d.jpg",(c_r));
	imwrite(c_o,img);

	c_r++;
}
	c++;

//if(!start)
//{
//	c++;
//}

#endif
}



//		int pix_x=10, pix_y=440, pix_ofs=14;
//		char TestStr_1[20];
//		sprintf(TestStr_1,"Pitch : %lf", pan );
//		putText(img, TestStr_1, Point(pix_x,pix_y), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);
//
//		char TestStr_2[20];
//		sprintf(TestStr_2,"Roll : %lf", tilt );
//		putText(img, TestStr_2, Point(pix_x,pix_y+pix_ofs), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);
//
//		char TestStr_3[20];
//		sprintf(TestStr_3,"TLD-Con :  %lf", cftld->currConf );
//		putText(img, TestStr_3, Point(pix_x+440,20), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);
//
//		char TestStr_4[20];
//		if(cftld->learningEnabled)
//		{
//			sprintf(TestStr_4,"Learn : on");
//		}
//		else
//		{
//			sprintf(TestStr_4,"Learn : off");
//		}
//
//		putText(img, TestStr_4, Point(pix_x+440,20+pix_ofs), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);
//
//		sprintf(FPS,"FPS : %lf", fps );
//		putText(img, FPS, Point(10,20), CV_FONT_NORMAL, 0.5, Scalar(0,255,0),1,1);

		imshow("camera", img);
		//moveWindow("camera", 10, 10);


    }

	data_t.close();
    return 0;

}
