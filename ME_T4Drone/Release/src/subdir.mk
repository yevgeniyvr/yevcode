################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/CMT.cpp \
../src/Config.cpp \
../src/Consensus.cpp \
../src/Features.cpp \
../src/Fusion.cpp \
../src/HaarFeature.cpp \
../src/HaarFeatures.cpp \
../src/HistogramFeatures.cpp \
../src/ImageRep.cpp \
../src/LaRank.cpp \
../src/ME_T4Drone.cpp \
../src/Matcher.cpp \
../src/MultiFeatures.cpp \
../src/RawFeatures.cpp \
../src/Sampler.cpp \
../src/Tracker.cpp \
../src/Tracker1.cpp \
../src/ardrone.cpp \
../src/command.cpp \
../src/common.cpp \
../src/config.cpp \
../src/gui.cpp \
../src/navdata.cpp \
../src/tcp.cpp \
../src/udp.cpp \
../src/version.cpp \
../src/video.cpp 

O_SRCS += \
../src/ardrone.o \
../src/command.o \
../src/config.o \
../src/navdata.o \
../src/tcp.o \
../src/udp.o \
../src/version.o \
../src/video.o 

OBJS += \
./src/CMT.o \
./src/Config.o \
./src/Consensus.o \
./src/Features.o \
./src/Fusion.o \
./src/HaarFeature.o \
./src/HaarFeatures.o \
./src/HistogramFeatures.o \
./src/ImageRep.o \
./src/LaRank.o \
./src/ME_T4Drone.o \
./src/Matcher.o \
./src/MultiFeatures.o \
./src/RawFeatures.o \
./src/Sampler.o \
./src/Tracker.o \
./src/Tracker1.o \
./src/ardrone.o \
./src/command.o \
./src/common.o \
./src/config.o \
./src/gui.o \
./src/navdata.o \
./src/tcp.o \
./src/udp.o \
./src/version.o \
./src/video.o 

CPP_DEPS += \
./src/CMT.d \
./src/Config.d \
./src/Consensus.d \
./src/Features.d \
./src/Fusion.d \
./src/HaarFeature.d \
./src/HaarFeatures.d \
./src/HistogramFeatures.d \
./src/ImageRep.d \
./src/LaRank.d \
./src/ME_T4Drone.d \
./src/Matcher.d \
./src/MultiFeatures.d \
./src/RawFeatures.d \
./src/Sampler.d \
./src/Tracker.d \
./src/Tracker1.d \
./src/ardrone.d \
./src/command.d \
./src/common.d \
./src/config.d \
./src/gui.d \
./src/navdata.d \
./src/tcp.d \
./src/udp.d \
./src/version.d \
./src/video.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


