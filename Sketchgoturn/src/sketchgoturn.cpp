
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video.hpp"
#include <iostream>
#include "opencv2/video/tracking.hpp"

#include <opencv2/videoio.hpp>

#include <stdio.h>
#include "opencv2/imgproc/imgproc.hpp"

#include "tracker.h"

#include <opencv2/videostab/inpainting.hpp>

#include "helper/helper.h"
#include "helper/bounding_box.h"
#include "network/regressor.h"
#include "network/regressor_train.h"
#include "helper/high_res_timer.h"
#include "helper/image_proc.h"

//#include <python2.7/Python.h>
#include <iostream>
#include <string>
#include <sstream>

using namespace cv;
using namespace std;

//Base of code from http://stackoverflow.com/questions/20180073/real-time-template-matching-opencv-c

Point point1, point2; /* vertical points of the bounding box */
int drag = 0;
Rect rect; /* bounding box */
Mat img, roiImg; /* roiImg - the part of the image in the bounding box */
int select_flag = 0;
bool go_fast = false;
bool Kalman_flag = false;
char code;
Mat mytemplate;
//Mat measurement;
Mat_<float> measurement(2,1); //measurement.setTo(Scalar(0));







///------- tracking --------------------------------------------------------------------------------------------------------

Tracker::Tracker(const bool show_tracking) :
  show_tracking_(show_tracking)
{
}

void Tracker::Init(const cv::Mat& image, const BoundingBox& bbox_gt,
                   RegressorBase* regressor) {
  image_prev_ = image;
  bbox_prev_tight_ = bbox_gt;

  // Predict in the current frame that the location will be approximately the same
  // as in the previous frame.
  // TODO - use a motion model?
  bbox_curr_prior_tight_ = bbox_gt;

  // Initialize the neural network.
  regressor->Init();
}

void Tracker::Init(const std::string& image_curr_path, const VOTRegion& region,
                   RegressorBase* regressor) {
  // Read the given image.
  const cv::Mat& image = cv::imread(image_curr_path);///Change this my bounding box

  // Convert the VOT region into a bounding box.
  BoundingBox bbox_gt(region);

  // Initialize the tracker.
  Init(image, bbox_gt, regressor);
}

void Tracker::Track(const cv::Mat& image_curr, RegressorBase* regressor,
                    BoundingBox* bbox_estimate_uncentered) {
  // Get target from previous image.
  cv::Mat target_pad;
  CropPadImage(bbox_prev_tight_, image_prev_, &target_pad);

  // Crop the current image based on predicted prior location of target.
  cv::Mat curr_search_region;
  BoundingBox search_location;
  double edge_spacing_x, edge_spacing_y;
  CropPadImage(bbox_curr_prior_tight_, image_curr, &curr_search_region, &search_location, &edge_spacing_x, &edge_spacing_y);

  // Estimate the bounding box location of the target, centered and scaled relative to the cropped image.
  BoundingBox bbox_estimate;
  regressor->Regress(image_curr, curr_search_region, target_pad, &bbox_estimate);

  // Unscale the estimation to the real image size.
  BoundingBox bbox_estimate_unscaled;
  bbox_estimate.Unscale(curr_search_region, &bbox_estimate_unscaled);

  // Find the estimated bounding box location relative to the current crop.
  bbox_estimate_unscaled.Uncenter(image_curr, search_location, edge_spacing_x, edge_spacing_y, bbox_estimate_uncentered);

  if (show_tracking_) {
    ShowTracking(target_pad, curr_search_region, bbox_estimate);
  }

  // Save the image.
  image_prev_ = image_curr;

  // Save the current estimate as the location of the target.
  bbox_prev_tight_ = *bbox_estimate_uncentered;

  // Save the current estimate as the prior prediction for the next image.
  // TODO - replace with a motion model prediction?
  bbox_curr_prior_tight_ = *bbox_estimate_uncentered;
}

///------- MouseCallback function ------------------------------------------------------------------------------------------

void mouseHandler(int event, int x, int y, int flags, void *param)
{
    if (event == CV_EVENT_LBUTTONDOWN && !drag)
    {
        /// left button clicked. ROI selection begins
        point1 = Point(x, y);
        drag = 1;
    }

    if (event == CV_EVENT_MOUSEMOVE && drag)
    {
        /// mouse dragged. ROI being selected
        Mat img1 = img.clone();
        point2 = Point(x, y);
        rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
        imshow("image", img1);
    }

    if (event == CV_EVENT_LBUTTONUP && drag)
    {
        point2 = Point(x, y);
        rect = Rect(point1.x, point1.y, x - point1.x, y - point1.y);
        drag = 0;
        roiImg = img(rect);
        roiImg.copyTo(mytemplate);
//  imshow("MOUSE roiImg", roiImg); waitKey(0);
    }

    if (event == CV_EVENT_LBUTTONUP)
    {
        /// ROI selected
        select_flag = 1;
        drag = 0;
    }

}



///------- Main() ----------------------------------------------------------------------------------------------------------

int main()
{
    int k;

///open webcam
    VideoCapture cap(0);
    if (!cap.isOpened())
      return 1;


    /*
    ///open video file
    VideoCapture cap;
    cap.open( "Megamind.avi" );
    if ( !cap.isOpened() )
    {   cout << "Unable to open video file" << endl;    return -1;    }
	*/
/*
    /// Set video to 320x240
     cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
     cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);*/

    cap >> img;
    GaussianBlur( img, img, Size(7,7), 3.0 );
    imshow( "image", img );

    while (1)
    {
        cap >> img;
        if ( img.empty() )
            break;

    // Flip the frame horizontally and add blur
    cv::flip( img, img, 1 );
    GaussianBlur( img, img, Size(7,7), 3.0 );

        if ( rect.width == 0 && rect.height == 0 )
            cvSetMouseCallback( "image", mouseHandler, NULL );
        else
        {
        	//Added by Yev ---> If user enters 'k' ---> Kalman Initialization begins.
        	code = (char)waitKey(5);
        	if (code == 'k')
        	{

        		std::cout << "You entered: " << code << endl;
        		//waitKey(30);

        	}
        	//Track(code);

        }
        imshow("image", img);
//  waitKey(100);   k = waitKey(75);
    k = waitKey(go_fast ? 30 : 10000);
        if (k == 27)
            break;
    }

    return 0;
}


